# NPS v3
Built using:
- MongoDB for the datastore and session store
- NodeJS w/ Express for the server backend
- jQuery on the front end
- SCSS for styling
- Pug for rendering pages server-side
- Gulp for compiling/minimizing Pug/JS/SCSS files
- Babel for compiling ES6 to compatible javascript for current and older browsers
- Font Awesome for the handy icons
- Helmet for securing HTTP headers
- ESLint, Prettifier with the Standard JS coding style
- Redis as an API cache

___

End users do not need an user account to use the service. Content moderators, however, do. User authorization is maintained using JSON web tokens stored on the client side.

User accounts can be edited and deleted via an account with the `Admin` role. `Moderator` accounts can only be created by an account with the `Admin` role.

There is no 'forgot password' option for logging in. If a moderator forgets their login information their password can be changed by any user with `Admin` privileges.

___

New submissions can be accepted/rejected by users with an account (restricted to moderators only)

Single submissions require both a PKG URL and a license; either a zRIF or RAP.

Batch contributions require both a .JSON file exported from PSDLE

If a user wishes to contribute a license to an item that's already been submitted, they can enter edit mode by clicking the pencil icon when viewing an item from the database, making their changes, then selecting 'Save Edits'. This will create an entry in the database for each item that's been changed.

Each individual edit can then be approved/rejected by a moderator.


## Routes
##### Unauthenticated
- /login
- /browse
- /search
- /contribute
- /faq
- /support

##### Authenticated
- /manage
- /manage/submissions
- /manage/edits
- /manage/users (**Admin Only**)
- /manage/seeddb

## To Do
- [x] Implement Prettify with ESLint for clean code output
  - [x] Fix/clean up any errors or warnings.
- [ ] Finish FAQ
- [x] Use same slide-down style from FAQ on batch contributions
- [ ] Finish graph charting % of PSN obtained
  - [x] Finish function to get all the data
- [x] Make the modal links on /browse able to be opened with right-click
- [ ] SeedDB 
  - [x] Allow multiple files to be selected for upload
  - [ ] Refactor to use streams
      - [x] TSV files
      - [ ] Game Details JSON
- [x]  Refactor Contributions to use streams
  - [x] Add spinner (in modal) when form is submitted for validation.
    - [x] Lock the form to prevent resending data
  - [x] Add spinner (in modal) when form is submitted for finalization.
- [x] Remove RAP column for PSP
- [ ] Create a Tools section
  - [ ] PS3 IDPS Dumper => https://nopaystation.com/ps3xploit/idps_dump/
  - [ ] rif2rap
  - [ ] rap2rif
  - [ ] rif2bin
  - [ ] zrif2rif
  - [ ] rap2file

## Setup
1. Install NodeJS
2. Install Yarn
3. Install MongoDB
4. Install Gulp
5. Install project dependencies: `yarn`
6. Compile JS/CSS/Pug files with Gulp: `gulp build`
7. Launch server: `yarn start`