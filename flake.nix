{
  description = "NPS Site";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux = let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      deps = pkgs.yarn2nix-moretea.mkYarnModules {
        name = "nps-site-deps";
        pname = "nps-site-deps";
        version = "0.0.1";
        packageJSON = ./site/package.json;
        yarnLock = ./site/yarn.lock;

        preBuild = ''
          mkdir -p $HOME/.node-gyp/${pkgs.nodejs.version}
          echo 9 > $HOME/.node-gyp/${pkgs.nodejs.version}/installVersion
          ln -sfv ${pkgs.nodejs}/include $HOME/.node-gyp/${pkgs.nodejs.version}
        '';

        pkgConfig = {
          node-sass = {
            buildInputs = [ pkgs.python pkgs.libsass pkgs.pkgconfig ];
            postInstall = ''
              LIBSASS_EXT=auto yarn --offline run build
              rm build/config.gypi
            '';
          };

          bcrypt = {
            buildInputs = [ pkgs.nodePackages.node-pre-gyp pkgs.pkgconfig ];
            postInstall = ''
              yarn --offline run install
              rm build/config.gypi
            '';
          };
        };
      };
    in pkgs.stdenv.mkDerivation {
      name = "nps-site";

      src = ./site;

      buildInputs = [
        pkgs.nodePackages.parcel-bundler
      ];

      buildPhase = ''
        mkdir node_modules
        cp -r ${deps}/node_modules/* node_modules
        cp -r ${deps}/node_modules/.bin node_modules
        ./node_modules/.bin/gulp build
      '';

      installPhase = ''
        mkdir $out
        ls public/build
        cp -rf * $out/
      '';
    };

    devShell.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.mkShell {
      NIX_PATH = "nixpkgs=${nixpkgs}";

      DB_CONNECTION = "mongodb://localhost/nps";
      PORT = "8080";
      SESSION_SECRET = "SuperSecurePassword";

      buildInputs = with nixpkgs.legacyPackages.x86_64-linux; [ 
        nodejs
        arion
        nodePackages.yarn
        nodePackages.parcel-bundler
        python
        pythonPackages.aenum
        pythonPackages.pycryptodomex
      ];
    };
  };
}
