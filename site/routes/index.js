const paginate = require('express-paginate')
let router = require('express').Router()
require(__basedir + '/config/passport')
const passport = require('passport')
let TimeAgo = require('javascript-time-ago')
let localeEn = require('javascript-time-ago/locale/en')
TimeAgo.addLocale(localeEn)

// Middleware for login/auth
let requireLogin = passport.authenticate('local', {
  successRedirect: '/manage',
  successFlash: 'Success! You are now logged in.',
  failureRedirect: '/login',
  failureFlash: 'Invalid credentials.'
})

// Controllers are externalized for readability
let Auth = require(__basedir + '/controllers/auth')
let Manage = require(__basedir + '/controllers/manage')
let Item = require(__basedir + '/controllers/item')
let Contribute = require(__basedir + '/controllers/contribute')
let User = require(__basedir + '/controllers/user')
let Api = require(__basedir + '/controllers/api')
// let Rss = require(__basedir + '/controllers/rssFeed')
let Tools = require(__basedir + '/controllers/tools')

/*************
 * Auth Routes
 *************/
router.get('/register', Auth.checkIfRegistrationOpen)
router.post('/register', Auth.register)

router.get('/login', (req, res) => {
  res.render('auth/login')
})
router.post('/login', requireLogin)

router.get('/logout', (req, res) => {
  req.logout()
  req.flash('error', 'You have logged out.')
  res.redirect('/')
})

/*************
 * RSS Feed Routes
 *************/
// router.get('/feed/rss', Rss.rss)

/*************
 * Open Routes
 *************/
router.use((req, res, next) => {
  res.locals.user = req.user
  res.locals.session = req.session
  next()
})

router.get('/', async (req, res) => {
  let lastUpdated = await Item.getLastUpdated()
  let timeAgo = new TimeAgo('en-US')
  let userAgent	= (req.headers["user-agent"] != undefined) ? req.headers["user-agent"] : "";
  let stats = {
    lastUpdated: timeAgo.format(Date.parse(lastUpdated), 'short'),
    percentComplete: await Item.getPercentPSNComplete(),
    isVita: (userAgent.includes("PlayStation Vita") || userAgent.includes("PLAYSTATION")) ,
  }

  res.render('index', stats)
})

router.get('/tsv/:query.tsv', async (req, res) => {
  let tsvUtils = require('../controllers/tsvUtils')
  let query = req.params.query.toUpperCase()

  if (query.startsWith('PS4')) {
    if (!req.isAuthenticated()) {
      return res.redirect('/404.html')
    } else {
      let path = await tsvUtils.getTSVPath(query)
      if (path) {
        return res.type('.tsv').sendFile(path)
      } else {
        return res.redirect('/404.html')
      }
    }
  } else {
    let path = await tsvUtils.getTSVPath(query)
    if (path) {
      return res.type('.tsv').sendFile(path)
    } else {
      return res.redirect('/404.html')
    }
  }
})

router.use('/edit/', async (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/login')
  }
})

router.use('/lock/', async (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/login')
  }
})

router.use('/unlock/', async (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/login')
  }
})


router.get('/browse', paginate.middleware(10, 100), Item.browse)
router.get('/browse/pending', paginate.middleware(10, 100), Item.browsePending)
router.get('/view/:platform/:titleId/:licenseId/:appVer', Item.show)
// router.get('/view/:platform/:titleId/:licenseId/:version/pkg', Item.downloadPkg)
router.get('/view/:platform/:titleId/:licenseId/:appVer/pkg', Item.downloadPkg)
router.get('/view/pending/:platform/:titleId/:licenseId/:appVer', Item.showPending)
router.get('/view/pending/:platform/:titleId/:licenseId/:appVer/pkg', Item.downloadPkg)
router.get('/edit/:platform/:category/:titleId/:licenseId', Item.edit)
router.get('/edit/:platform/:category/:titleId/:licenseId/:appVer', Item.edit)
router.post('/edit/:platform/:category/:titleId/:licenseId', Item.submitEdit)
router.post('/edit/:platform/:category/:titleId/:licenseId/:appVer', Item.submitEdit)
router.get('/lock/:platform/:category/:titleId/:licenseId/:appVer', Item.lock)
router.get('/unlock/:platform/:category/:titleId/:licenseId/:appVer', Item.unlock)

router.get('/tools/validate/:platform/:category/:titleId/:licenseId/:appVer', Tools.validate)

// search result pages
router.get('/search', paginate.middleware(10, 100), Item.searchItems)
router.get('/search/pending', paginate.middleware(10, 100), Item.searchPendingItems)

router.get('/contribute', Contribute.index)
router.get('/contribute/single', Contribute.single)
router.get('/contribute/batch', Contribute.batch)
// router.post('/contribute/single/validate', Contribute.validateSingle)
// router.post('/contribute/:method(single|batch)/finalize', Contribute.submit)
// router.post('/contribute/finalize', Contribute.finalize)

router.get('/faq', (req, res) => {
  res.render('faq')
})

router.get('/support', (req, res) => {
  res.render('support')
})

/*************
 * Tools Routes
 *************/
router.get('/tools/rap2file/:contentId/:rap', (req, res) => {
  res.setHeader('Content-Type', 'application/stream')
  res.setHeader('Content-Disposition', `attachment; filename=${req.params.contentId}.rap`)
  res.send(Tools.rap2file(req.params.rap))
})

router.get('/tools/zrif2bin/:zrif', (req, res) => {
  res.setHeader('Content-Type', 'application/stream')
  // res.setHeader('Content-Disposition', 'attachment; filename=work.bin')
  res.send(Tools.zrif2rif(decodeURIComponent(req.params.zrif)))
})

/*************
 * Authenticated Routes
 *************/
// Middleware to check if the user is authenticated.
router.use('/manage', async (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/login')
  }
})

router.get('/manage', Manage.index)

router.get('/manage/users', Manage.users)
router.post('/manage/users/:id/delete', User.deleteUser)
router.post('/manage/users/:id/edit', User.updateUser)

router.get('/manage/changePassword', Manage.changePassword)
router.post('/manage/changePassword', User.updatePassword)

router.get('/manage/contributions', paginate.middleware(10, 100), Manage.contributions)
router.get('/manage/contributions/:id/approve', Manage.approveContribution)
router.get('/manage/contributions/:id/reject', Manage.rejectContribution)
router.post('/manage/contributions/:id/save', Manage.saveEditedContribution)
router.post(
  '/manage/contributions/:action(approve|reject)',
  Manage.batchContributionAction
)

router.get('/manage/edits', Manage.edits)
router.get('/manage/edits/:id', Manage.viewEdit)
router.get('/manage/edits/:id/approve', Manage.approveEdit)
router.get('/manage/edits/:id/reject', Manage.rejectEdit)
router.get('/manage/edits/:id/edit', Manage.changeEdit)
router.post('/manage/edits/:id/edit', Manage.saveChangedEdit)

router.get('/manage/items/:itemId/delete', Manage.deleteItem)

router.get('/manage/database', Manage.database)
router.get('/manage/database/backup', Manage.dbBackup)

router.post('/manage/database/generateTSV', async (req, res) => {
  let tsvUtils = require('../controllers/tsvUtils')
  let q = req.body.tsv.split(' ')
  let platform = q[0]
  let category = q[1]
  await tsvUtils.makeTSV(platform, category)
  req.flash('success', `TSV for ${req.body.tsv} created.`)
  return res.redirect('/manage/database')
})

router.post('/manage/database/generatePendingTSV', async (req, res) => {
  let tsvUtils = require('../controllers/tsvUtils')
  let q = req.body.tsv.split(' ')
  let platform = q[0]
  let category = q[1]
  await tsvUtils.makePendingTSV(platform, category)
  req.flash('success', `TSV for ${req.body.tsv} created.`)
  return res.redirect('/manage/database')
})

router.post('/manage/seed/tsv', Manage.seedTSVItems)
router.post('/manage/seed/gamedetails', Manage.seedGameDetails)

/*************
 * API Routes
 *************/
// Middleware to cast all query parameters to lowercase
router.use('/api', (req, res, next) => {
  req.query = new Proxy(req.query, {
    get: (target, name) =>
      target[
        Object.keys(target).find(
          key => key.toLowerCase() === name.toLowerCase()
        )
      ]
  })

  next()
})

router.get('/api', Api.getItems)
router.get('/api/lastUpdated', Api.lastUpdated)

module.exports = router
