const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

const User = require(__basedir + '/models/user')

let localAuth = new LocalStrategy((username, password, done) => {
  User.findOne({ username: username }, (err, user) => {
    if (err) return done(err)
    if (!user) {
      return done(null, false, {
        error: 'Your login details could not be verified. Please try again.'
      })
    }

    user.verifyPassword(password, (err, isMatch) => {
      if (err) return done(err)
      if (!isMatch) {
        return done(null, false, {
          error: 'Incorrect password. Please try again.'
        })
      }

      return done(null, user)
    })
  }).select('username password role')
})

passport.serializeUser((user, cb) => {
  cb(null, user._id)
})

passport.deserializeUser((id, cb) => {
  User.findById(id, (err, user) => {
    if (err) cb(err, null)
    cb(null, user)
  })
})

passport.use(localAuth)
