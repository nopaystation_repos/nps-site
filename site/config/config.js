const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  nodeEnv: process.env.NODE_ENV,
  port: process.env.PORT,
  redisPort: process.env.REDIS_PORT,
  dbConnection: process.env.DB_CONNECTION,
  redisConnection: process.env.REDIS_CONNECTION,

  sessionSecret: process.env.SESSION_SECRET,

  twitterConsumerKey: process.env.TWITTER_CONSUMER_KEY,
  twitterConsumerSecret: process.env.TWITTER_CONSUMER_SECRET,
  twitterAccessTokenKey: process.env.TWITTER_ACCESS_TOKEN_KEY,
  twitterAccessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET
}
