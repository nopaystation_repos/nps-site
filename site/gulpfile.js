// Gulp.js config
global.__basedir = __dirname

require('dotenv').config()
// const { nodeEnv } = require(__basedir + '/config/config')

let gulp = require('gulp')
let nodemon = require('gulp-nodemon')
let path = require('path')
let sourcemaps = require('gulp-sourcemaps')

// SCSS Processing
let sass = require('gulp-sass')
let postcss = require('gulp-postcss')
let autoprefixer = require('autoprefixer')
let mqpacker = require('css-mqpacker')
let cssnano = require('cssnano')

// JS Processing
let stripdebug = require('gulp-strip-debug')
let uglify = require('gulp-uglify-es').default
let flatmap = require('gulp-flatmap')
let babel = require('gulp-babel')

// Compress images
let imagemin = require('gulp-imagemin')

// Compile a few Pug files
let pug = require('gulp-pug')
let replace = require('gulp-replace')
let exec = require('child_process').exec

const devEnv = process.env.NODE_ENV === 'development'

/**********
 *   Source Paths
 **********/
const dirs = {
  src: 'src',
  dest: 'public',
  build: './public/build'
}

const sassPaths = {
  src: `${dirs.src}/scss/main.scss`,
  dest: `${dirs.dest}/css/`,
  webfontsSrc: `${dirs.src}/scss/webfonts/*`,
  webfontsDest: `${dirs.dest}/css/webfonts/`
}

const jsPaths = {
  src: `${dirs.src}/js/*.js`,
  dest: `${dirs.dest}/js/`
}

const bundlePaths = (() => {
  let pugFiles = [
    'finalizeSingle.pug',
    'batchConfirm.pug',
    'newContribFinalizeForm.pug'
  ]
  //
  let jsFiles = [
    'contribution.js',
    'rifraptools.js',
    'seeddb.js',
    'utils.js',
  ]

  let pugSrcBase = `${dirs.src}/views/contribute/`
  let pugDestBase = `${dirs.dest}/js/`
  let jsSrcBase = `${dirs.src}/js/`
  let jsDestBase = `${dirs.build}/js/`

  let makeFilePaths = function(basePath, files) {
    return files.map(x => `${basePath}${x}`)
  }

  return {
    pugSrc: makeFilePaths(pugSrcBase, pugFiles),
    pugSrcBase: pugSrcBase,
    pugDestBase: pugDestBase,

    jsSrc: makeFilePaths(jsSrcBase, jsFiles),
    jsSrcBase: jsSrcBase,
    jsSrcDest: jsDestBase
  }
})()

const imagePaths = {
  src: `${dirs.src}/images/**/**/**/*`,
  dest: `${dirs.dest}/images/`
}

/**********
 *   Tasks
 **********/
// Start Nodemon with a file watcher, runs 'compile' task on Nodemon restart
gulp.task('dev', done => {
  nodemon({
    script: 'server.js',
    watch: 'src/**/*',
    tasks: ['js', 'styles', 'images'],
    env: {
      NODE_ENV: 'development'
    },
    done: done
  })
})


/**********
 *   Build Tasks
 **********/

gulp.task('js', (done) => {
  if (!devEnv) {
    gulp
      .src(jsPaths.src)
      .pipe(sourcemaps.init())
      // .pipe(stripdebug())
      .pipe(babel())
      .pipe(uglify())
      .pipe(sourcemaps.write()) // inline sourcemaps
      .pipe(gulp.dest(jsPaths.dest))
  } else {
    gulp.src(jsPaths.src).pipe(gulp.dest(jsPaths.dest))
  }

  done()
})

// Compile / Uglify JS files
// gulp.task('scripts', () => {
//     let jsbuild = gulp.src(jsPaths.src)
//
//     if (!devEnv) {
//         jsbuild = jsbuild
//             .pipe(sourcemaps.init())
//             // .pipe(stripdebug())
//             .pipe(babel())
//             .pipe(uglify())
//             .pipe(sourcemaps.write()) // inline sourcemaps
//     }
//
//     return jsbuild.pipe(gulp.dest(jsPaths.dest))
// })



gulp.task('compileScss', () => {
  let postCssOpts = [
    autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
    mqpacker
  ]

  if (!devEnv) {
    postCssOpts.push(cssnano)
  }

  return gulp
    .src(sassPaths.src)
    .pipe(
      sass({
        outputStyle: 'nested',
        precision: 3,
        errLogToConsole: true
      })
    )
    .pipe(postcss(postCssOpts))
    .pipe(gulp.dest(sassPaths.dest))
})

gulp.task('moveWebfonts', (done) => {
  gulp.src(sassPaths.webfontsSrc).pipe(gulp.dest(sassPaths.webfontsDest))

  done()
})

// Compile / Minify SCSS
gulp.task('styles', gulp.series('moveWebfonts', 'compileScss'))

// gulp.task('bundle', (done) => {
//   return gulp.series(views, (cb) => {
//     let cmd = 'parcel build '
//     let filepaths = viewPaths.files.map(x => `./${viewPaths.dest}${x.replace(/.pug/g, '.js')}`).join(' ')
//     cmd += filepaths
//     cmd += ' --out-dir ./public/build/js --cache-dir ./public/build/cache --no-minify'
//
//     console.log(cmd)
//     gulp.src(filepaths, {
//       pwd: __basedir,
//       allowEmpty: true
//     })
//                .pipe(exec(cmd, function(err, stdout, stderr) {
//                    console.log(stdout);
//                    console.log(stderr);
//                  })
//                )
//   })
// })

// function views(cb) {
//   gulp.src(viewPaths.src)
//    .pipe(pug({
//      client: true
//    }))
//    .pipe(replace('function template(locals)', 'module.exports = function(locals)'))
//    .pipe(gulp.dest(viewPaths.dest))
//
//   return cb()
// }

// gulp.task('bundle', ['views'], (done) => {
//   let cmd = 'parcel build '
//   let filepaths = viewPaths.files.map(x => `./${viewPaths.dest}${x.replace(/.pug/g, '.js')}`).join(' ')
//   cmd += filepaths
//   cmd += ' --out-dir ./public/build/js --cache-dir ./public/build/cache --no-minify'
//
//   console.log(cmd)
//   return gulp.src(filepaths, {
//     pwd: __basedir,
//     allowEmpty: true
//   })
//              .pipe(exec(cmd, function(err, stdout, stderr) {
//                console.log(stdout);
//                console.log(stderr);
//              })
//              )
// })

gulp.task('pug-to-js', () => {
  return gulp
    .src(bundlePaths.pugSrc)
    .pipe(
      pug({
        client: true
      })
    )
    .pipe(
      replace('function template(locals)', 'module.exports = function(locals)')
    )
    .pipe(gulp.dest(bundlePaths.pugDestBase))
})

// Compile Pug views that are called on client-side
gulp.task('bundle:js', gulp.series('pug-to-js', (done) => {
  let cmd = 'parcel build '
  cmd += `${bundlePaths.jsSrc.map(x => x.replace(/.pug/g, '.js')).join(' ')}`
  cmd += ` --out-dir ${bundlePaths.jsSrcDest} --cache-dir ${
    bundlePaths.jsSrcDest
  }../cache --no-minify`

  gulp.src(bundlePaths.jsSrcDest, {
    allowEmpty: true
  })
  exec(cmd, function(err, stdout, stderr) {
    console.log(stdout)
  })

  done()
}))

// Optomize Images
gulp.task('images', () => {
  return (
    gulp
      .src(imagePaths.src)
      // .pipe(imagemin({ optimizationLevel: 5 }))
      .pipe(gulp.dest(imagePaths.dest))
  )
})


gulp.task('build', gulp.series('styles', 'images', 'bundle:js', 'js'))

gulp.task('nodemon', gulp.series('build', done => {
  nodemon({
    script: 'server.js',
    watch: 'src/**/*',
    tasks: ['build'],
    env: {
      NODE_ENV: 'development'
    },
    done: done
  })
}))
