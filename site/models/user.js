let mongoose = require('mongoose')
let bcrypt = require('bcrypt')
let HASH_LENGTH = 10
let Schema = mongoose.Schema

let userSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  role: {
    type: String,
    enum: ['moderator', 'editor', 'admin'],
    required: true
  },
  updated_at: {
    type: Date,
    default: Date.now(),
    select: false
  }
})

userSchema.pre('save', function(next) {
  let user = this

  // .isModified is a built-in API for mongoose
  // Do nothing if the 'password' field hasn't been modified
  if (!user.isModified('password') || user.password === '') return next()

  // If 'password' field has been modified, hash cleartext password and store
  bcrypt.hash(user.password, HASH_LENGTH, function(err, hash) {
    if (err) return next(err)

    // override cleartext password with hashed one
    user.password = hash
    next()
  })
})

// Instance Methods
userSchema.methods.verifyPassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err)
    cb(null, isMatch)
  })
}

// Turn the Schema into a Model so we can use it
let User = mongoose.model('User', userSchema)

// Make the model available to the Node application
module.exports = User
