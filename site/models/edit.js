let mongoose = require('mongoose')

let Schema = mongoose.Schema

let editSchema = new Schema({
  originalItem: {
    type: Schema.Types.ObjectId,
    ref: 'Item',
    required: true
  },
  property: {
    type: String,
    required: true
  },
  newValue: {
    type: String,
    required: true
  }
})

mongoose.Schema.Types.String.checkRequired(v => v != null)

// Turn the Schema into a Model so we can use it
let Edit = mongoose.model('Edit', editSchema)

// Make the model available to the Node application
module.exports = Edit
