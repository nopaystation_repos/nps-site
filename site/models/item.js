let mongoose = require('mongoose')
const mongooseLeanVirtuals = require('mongoose-lean-virtuals')
let Schema = mongoose.Schema

function deleteEmpty(v) {
  if (v === null) {
    return undefined
  }
  return v
}

let itemSchema = new Schema(
  {
    titleId: {
      type: String,
      required: true
    },
    contentId: {
      type: String,
      required: true
    },
    region: {
      type: String,
      enum: ['US', 'EU', 'JP', 'ASIA', 'INT', 'UNKNOWN'],
      uppercase: true
    },
    title: {
      type: String,
      default: 'UNKNOWN TITLE'
    },
    regionalTitle: {
      type: String
    },
    platform: {
      type: String,
      enum: ['PSX', 'PSP', 'PSM', 'PSV', 'PS3', 'PS4', 'UNKNOWN'],
      default: 'UNKNOWN',
      uppercase: true
    },
    category: {
      type: String,
      enum: ['Game', 'Demo', 'Update', 'Theme', 'DLC', 'Avatar', 'Unknown'],
      default: 'Unknown',
      set: v => v.capitalize(),
      get: v => v.capitalize()
    },
    pkg: {
      type: String,
      trim: true,
      default: 'MISSING'
    },
    url: {
      type: String
    },
    license: {
      type: String,
      set: deleteEmpty,
      trim: true
    },
    licenseType: {
      type: String,
      enum: ['zRIF', 'RAP'],
      required: false
    },
    subType: {
      type: String,
      enum: ['PC Engine', 'Go', 'PSP Mini', 'PSP NeoGeo', 'PS2 Classic', 'PSP Remaster', 'PSP', 'Unknown', 'Normal', 'Beta'],
    },
    minFw: {
      type: Number,
      default: 0.0
    },
    appVer: {
      type: Number,
      default: 1.0
    },
    version: {
      type: Number
    },
    fileSize: {
      type: Number
    },
    prettySize: {
      type: String
    },
    sha256: {
      type: String
    },
    productId: {
      type: String
    },
    locked: {
      type: Boolean,
      default: false
    },
    status: {
      type: String,
      enum: ['pending', 'approved'],
      default: 'pending'
    },
    completionDate: {
      type: Date,
      default: null
    },
    notes: {
      type: String,
      set: deleteEmpty
    }
  },
  {
    toJSON: {
      virtuals: true
    },
    toObject: {
      virtuals: true
    },
    timestamps: true
  }
)

itemSchema.index({contentId: 'text', title: 'text'})

itemSchema.index({titleId: 1, platform: -1, contentId: 1, category: 1, region: -1, appVer: 1, version: 1}, {unique: true})

itemSchema.post('update', function() {
  if (this.pkg !== 'MISSING' && this.license !== 'MISSING' && this.completionDate !== null) {
    this.completionDate = Date.now()
  }
  // if (this.isModified('pkg') || this.isModified('license')) {
  //   if (this.pkg !== 'MISSING' && this.license !== 'MISSING') {
  //     this.completionDate = Date.now()
  //   }
  // }
})

itemSchema.pre('save', function(next) {
  // if (this.completionDate === null || this.isModified('status')) {
  //   if ( (this.url !== 'MISSING' && this.license !== 'MISSING') || (this.url !== 'MISSING' && this.license === 'MISSING' && (this.platform === 'PSP' || this.platform === 'PSX')) ) {
  //     this.completionDate = Date.now()
  //   } else {
  //     this.completionDate = null
  //   }
  // }
  //
  // if ( (this.url !== 'MISSING' && this.license !== 'MISSING') ||
  //     (this.url !== 'MISSING' && this.license === 'MISSING' && (this.platform === 'PSP' || this.platform === 'PSX') ) ) {
  //   this.completionDate = Date.now()
  // } else {
  //   this.completionDate = null
  // }

  // if ( (this.url !== 'MISSING' && this.license !== 'MISSING') || (this.url !== 'MISSING' && this.license === 'MISSING' && (this.platform === 'PSP' || this.platform === 'PSX' || this.category === 'Demo') ) ) {
  //   this.completionDate = Date.now()
  // }
  //
  if (this.isModified('status')) {
    if ((this.pkg !== 'MISSING' && this.license !== 'MISSING') ||
      (this.pkg !== 'MISSING' && this.license === 'MISSING' && (this.platform === 'PSP' || this.platform === 'PSX')) ||
      (this.pkg !== 'MISSING' && (this.platform === 'PS3' && this.license === 'NOT REQUIRED')) ) {
      this.completionDate = Date.now()
    }
    else {
      this.completionDate = null
    }
  }

  // if (this.status === 'approved' && this.isModified('category')) {
  //   if (this.platform === 'PS3' && this.category === 'Demo') {
  //     this.license = 'NOT REQUIRED'
  //   }
  // }

  next()
})

itemSchema.virtual('licenseId').get(function() {
  return this.contentId.split('-').pop()
})

itemSchema.plugin(mongooseLeanVirtuals)

// Turn the Schema into a Model so we can use it
let Item = mongoose.model('Item', itemSchema)

// Make the model available to the Node application
module.exports = Item
