let mongoose = require('mongoose')

let Schema = mongoose.Schema

let detailSchema = new Schema({
  contentId: {
    type: String,
    required: true,
    index: true
  },
  platform: {
    type: String,
    enum: ['PSX', 'PSP', 'PSM', 'PSV', 'PS3', 'PS4', 'UNKNOWN'],
    default: 'UNKNOWN',
    uppercase: true
  },
  category: {
    type: String,
    enum: ['Game', 'Demo', 'Update', 'Theme', 'DLC', 'Avatar', 'Unknown'],
    default: 'Unknown',
    set: v => v.capitalize(),
    get: v => v.capitalize()
  },
  addOns: {
    type: [
      {
        name: {
          type: String
        },
        contentId: {
          type: String
        }
      }
    ]
  },
  themes: {
    type: [
      {
        name: {
          type: String
        },
        contentId: {
          type: String
        }
      }
    ]
  },
  cover: {
    type: String
  },
  description: {
    type: String
  },
  region: {
    type: String,
    enum: ['US', 'EU', 'JP', 'ASIA', 'INT'],
    uppercase: true
  },
  genres: {
    type: [String]
  },
  languages: {
    type: [{ codes: [String] }]
  },
  platforms: {
    type: [String]
  },
  publishDate: {
    type: Date
  },
  publisher: {
    type: String
  },
  rating: {
    type: String
  },
  size: {
    type: String
  }
})

// Turn the Schema into a Model so we can use it
let Detail = mongoose.model('Detail', detailSchema)

// Make the model available to the Node application
module.exports = Detail
