let Item = require(__basedir + '/models/item')
let cache = require('./redisClient')
let itemController = require('./item')

exports.getItems = async (req, res) => {
  if (Object.keys(req.query).length) {
    cache.get(req.originalUrl).then(async data => {
      if (!data) {
        let platform = req.query.platform || ''
        let category = req.query.category || ''
        let region = req.query.region || ''
        let query = req.query.query || ''

        let opts = {
          $or: [
            {
              title: {
                $regex: query || '',
                $options: 'i'
              }
            },
            {
              regionalTitle: {
                $regex: query || '',
                $options: 'i'
              }
            },
            {
              contentId: {
                $regex: query || '',
                $options: 'i'
              }
            }
          ],
          category: {
            $ne: 'Update'
          },
          status: 'approved'
        }

        if (category) {
          if (category === 'dlc') {
            opts.category = category.toUpperCase()
          } else {
            opts.category = category.capitalize()
          }
        } else {
          opts.category = {
            $ne: 'Update'
          }
        }

        if (region) {
          opts.region = region.toUpperCase()
        }

        if (platform && platform !== 'PS4') {
          opts.platform = platform.toUpperCase()
        } else {
          opts.platform = {
            $ne: 'PS4'
          }
        }

        if (!platform || !category) {
          res.send('Error! A platform or a category is missing.')
        } else {
          let items = await Item.find(opts)
            .collation({ locale: 'en' })
            .exec()

          // 600 = 10 minutes
          cache.setex(req.originalUrl, 600, JSON.stringify(items))

          res.json(items)
        }
      } else {
        res.json(JSON.parse(data))
      }
    })
  } else {
    // Display API readme if no parameters are sent.
    res.sendFile(__basedir + '/src/views/api-readme.txt')
  }
}

exports.lastUpdated = async (req, res) => {
  res.json({
    lastUpdated: await itemController.getLastUpdated()
  })
}
