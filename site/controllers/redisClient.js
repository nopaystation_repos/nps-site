const config = require(__basedir + '/config/config')
const Redis = require('ioredis')

module.exports = new Redis(config.redisConnection)
