let User = require(__basedir + '/models/user')
let Edit = require(__basedir + '/models/edit')
let Item = require(__basedir + '/models/item')
let Detail = require(__basedir + '/models/detail')
const fs = require('fs')
let paginate = require('express-paginate')
const formidable = require('formidable')
const Twitter = require('twitter')
const tsvUtils = require('./tsvUtils')
const cache = require('./redisClient')
const config = require(__basedir + '/config/config')

const childProcess = require('child_process')

function humanFileSize(bytes, si = false) {
  if (bytes === 0 || !bytes) return '0 B'

  var thresh = si ? 1000 : 1024
  if (Math.abs(bytes) < thresh) {
    return bytes + ' B'
  }
  var units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
  var u = -1
  do {
    bytes /= thresh
    ++u
  } while (Math.abs(bytes) >= thresh && u < units.length - 1)
  return bytes.toFixed(1) + ' ' + units[u]
}

exports.index = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let stats = {
    connections: await req.connection.server.connections,
    edits: await Edit.count(),
    contributions: await Item.find({
      status: 'pending',
      platform: { $ne: "PS4" } 
    }).count()
  }

  res.render('manage/manage', {
    stats
  })
}

exports.users = (req, res) => {
  if (req.user.role !== 'admin') {
    return res.status(401).send('Unauthorized')
  }

  User.find({}, (err, users) => {
    if (err) {
      req.flash('error', 'Something went wrong. Try again.')
      return res.redirect('/manage')
    }

    return res.render('manage/users', {
      users: users
    })
  })
}

exports.database = (req, res) => {
  return res.render('manage/database')
}

exports.changePassword = (req, res) => {
  return res.render('manage/changePassword')
}

exports.contributions = async (req, res) => {
  let qparams = req.query
  let completeness = req.query.completeness || ''
  let query = req.query.query || ''
  if (!req.user.role || req.user.role === "editor") {
    return res.status(401).send('Unauthorized')
  }

  let page = req.query.page
  let limit = req.query.limit
  let skip = req.skip

  let platform = req.query.platform || ''
  let category = req.query.category || ''

  let opts = {
    status: ['pending'],
  }

  if (query != '') {
    opts['$or'] = [
      {
        title: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        regionalTitle: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        contentId: {
          $regex: query || '',
          $options: 'i'
        }
      }
    ]
  }

  if (completeness) {
    if (completeness === 'pkg') {
      opts['pkg'] = {
        $ne: 'MISSING'
      }
    } else if (completeness === 'rapzrif') {
      opts['license'] = {
        $ne: 'MISSING'
      }
    } else if (completeness === 'pkgrapzrif') {
      opts['$and'] = [
        {
          license: {
            $ne: 'MISSING'
          }
        },
        {
          pkg: {
            $ne: 'MISSING'
          }
        }
      ]
    }
  }


  if (platform !== '' ) {
    opts["platform"] = platform.split(",")
  } else {
    opts["platform"] = {
      $ne: "PS4",
    }
  }

  if (category !== '') {
    opts["category"] = category
  } else {
    opts["category"] = {
      $ne: "Update",
    }
  }

  const [temp, totalItemCount] = await Promise.all([
    Item.find(opts)
    .skip(skip)
    .lean(false)
    .exec(),
    Item.countDocuments(opts)
  ])

  const [results, itemCount] = await Promise.all([
    Item.find(opts)
    .limit(limit)
    .skip(skip)
    .lean(false)
    .exec(),
    Item.countDocuments(opts)
  ])

  const pageCount = Math.ceil(itemCount / limit)

  let baseUrl = "/manage/contributions"

  let renderVars = {
    prevUrl:
    page - 1 > 0
    ? baseUrl + '?page=' + parseInt(page - 1) + '&limit=' + limit
    : null,
    nextUrl:
    page + 1 < pageCount
    ? baseUrl + '?page=' + parseInt(page + 1) + '&limit=' + limit
    : null,
    items: results,
    totalItemCount: totalItemCount,
    user: req.user,
    currentUrl: "contributions",
    filterParams: qparams,
    pagination: {
      limit: limit,
      currentPage: page,
      pageCount: pageCount,
      itemCount: itemCount,
      hasPreviousPages: page > 1,
      pages: paginate.getArrayPages(req)(15, pageCount, page)
    }
  }

  return res.render('manage/contributions', renderVars)
}

exports.approveContribution = (req, res) => {
  if (!req.user.role || req.user.role === "editor") {
    return res.status(401).send('Unauthorized')
  }

  let id = req.params.id

  Item.findById(id, async (err, item) => {
    if (err) {
      req.flash('error', 'Something went wrong. Try again.')
      return res.redirect('back');
    }

    item.status = 'approved'
    await item.save()

    if (item.completionDate) {
      let approvals = {}
      if (!approvals[`${item.platform} ${item.category}s`]) {
        approvals[`${item.platform} ${item.category}s`] = 0
      }
      approvals[`${item.platform} ${item.category}s`] += 1


      //await makeTweet(approvals)
      //triggerCompatPackWorkstation()
    }

    appVer = (item.appVer) ? item.appVer : 0

    if (item.version !== undefined) {
      cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}/${appVer}?version=${item.version}`)
    } else {
      cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}/${appVer}`)
    }
    cache.del('psnPercentComplete')
    cache.del('lastUpdated')

    await tsvUtils.makeTSV(item.platform, item.category)
    await tsvUtils.makePendingTSV(item.platform, item.category)

    req.flash('success', 'Contribution approved!')
    return res.redirect('back');
  })
}

exports.rejectContribution = (req, res) => {
  if (!req.user.role || req.user.role === "editor") {
    return res.status(401).send('Unauthorized')
  }

  let id = req.params.id

  Item.findByIdAndDelete(id, err => {
    if (err) {
      req.flash('error', 'Something went wrong. Try again.')
      return res.redirect('/manage/contributions')
    }
    req.flash('success', 'Contribution rejected.')
    return res.redirect('/manage/contributions')
  })
}

exports.saveEditedContribution = async (req, res) => {
  let id = req.params.id
  let keys = Object.keys(req.body)

  Item.findById(id, async (err, doc) => {
    if (err) {
      console.error(err)
      req.flash('error', 'Could not save changes to contribution.')
      return res.redirect('/manage/contributions')
    }

    keys.forEach((v, idx) => {
      if (!req.body[v]) return
      if (v === 'minFw') v = parseFloat(v)

      if (doc[v] !== req.body[v]) {
        doc[v] = req.body[v]
      }
    })

    await doc.save()

    req.flash('success', `Successfully made changes to ${doc.contentId}`)
    return res.redirect('back')
  })
}

exports.batchContributionAction = (req, res) => {
  if (!req.user.role || req.user.role === "editor") {
    return res.status(401).send('Unauthorized')
  }

  let action = req.params.action.toLowerCase()
  let ids = JSON.parse(req.body.selected)

  if (action === 'approve') {
    let approvals = {}

    let mapRes = ids.map(async id => {
      return Item.findById(id, (err, item) => {
        if (err) {
          req.flash('error', 'Something went wrong. Try again.')
          return res.redirect('back');
        }

        if ( ((item.platform === 'PSP' || item.platform === 'PSX') && item.pkg !== 'MISSING') || (item.pkg !== 'MISSING' && item.license !== 'MISSING') ) {
          if (!approvals[`${item.platform} ${item.category}s`]) {
            approvals[`${item.platform} ${item.category}s`] = 0
          }
          approvals[`${item.platform} ${item.category}s`] += 1
        }

        item.status = 'approved'
        item.save()

        appVer = (item.appVer) ? item.appVer : 0

        if (item.version !== undefined) {
          cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}/${appVer}?version=${item.version}`)
        } else {
          cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}/${appVer}`)
        }

      })
    })
    Promise.all(mapRes)
      .then(() => {
        triggerCompatPackWorkstation()
        return makeTweet(approvals)
      })
      .then(() => {
        return tsvUtils.generateTSV(approvals)
      })
      .then(() => {
        cache.del('psnPercentComplete')
        cache.del('lastUpdated')
      })
      .then(() => {
        req.flash('success', `${ids.length} contributions approved!`)
        return res.redirect('back');
      })
  } else if (action === 'reject') {
    let mapRes = ids.map(async id => {
      Item.findByIdAndDelete(id, err => {
        if (err) {
          req.flash('error', 'Something went wrong. Try again.')
          return res.redirect('back');
        }
      })
    })
    Promise.all(mapRes).then(() => {
      req.flash('success', `${ids.length} contributions rejected!`)
      return res.redirect('back');
    })
  }
}

exports.edits = (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  Edit.aggregate()
    .group({
      _id: '$originalItem',
      numEdits: {
        $sum: 1
      },
      edits: {
        $push: {
          property: '$property',
          newValue: '$newValue'
        }
      }
    })
    .lookup({
      from: 'items',
      localField: '_id',
      foreignField: '_id',
      as: 'originalItem'
    })
    .exec((err, items) => {
      if (err) {
        req.flash('error', err)
        res.redirect('/manage')
      }

      res.render('manage/edits/index', {
        items: items
      })
    })
}

exports.viewEdit = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let editId = req.params.id

  let originalItem = await Item.findById(editId).lean({virtuals: ['licenseId']})
  let edits = await Edit.find({ originalItem: editId })

  if (edits.length) {
    return res.render('manage/edits/view', {
      originalItem: originalItem,
      edits: edits
    })
  } else {
    return res.redirect('/manage/edits')
  }
}

exports.approveEdit = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let editId = req.params.id

  let edit = await Edit.findById(editId).populate('originalItem').lean(false)
  let item = edit.originalItem

  item[edit.property] = edit.newValue

  // let param = {'$set': {}}
  // param['$set'][edit.property] = edit.newValue

  await item.save(async (err, doc) => {
    if (err) {
      if (err.code === 11000) {
        // duplicate key
        req.flash('error', 'Error! Unique key validation failed. Delete conflicting item first then try again.')
        return res.redirect(`/manage/edits/${item._id}`)
      }
    } else {
      // remove item from cache so the changes are reflected
      await cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}`)

      await Edit.deleteOne({ _id: editId }, async err => {
        if (err) {
          console.error(err)
          req.flash('error', 'Edit could not be approved. Try again.')
          res.redirect(`/manage/edits/${item._id}`)
        }

        // Regenerate TSV after edit approval
        await tsvUtils.makeTSV(item.platform, item.category)
	await tsvUtils.makePendingTSV(item.platform, item.category)

        req.flash('success', 'Proposed edit successfully approved!')
        res.redirect(`/manage/edits/${item._id}`)
      })
    }
  })


  // function runUpdate(param, run = true) {
  //   if (run) {
  //     item.updateOne(param, (err, doc) => {
  //       if (err.code === 11000) {
  //         // duplicate key
  //
  //
  //         deleteDupe()
  //       }
  //     })
  //   }
  // }
  // async function deleteDupe() {
  //   await Item.deleteOne({
  //     titleId: item.titleId,
  //     platform: item.platform,
  //     contentId: item.contentId,
  //     category: item.category,
  //     region: item.region,
  //     appVer: item.appVer
  //   }, async (err, doc) => {
  //     if (err) {
  //       console.error(err)
  //     } else {
  //       await runUpdate(param, false)
  //       await Edit.deleteMany({
  //         originalItem: item._id
  //       })
  //     }
  //   })
  //
  // }
  //
  //
  // await runUpdate(param)
  // await item.updateOne(param, async (err, doc) => {
  //   if (err.code === 11000) {
  //     // duplicate key
  //
  //     let dupe = await Item.findOne({
  //         titleId: item.titleId,
  //         platform: item.platform,
  //         contentId: item.contentId,
  //         category: item.category,
  //         region: item.region,
  //         appVer: item.appVer,
  //         _id: {
  //           $ne: item._id
  //         }
  //       })
  //
  //     // Item.deleteOne({
  //     //   titleId: item.titleId,
  //     //   platform: item.platform,
  //     //   contentId: item.contentId,
  //     //   category: item.category,
  //     //   region: item.region,
  //     //   appVer: item.appVer
  //     // }, (err, doc) => {
  //     //   if (err) console.error(err)
  //     //
  //     //
  //     // })
  //   }
  //
  //   console.error(err)
  // })



}

exports.rejectEdit = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let editId = req.params.id
  let edit = await Edit.findById(editId)
  let item = edit.originalItem
  Edit.deleteOne({ _id: editId }, err => {
    if (err) {
      console.error(err)
      req.flash('error', 'Edit could not be rejected. Try again.')
      res.redirect(`/manage/edits/${item._id}`)
    }
    req.flash('success', 'Successfully rejected a proposed edit.')
    res.redirect(`/manage/edits/${item._id}`)
  })
}

exports.changeEdit = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let editId = req.params.id
  let edit = await Edit.findById(editId)

  res.render('manage/edits/edit', {
    edit: edit
  })
}

exports.saveChangedEdit = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let editId = req.params.id
  let edit = await Edit.findById(editId)
  let item = edit.originalItem

  edit.newValue = req.body.newValue
  await edit.save()

  req.flash('success', 'Property of proposed edit saved!')
  res.redirect(`/manage/edits/${item._id}`)
}

exports.deleteItem = (req, res) => {
  let itemId = req.params.itemId

  Item.findByIdAndDelete(itemId, async (err, item) => {
    if (err) {
      console.error(err)
      req.flash('error', 'Item could not be deleted. Try again.')
      res.redirect(`/browse/${itemId}`)
    }

    await Edit.deleteMany({
      originalItem: item._id
    })

    cache.del(`/view/${item.platform}/${item.titleId}/${item.licenseId}`)
    cache.del('psnPercentComplete')
    cache.del('lastUpdated')

    // Regenerate TSV after edit
    await tsvUtils.makeTSV(item.platform, item.category)
    await tsvUtils.makePendingTSV(item.platform, item.category)

    req.flash('success', 'Successfully deleted an item.')
    if (req.get('referrer') === `/view/${item.platform}/${item.titleId}/${item.licenseId}`) {
      res.redirect('/browse')
    } else {
      res.redirect(req.get('referer'))
    }
  })
}

exports.seedTSVItems = (req, res) => {
  let form = new formidable.IncomingForm()

  form.parse(req, (err, fields, files) => {
    if (err) throw err

    let splitFilename = files.tsvFile.name.replace(/.tsv/gi, '').split('_')
    let consoleType = splitFilename[0]
    let fileTypePlural = splitFilename[splitFilename.length - 1]
    let fileType = fileTypePlural.substring(0, fileTypePlural.length - 1)

    if (fileType !== 'DLC') fileType = fileType.toLowerCase()

    let fpath = files.tsvFile.path
    fs.readFile(fpath, async (err, data) => {
      if (err) throw err

      let objects = parseData(data)

      console.log(objects)

      let objArr = []

      objects.forEach(async obj => {
        if (fileType === 'update') {
          if (obj.requiredFwVersion === '3.60 or lower') {
            obj.requiredFwVersion = '3.60'
          }

          let item = new Item()
          item.titleId = obj.titleId
          item.region = obj.region
          item.title = obj.name
          item.pkg = obj.pkgDirectLink
          item.minFw = obj.requiredFwVersion
          item.appVer = obj.updateVersion
          item.fileSize = obj.fileSize === 'MISSING' ? 0 : obj.fileSize
          item.prettySize = humanFileSize(item.fileSize)
          item.sha256 = obj.sha256
          item.platform = consoleType
          item.category = fileType
          item.status = 'approved'
          item.contentId = obj.pkgDirectLink
            .split('/')
            .pop()
            .substring(0, 36)
          item.license = 'NOT REQUIRED'
          item.subType = obj.type

          item.completionDate =
            obj.lastModificationDate === null || obj.lastModificationDate === ''
              ? null
              : obj.lastModificationDate
          objArr.push(item)
        } else {
          if (!obj.contentId) return

          let item = new Item()
          item.titleId = obj.titleId
          item.region = obj.region
          item.title = obj.name
          item.pkg = obj.pkgDirectLink || obj.jsonDirectLink
          item.completionDate =
            obj.lastModificationDate === null || obj.lastModificationDate === ''
              ? null
              : obj.lastModificationDate
          item.fileSize = obj.fileSize === 'MISSING' ? 0 : obj.fileSize
          item.prettySize = humanFileSize(item.fileSize)
          item.sha256 = obj.sha256

          item.contentId = obj.contentId
          item.regionalTitle = obj.originalName
          item.minFw = obj.requiredFw
          item.appVer = obj.appVersion ? obj.appVersion : '1.00'

          item.platform = consoleType
          item.category = fileType
          item.status = 'approved'

          if (obj.rap) {
            item.licenseType = 'RAP'
            item.license = obj.rap
          } else if (obj.zrif) {
            item.licenseType = 'zRIF'
            item.license = obj.zrif
          }
          item.subType = obj.type

          objArr.push(item)
        }
      })

      Item.insertMany(objArr, async (err, docs) => {
        if (err) {
          console.error(err)
          req.flash('error', 'Error! Something went wrong. Try again.')
          return res.redirect('/manage/database')
        }

        cache.del('psnPercentComplete')
        cache.del('lastUpdated')

        // Regenerate TSV after seed
        await tsvUtils.makeTSV(consoleType, fileType)

        req.flash(
          'success',
          `Saved ${docs.length} ${consoleType} ${fileTypePlural} to database.`
        )
        res.redirect('/manage/database')
      })
    })
  })
}

function parseData(data) {
  let strData = data.toString()
  let lines = strData.split('\r\n')
  let result = []
  let headers = lines[0].split('\t')

  for (let i = 1; i < lines.length; i++) {
    let obj = {}
    let currentLine = lines[i].split('\t')

    for (let j = 0; j < headers.length; j++) {
      obj[headers[j].toCamelCase()] = currentLine[j]
    }

    result.push(obj)
  }

  return result
}

// Importing Item Details
/*
 for each file in zip:
 1. find game by content id (title of json file)
 2. if 'addons' is not an empty array, find each addon by its content id
 3. replace the content id inside the 'addons' array with a mongodb object
 4. save details as a new property on the item (item.details)
 */
exports.seedGameDetails = (req, res) => {
  let form = new formidable.IncomingForm()

  form.parse(req, (err, fields, files) => {
    if (err) throw err

    let fpath = files.detailsFile.path
    fs.readFile(fpath, (err, data) => {
      if (err) throw err

      let objects = Object.values(JSON.parse(data))
      let objArr = []

      objects.forEach(object => {
        if (object.contentType === '') {
          object.contentType = object.category
        }

        objArr.push(new Detail(object))
      })

      Detail.insertMany(objArr, (err, docs) => {
        if (err) {
          console.error(err)
          req.flash('error', 'Error! Something went wrong. Try again.')
          return res.redirect('/manage/seeddb')
        }

        req.flash(
          'success',
          `Saved ${objects.length} item details to database.`
        )
        res.redirect('/manage/seeddb')
      })
    })
  })
}

async function makeTweet(approvals) {
  // Skip making a tweet if the approvals object is empty.
  if (!Object.keys(approvals).length) return

  let tweetBody = 'NPS database has been updated!\n'

  tweetBody += Object.keys(approvals)
    .map(key => {
      if (!key.startsWith('PS4')) {
        return `${key}: +${approvals[key]}`
      }
    })
    .toString()
    .split(',')
    .join(', ')

  tweetBody += '\n#nopaystation\n' + Date.now()

  if (config.nodeEnv === 'development') {
    return Promise.resolve()
  } else {
    //let client = new Twitter({
    //  consumer_key: config.twitterConsumerKey,
    //  consumer_secret: config.twitterConsumerSecret,
    //  access_token_key: config.twitterAccessTokenKey,
    //  access_token_secret: config.twitterAccessTokenSecret
    //})

    //return client
    //  .post('statuses/update', { status: tweetBody })
    //  .catch(function(err) {
    //    throw err
    //  })
  }
}

function triggerCompatPackWorkstation() {
  const http = require('http')
  http.get('http://gateway.fast.sheridanc.on.ca/nps/api/new_games')
}

exports.dbBackup = async (req, res) => {
  let pathPrefix = `/var/www/nopaystation.com/backups`
  let folderName = Date.now()
  let path = `${pathPrefix}/${folderName}`
  const { stdout, stderr, error } = await childProcess.exec(
    `mongodump --out ${pathPrefix}/dump && cd ${pathPrefix} && zip -r ${folderName}.zip ./dump/*/** && rm -rf ./dump`
  )
  if (stderr) console.error(stderr)
  else if (error) {
    console.error(error)
  }
  setTimeout(() => {
    res.download(`${path}.zip`)
  }, 5000)
}
