const Item = require(__basedir + '/models/item')
const Parser = require('json2csv').Parser
const fs = require('fs')

exports.makeTSV = async (platform, category) => {
  if (category !== 'DLC') category = category.toLowerCase().capitalize()

  const fields = {
    titleId: {
      label: 'Title ID',
      value: 'titleId'
    },

    region: {
      label: 'Region',
      value: 'region'
    },

    title: {
      label: 'Name',
      value: 'title'
    },

    pkg: {
      label: 'PKG direct link',
      value: 'pkg'
    },

    license: {
      label: (() => {
        if (platform === 'PSV' || platform === 'PSM') return 'zRIF'
        if (platform === 'PS3' || platform === 'PSP') return 'RAP'
      })(),
      value: 'license'
    },

    contentId: {
      label: 'Content ID',
      value: 'contentId'
    },

    completionDate: {
      label: 'Last Modification Date',
      value: row => row.completionDate ? new Date(row.completionDate).toISOString().split('T').join(' ').slice(0, -5) : null
    },

    regionalTitle: {
      label: 'Original Name',
      value: 'regionalTitle'
    },

    fileSize: {
      label: 'File Size',
      value: 'fileSize'
    },

    sha256: {
      label: 'SHA256',
      value: 'sha256'
    },

    minFW: {
      label: 'Required FW',
      value: 'minFw'
    },

    appVer: {
      label: 'App Version',
      value: 'appVer'
    },

    subType: {
      label: 'Type',
      value: function(row) {
        if (row.subType === 'PSP Mini') return 'Minis'
        if (row.subType === 'PSP NeoGeo') return 'NeoGeo'
        return row.subType
      }
    }
  }

  let d = await Item.find({
    platform: platform,
    category: category,
    status: 'approved'
  })
    .collation({ locale: 'en' })
    .exec()

  if (!d) {
    console.log('no docs')
    return
  }

  let outputFields = (() => {
    if (platform === 'PSV') {
      if (
        category.toUpperCase() === 'GAME' ||
        category.toUpperCase() === 'DEMO'
      ) {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.regionalTitle,
          fields.fileSize,
          fields.sha256,
          fields.minFW,
          fields.appVer
        ]
      } else if (category.toUpperCase() === 'DLC') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.fileSize,
          fields.sha256
        ]
      } else if (category.toUpperCase() === 'THEME') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.fileSize,
          fields.sha256,
          fields.minFW
        ]
      } else if (category.toUpperCase() === 'UPDATE') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          {
            label: 'Update Version',
            value: 'appVer'
          },
          {
            label: 'Required FW VERSION',
            value: 'minFw'
          },
          fields.pkg,
          fields.completionDate,
          fields.fileSize,
          fields.sha256
        ]
      }
    } else if (platform === 'PS3') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.license,
        fields.contentId,
        fields.completionDate,
        {
          label: 'Download .RAP file',
          value: ''
        },
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSM') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.license,
        fields.contentId,
        fields.completionDate,
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSP') {
      if (category.toUpperCase() === 'GAME') {
        return [
          fields.titleId,
          fields.region,
          fields.subType,
          fields.title,
          fields.pkg,
          fields.contentId,
          fields.completionDate,
          fields.license,
          {
            label: 'Download .RAP file',
            value: ''
          },
          fields.fileSize,
          fields.sha256
        ]
      }
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.completionDate,
        fields.license,
        {
          label: 'Download .RAP file',
          value: ''
        },
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSX') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.completionDate,
        fields.regionalTitle,
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PS4') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.fileSize
      ]
    }
  })()

  const parser = new Parser({
    fields: outputFields,
    quote: '',
    delimiter: '\t',
    eol: '\r\n',
    header: true
  })
  const csv = parser.parse(d)

  // pluralize category
  category += 's'

  let filename = platform.toUpperCase() + '_' + category.toUpperCase() + '.tsv'
  let path = __basedir + '/public/tsv/' + filename
  fs.writeFile(path, csv, function(err) {
    if (err) throw err
    console.log('TSV saved!')
    return path
  })
}

exports.makePendingTSV = async (platform, category) => {
  if (category !== 'DLC') category = category.toLowerCase().capitalize()

  const fields = {
    titleId: {
      label: 'Title ID',
      value: 'titleId'
    },

    region: {
      label: 'Region',
      value: 'region'
    },

    title: {
      label: 'Name',
      value: 'title'
    },

    pkg: {
      label: 'PKG direct link',
      value: 'pkg'
    },

    license: {
      label: (() => {
        if (platform === 'PSV' || platform === 'PSM') return 'zRIF'
        if (platform === 'PS3' || platform === 'PSP') return 'RAP'
      })(),
      value: 'license'
    },

    contentId: {
      label: 'Content ID',
      value: 'contentId'
    },

    completionDate: {
      label: 'Last Modification Date',
      value: row => row.completionDate ? new Date(row.completionDate).toISOString().split('T').join(' ').slice(0, -5) : null
    },

    regionalTitle: {
      label: 'Original Name',
      value: 'regionalTitle'
    },

    fileSize: {
      label: 'File Size',
      value: 'fileSize'
    },

    sha256: {
      label: 'SHA256',
      value: 'sha256'
    },

    minFW: {
      label: 'Required FW',
      value: 'minFw'
    },

    appVer: {
      label: 'App Version',
      value: 'appVer'
    },

    subType: {
      label: 'Type',
      value: function(row) {
        if (row.subType === 'PSP Mini') return 'Minis'
        if (row.subType === 'PSP NeoGeo') return 'NeoGeo'
        return row.subType
      }
    }
  }

  let d = await Item.find({
    platform: platform,
    category: category,
    status: 'pending'
  })
    .collation({ locale: 'en' })
    .exec()

  if (!d) {
    console.log('no docs')
    return
  }

  let outputFields = (() => {
    if (platform === 'PSV') {
      if (
        category.toUpperCase() === 'GAME' ||
        category.toUpperCase() === 'DEMO'
      ) {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.regionalTitle,
          fields.fileSize,
          fields.sha256,
          fields.minFW,
          fields.appVer
        ]
      } else if (category.toUpperCase() === 'DLC') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.fileSize,
          fields.sha256
        ]
      } else if (category.toUpperCase() === 'THEME') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          fields.pkg,
          fields.license,
          fields.contentId,
          fields.completionDate,
          fields.fileSize,
          fields.sha256,
          fields.minFW
        ]
      } else if (category.toUpperCase() === 'UPDATE') {
        return [
          fields.titleId,
          fields.region,
          fields.title,
          {
            label: 'Update Version',
            value: 'appVer'
          },
          {
            label: 'Required FW VERSION',
            value: 'minFw'
          },
          fields.pkg,
          fields.completionDate,
          fields.fileSize,
          fields.sha256
        ]
      }
    } else if (platform === 'PS3') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.license,
        fields.contentId,
        fields.completionDate,
        {
          label: 'Download .RAP file',
          value: ''
        },
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSM') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.license,
        fields.contentId,
        fields.completionDate,
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSP') {
      if (category.toUpperCase() === 'GAME') {
        return [
          fields.titleId,
          fields.region,
          fields.subType,
          fields.title,
          fields.pkg,
          fields.contentId,
          fields.completionDate,
          fields.license,
          {
            label: 'Download .RAP file',
            value: ''
          },
          fields.fileSize,
          fields.sha256
        ]
      }
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.completionDate,
        fields.license,
        {
          label: 'Download .RAP file',
          value: ''
        },
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PSX') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.completionDate,
        fields.regionalTitle,
        fields.fileSize,
        fields.sha256
      ]
    } else if (platform === 'PS4') {
      return [
        fields.titleId,
        fields.region,
        fields.title,
        fields.pkg,
        fields.contentId,
        fields.fileSize
      ]
    }
  })()

  const parser = new Parser({
    fields: outputFields,
    quote: '',
    delimiter: '\t',
    eol: '\r\n',
    header: true
  })
  const csv = parser.parse(d)

  // pluralize category
  category += 's'

  let filename = platform.toUpperCase() + '_' + category.toUpperCase() + '.tsv'
  let path = __basedir + '/public/tsv/pending/' + filename
  fs.writeFile(path, csv, function(err) {
    if (err) throw err
    console.log('TSV saved!')
    return path
  })
}

exports.generateTSV = async approvals => {
  Object.keys(approvals).map(async x => {
    let arr = x.split(' ')
    let platform = arr[0]
    let category = arr[1].slice(0, -1)

    await this.makeTSV(platform, category)
  })
}

exports.getTSVPath = async query => {
  let arr = query.split('_')
  let platform = arr[0]
  let category = arr[1].slice(0, -1)
  let combinations = [
    'PSV_GAMES',
    'PSV_DEMOS',
    'PSV_DLCS',
    'PSV_THEMES',
    'PSV_UPDATES',

    'PS3_GAMES',
    'PS3_DEMOS',
    'PS3_DLCS',
    'PS3_THEMES',
    'PS3_AVATARS',

    'PSP_GAMES',
    'PSP_DLCS',
    'PSP_THEMES',
    'PSP_UPDATES',

    'PSM_GAMES',
    'PSX_GAMES',

    'PS4_GAMES',
    'PS4_DEMOS',
    'PS4_DLCS',
    'PS4_THEMES',
    'PS4_AVATARS',
  ]

  let path = __basedir + '/public/tsv/' + query + '.tsv'

  if (combinations.includes(query)) {
    if (await !fs.existsSync(path)) {
      await this.makeTSV(platform.toUpperCase(), category.toUpperCase())
      return path
    } else {
      return path
    }
  } else {
    return null
  }
}

exports.getPendingTSVPath = async query => {
  let arr = query.split('_')
  let platform = arr[0]
  let category = arr[1].slice(0, -1)
  let combinations = [
    'PSV_GAMES',
    'PSV_DEMOS',
    'PSV_DLCS',
    'PSV_THEMES',
    'PSV_UPDATES',

    'PS3_GAMES',
    'PS3_DEMOS',
    'PS3_DLCS',
    'PS3_THEMES',
    'PS3_AVATARS',

    'PSP_GAMES',
    'PSP_DLCS',
    'PSP_THEMES',
    'PSP_UPDATES',

    'PSM_GAMES',
    'PSX_GAMES',

    'PS4_GAMES',
    'PS4_DEMOS',
    'PS4_DLCS',
    'PS4_THEMES',
    'PS4_AVATARS',
  ]

  let path = __basedir + '/public/tsv/pending/' + query + '.tsv'

  if (combinations.includes(query)) {
    if (await !fs.existsSync(path)) {
      await this.makeTSV(platform.toUpperCase(), category.toUpperCase())
      return path
    } else {
      return path
    }
  } else {
    return null
  }
}
