/* global Blob */
const pako = require('pako')
const buffer = require('./buffer-bundle')

exports.rifRapTools = function() {
  let ddd = pako.inflate(
    Buffer.from('eNpjYBgFo2AU0AsYAIElGt8MRJiDCAsw3xhEmIAIU4N4AwNdRxcXZ3+/EJCAkW6Ac7C7ARwYgviuQAaIdoPSzlDaBUo7QmknIM3ACIZM78+u7kx3VWYEAGJ9HV0=', 'base64').toString('binary')
  )

  function base64ArrayBuffer(bytes) {
    let base64 = ''
    let encodings =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    let byteLength = bytes.byteLength
    let byteRemainder = byteLength % 3
    let mainLength = byteLength - byteRemainder

    let a, b, c, d
    let chunk

    // Main loop deals with bytes in chunks of 3
    for (let i = 0; i < mainLength; i = i + 3) {
      // Combine the three bytes into a single integer
      chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

      // Use bitmasks to extract 6-bit segments from the triplet
      a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
      b = (chunk & 258048) >> 12 // 258048   = (2^6 - 1) << 12
      c = (chunk & 4032) >> 6 // 4032     = (2^6 - 1) << 6
      d = chunk & 63 // 63       = 2^6 - 1

      // Convert the raw binary segments to the appropriate ASCII encoding
      base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
    }

    // Deal with the remaining bytes and padding
    if (byteRemainder === 1) {
      chunk = bytes[mainLength]

      a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

      // Set the 4 least significant bits to zero
      b = (chunk & 3) << 4 // 3   = 2^2 - 1

      base64 += encodings[a] + encodings[b] + '=='
    } else if (byteRemainder === 2) {
      chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

      a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
      b = (chunk & 1008) >> 4 // 1008  = (2^6 - 1) << 4

      // Set the 2 least significant bits to zero
      c = (chunk & 15) << 2 // 15    = 2^4 - 1

      base64 += encodings[a] + encodings[b] + encodings[c] + '='
    }

    return base64
  }

  function concatTypedArrays(a, b) {
    // a, b TypedArray of same type
    let c = new a.constructor(a.length + b.length)
    c.set(a, 0)
    c.set(b, a.length)
    return c
  }

  return {
    zrif2rif: function(zrif, cb) {
      let bin

      let contentId = ''

      let rawKey = ''

      try {
        bin = this.decode(zrif)
      } catch (e) {
        throw new Error('Error converting: ' + e)
        return
      }

      if (bin.length === 512) {
        for (let i = 0x10; i < 0x40; i++) {
          if (bin[i] === 0) break
          contentId += String.fromCharCode(bin[i])
        }

        for (let i = 0x50; i < 0x60; i++) {
          let hex = Number(bin[i]).toString(16)
          if (hex.length === 1) {
            hex = '0' + hex
          }
          rawKey += hex
        }
      } else if (bin.length === 1024) {
        for (let i = 0x50; i < 0x74; i++) {
          if (bin[i] === 0) break
          contentId += String.fromCharCode(bin[i])
        }

        for (let i = 0x100; i < 0x300; i++) {
          let hex = Number(bin[i]).toString(16)
          if (hex.length === 1) {
            hex = '0' + hex
          }
          rawKey += hex
        }
      } else {
        throw new Error('Wrong fileSize of work.bin')
        return
      }

      if (typeof cb !== 'undefined') {
        cb(bin)
      }

      let titleId = contentId.split('-')[1].split('_')[0]
      return {
        license: zrif,
        contentId: contentId,
        titleId: titleId,
        platform: this.getPlatformFromLicenseLength(zrif),
        region: this.getRegionFromTitleID(titleId),
        rawKey: rawKey
      }
    },

    getPlatformFromLicenseLength: function(license) {
      let len = license.length
      let type = 'UNKNOWN'

      if (len > 0 && len <= 32) {
        type = 'PS3'
      } else if (len >= 70 && len <= 100) {
        type = 'PSV'
      } else if (len >= 770 && len <= 790) {
        type = 'PSM'
      }

      return type
    },

    getRegionFromTitleID: function(titleId) {
      let regionChar = titleId[2]
      if (regionChar === 'A') return 'ASIA'
      else if (regionChar === 'E') return 'EU'
      else if (regionChar === 'H') return 'ASIA'
      else if (regionChar === 'J') return 'JP'
      else if (regionChar === 'K') return 'ASIA'
      else if (regionChar === 'P') return 'JP'
      else if (regionChar === 'U') return 'US'
      else if (regionChar === 'I') return 'INT'
      else if (regionChar === 'X') return 'INT'
    },

    encode: function(arr) {
      arr = pako.deflate(arr, {
        level: 9,
        windowBits: 10,
        memLevel: 8,
        dictionary: ddd
      })
      pako.inflate(arr, {
        dictionary: ddd
      })

      while (arr.length % 3 !== 0) {
        arr = concatTypedArrays(arr, new Uint8Array([0]))
      }

      return base64ArrayBuffer(arr)
    },

    decode: function(str) {
      let tmp = Buffer.from(str, 'base64').toString('binary')
      let arr = new Uint8Array(tmp.length)
      for (let i = 0; i < tmp.length; i++) {
        arr[i] = tmp.charCodeAt(i)
      }
      arr = pako.inflate(arr, {
        windowBits: 10,
        dictionary: ddd
      })
      return arr
    }
  }
}
