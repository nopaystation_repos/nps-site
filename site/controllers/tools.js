const pako = require('pako')
const path = require('path')
const rifraptools = require('./rifraptools.js').rifRapTools()
const contribute = require('./contribute.js')
let Item = require(path.join(__basedir, '/models/item'))

let ddd = pako.inflate(
  Buffer.from('eNpjYBgFo2AU0AsYAIElGt8MRJiDCAsw3xhEmIAIU4N4AwNdRxcXZ3+/EJCAkW6Ac7C7ARwYgviuQAaIdoPSzlDaBUo7QmknIM3ACIZM78+u7kx3VWYEAGJ9HV0=', 'base64')
)

function base64ArrayBuffer(bytes) {
  let base64 = ''
  let encodings =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

  let byteLength = bytes.byteLength
  let byteRemainder = byteLength % 3
  let mainLength = byteLength - byteRemainder

  let a, b, c, d
  let chunk

  // Main loop deals with bytes in chunks of 3
  for (let i = 0; i < mainLength; i = i + 3) {
    // Combine the three bytes into a single integer
    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

    // Use bitmasks to extract 6-bit segments from the triplet
    a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
    b = (chunk & 258048) >> 12 // 258048   = (2^6 - 1) << 12
    c = (chunk & 4032) >> 6 // 4032     = (2^6 - 1) << 6
    d = chunk & 63 // 63       = 2^6 - 1

    // Convert the raw binary segments to the appropriate ASCII encoding
    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
  }

  // Deal with the remaining bytes and padding
  if (byteRemainder === 1) {
    chunk = bytes[mainLength]

    a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

    // Set the 4 least significant bits to zero
    b = (chunk & 3) << 4 // 3   = 2^2 - 1

    base64 += encodings[a] + encodings[b] + '=='
  } else if (byteRemainder === 2) {
    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

    a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
    b = (chunk & 1008) >> 4 // 1008  = (2^6 - 1) << 4

    // Set the 2 least significant bits to zero
    c = (chunk & 15) << 2 // 15    = 2^4 - 1

    base64 += encodings[a] + encodings[b] + encodings[c] + '='
  }

  return base64
}

function concatTypedArrays(a, b) {
  // a, b TypedArray of same type
  let c = new a.constructor(a.length + b.length)
  c.set(a, 0)
  c.set(b, a.length)
  return c
}

exports.encode = function(arr) {
  arr = pako.deflate(arr, {
    level: 9,
    windowBits: 10,
    memLevel: 8,
    dictionary: ddd
  })
  pako.inflate(arr, {
    dictionary: ddd
  })

  while (arr.length % 3 !== 0) {
    arr = concatTypedArrays(arr, new Uint8Array([0]))
  }

  return base64ArrayBuffer(arr)
}

exports.decode = function(str) {
  let tmp = Buffer.from(str, 'base64').toString('binary')
  let arr = new Uint8Array(tmp.length)
  for (let i = 0; i < tmp.length; i++) {
    arr[i] = tmp.charCodeAt(i)
  }
  arr = pako.inflate(arr, {
    windowBits: 10,
    dictionary: ddd
  })
  return arr
}

exports.getPlatformFromLicenseLength = function(license) {
  let len = license.length
  let type = 'UNKNOWN'

  if (len > 0 && len <= 32) {
    type = 'PS3'
  } else if (len >= 70 && len <= 100) {
    type = 'PSV'
  } else if (len >= 770 && len <= 790) {
    type = 'PSM'
  }

  return type
}

exports.zrif2rif = function(zrif) {
  let bin

  // let contentId = ''

  // let rawKey = ''

  try {
    bin = this.decode(zrif)
    return Buffer.from(bin, 'binary')
  } catch (e) {
    // window.alert('Error converting: ' + e)
    console.error(e)
  }
  //
  // if (bin.length === 512) {
  //   for (let i = 0x10; i < 0x40; i++) {
  //     if (bin[i] === 0) break
  //     contentId += String.fromCharCode(bin[i])
  //   }
  //
  //   for (let i = 0x50; i < 0x60; i++) {
  //     let hex = Number(bin[i]).toString(16)
  //     if (hex.length === 1) {
  //       hex = '0' + hex
  //     }
  //     rawKey += hex
  //   }
  // } else if (bin.length === 1024) {
  //   for (let i = 0x50; i < 0x74; i++) {
  //     if (bin[i] === 0) break
  //     contentId += String.fromCharCode(bin[i])
  //   }
  //
  //   for (let i = 0x100; i < 0x300; i++) {
  //     let hex = Number(bin[i]).toString(16)
  //     if (hex.length === 1) {
  //       hex = '0' + hex
  //     }
  //     rawKey += hex
  //   }
  // } else {
  //   window.alert('Wrong fileSize of work.bin')
  //   return
  // }
  //
  // if (typeof cb !== 'undefined') {
  //   cb(bin)
  // }
  //
  // return {
  //   license: zrif,
  //   contentId: contentId,
  //   titleId: contentId.split('-')[1].split('_')[0],
  //   platform: this.getPlatformFromLicenseLength(zrif),
  //   rawKey: rawKey
  // }
}

exports.rap2file = function(rap) {
  return Buffer.from(
    (rap.match(/../g) || [])
      .map(x => {
        return String.fromCharCode(parseInt(x, 16))
      })
      .join(''),
    'binary')
  // let buf = Buffer.from(rap, 'hex')
  //
  // let blob = new Blob([buf], { type: 'application/octet-stream' })
  //
  // let url = URL.createObjectURL(blob)
  //
  // let a = window.document.createElement('a')
  // a.href = url
  // a.download = contentId + '.rap'
  // a.click()
  // URL.revokeObjectURL(url)
}

exports.validate = async (req, res) => {
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let category = req.params.category
  let appVer = req.params.appVer || 0
  let version = req.query.version

  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: category,
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let item = await Item.findOne(filter).lean({virtuals: ['licenseId']}).exec()

  if (item && item.license && item.license != "MISSING" && item.platform == "PSV") {
    try {
      contentId = rifraptools.zrif2rif(item.license).contentId
      if (contentId == item.contentId) { 
        if (item.license.length <= 128) {
          req.flash('success', 'zRIF Verified correctly')
        } else {
          req.flash('error', 'zRIF too long, most likely user zrif instead of fake zrif')
        }
      } else {
        req.flash('error', 'zRIF is incorrect, its content id is shown as ' + contentID)
      }
    } catch(e) {
      req.flash('error', e)
    }
  }

  if (item && item.pkg && item.pkg != "MISSING") {
    pkgInfo = await contribute.getPkgInfo(item.pkg)
    if (pkgInfo.pkgContentId == item.contentId) { 
      req.flash('success', 'PKG Verified correctly')
    } else {
      req.flash('error', 'PKG is incorrect, its content id is shown as ' + pkgInfo.pkgContentId)
    }

    if (pkgInfo.platform == item.platform) { 
      req.flash('success', 'Platform Verified correctly')
    } else {
      req.flash('error', 'Platform is incorrect, it is shown as ' + pkgInfo.platform)
    }
  }

  if (version) {
    res.redirect(`/view/${platform}/${titleId}/${licenseId}/${appVer}?version=${version}`)
  } else {
    res.redirect(`/view/${platform}/${titleId}/${licenseId}/${appVer}`)
  }
}
