let validator = require('validator')
let User = require(__basedir + '/models/user')

async function doesAdminAccountExist() {
  let acct = await User.findOne({
    role: 'admin'
  }).exec()

  return acct !== null
}

exports.checkIfRegistrationOpen = async (req, res) => {
  if (await doesAdminAccountExist()) {
    req.flash('error', 'Registrations are closed. Contact an Administrator.')
    res.redirect('/')
  } else {
    res.render('auth/register')
  }
}

exports.register = function(req, res) {
  let username = req.body.username || ''
  let password = req.body.password || ''
  let cPassword = req.body.cPassword || ''
  let errors = []
  let usernameIsValid = false

  let checkUsername = () => {
    return new Promise(function(resolve, reject) {
      User.findOne({ username: username }, function(err, user) {
        if (err) {
          errors.push('Something went wrong.')
        }
        if (user) {
          errors.push('A user with that username already exists.')
        } else {
          usernameIsValid = true
        }
        return resolve()
      })
    })
  }

  if (username === '' || password === '' || cPassword === '') {
    req.flash('error', 'Invalid user input.')
    res.redirect('/register')
  }

  if (validator.isAlphanumeric(username) && password === cPassword) {
    checkUsername().then(async function() {
      if (usernameIsValid) {
        let newUser = new User()
        newUser.username = username
        newUser.password = password
        newUser.updated_at = Date.now()

        if (await doesAdminAccountExist()) {
          newUser.role = req.body.role
        } else {
          newUser.role = 'admin'
        }

        newUser.save()

        req.flash('success', 'User was successfully created.')
        res.redirect('/login')
      } else {
        let message = errors.join(' ')
        res.json({
          success: false,
          message: message
        })
      }
    })
  } else {
    req.flash('error', 'Invalid user input.')
    res.redirect('/register')
  }
}
