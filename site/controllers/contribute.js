let Item = require(__basedir + '/models/item')
let Edit = require(__basedir + '/models/edit')
const workerOptions = {
  maxCallsPerWorker: 1,
  maxConcurrentCallsPerWorker: 1
}
const workerFarm = require('worker-farm')

const workers = workerFarm(
  workerOptions,
  require.resolve(__basedir + '/src/child')
)

exports.index = (req, res) => {
  res.render('contribute')
}
exports.single = (req, res) => {
  res.render('contribute/single')
}
exports.batch = (req, res) => {
  res.render('contribute/batch')
}

// exports.validate = (req, res) => {
//   let method = req.params.method.toLowerCase()
//
//   if (method === 'single') {
//     let item = req.body
//     if (item.platform === 'PSV' || item.platform === 'PSM') {
//       item.licenseType = 'zRIF'
//     } else if (
//       item.platform === 'PS3' ||
//       item.platform === 'PSP' ||
//       item.platform === 'PS4'
//     ) {
//       item.licenseType = 'RAP'
//     }
//     validateSingle(item, req, res)
//   } else if (method === 'batch') {
//     let items = req.body.items
//     validateBatch(items, req, res)
//   }
// }

// exports.submit = (req, res) => {
//   let method = req.params.method
//
//   if (method === 'single') {
//     submitSingle(req.body, req, res)
//   } else if (method === 'batch') {
//     let items = JSON.parse(req.body.serializedInput)
//     submitBatch(items, req, res)
//   }
// }

async function getPkgInfo(url) {
  return new Promise((resolve, reject) => {
    workers(url, function(err, output) {
      if (err) {
        reject(err)
      }

      console.log(output)

      if (output.stdout) {
        let results = JSON.parse(output.stdout).results

        results.platform = results.pkgPlatform
        results.category = results.pkgType

        if (results.hasOwnProperty('pkgSubType')) {
          results.subType = results.pkgSubType
        }

        resolve(results)
      } else {
        let errsplit = output.stderr.split('\n')
        reject(
          new Error(
            errsplit[errsplit.length - 2].replace('[ERROR] [INPUT] ', '')
          )
        )
      }
    })
  }).catch(e => {
    dprint(e)
    return null
  })
}

async function makeItem(params) {
  let item = new Item()

  item.title = params.title
  item.regionalTitle = params.regionalTitle
  item.titleId = params.titleId
  item.region = params.region || 'Unknown'
  item.contentId = params.contentId
  item.productId = params.productId
  item.url = params.url
  item.pkg = params.pkg
  item.fileSize = params.fileSize
  item.appVer = params.appVer || 0
  item.version = params.version || 0
  item.prettySize = params.prettySize
  item.platform = params.platform
  item.category = params.category || 'Unknown'
  item.notes = params.notes
  item.licenseType = (function() {
    if (item.platform === 'PSV' || item.platform === 'PSM') return 'zRIF'
    else if (item.platform === 'PS3' || item.platform === 'PS4') return 'RAP'
    else if (
      params.license &&
      (item.platform === 'PSP' || item.platform === 'PSX')
    )
      return 'RAP'
  })()

  if (item.platform !== 'PSP' && item.platform !== 'PSX') {
    if (!params.license) {
      params.license = 'MISSING'
    }

    if (item.platform === 'PS3' && item.category === 'Demo') {
      item.license = 'NOT REQUIRED'
    } else {
      item.license = params.license
    }
  }

  item.subType = params.subType

  if (item.platform === 'PSP' && item.category === 'Game') {
    if (!params.subType) {
      item.subType = 'PSP'
    }
  }

  if ((item.platform === 'PS3' || item.platform === 'PSV') && item.category === 'Game') {
    if (!params.subType) {
      item.subType = 'Normal'
    }
  }

  // if (item.platform === 'PS3' && item.category === 'Demo') {
  //   item.license = 'NOT REQUIRED'
  // } else {
  //   item.license = params.license
  // }

  item.minFw = params.minFw
  // if (params.platform === 'PSV' || params.platform === 'PSM') {
  // item.appVer = params.appVer || 1

  // item.license = params.license || 'MISSING'
  // }

  // if (params.license === 'MISSING' && (params.platform === 'PSP' || params.platform === 'PSX')) {
  //   item.license = null
  // }

  // if (item.license !== 'MISSING') {

  // let doc = await Item.find({
  //   contentId: item.contentId,
  //   titleId: item.titleId,
  //   region: item.region,
  //   category: item.category,
  //   platform: item.platform
  // })
  //
  // await Item.replaceOne({_id: doc._id}, item, {upsert: true}, async (err, res) => {
  //   if (err) console.log(err)
  //
  //   console.debug(res)
  //
  //   if (doc) {
  //     // Delete any pending edits for this item.
  //     await Edit.deleteMany({
  //       originalItem: doc._id
  //     })
  //   }
  // })

  return item
}

exports.finalize = async (params, ws) => {
  let sendBack = (object) => {
    if (ws.readyState === ws.OPEN) {
      ws.send(JSON.stringify(object))
    } else {
      console.error(`Error, client state is ${ws.readyState}`)
    }
  }

  try {
    let item = await makeItem(params)
    let filter = {
      contentId: item.contentId,
      titleId: item.titleId,
      region: item.region,
      category: item.category,
      platform: item.platform,
      minFw: item.minFw,
      version: item.version
    }
    let options = { upsert: true, new: true, omitUndefined: true }
    let itemToUpdate = {}
    itemToUpdate = Object.assign(itemToUpdate, item._doc)
    itemToUpdate.status = 'pending'
    delete itemToUpdate._id

    await Item.update(filter, itemToUpdate, options, async (err, doc) => {
      if (err) {
        console.error(err)
        return sendBack({
            result: 'error',
            message: err
          })
      }

      sendBack({
          result: 'success',
          item: doc
        })
    })
  } catch (error) {
    console.error(error)
    return sendBack({
        result: 'error',
        message: error
      })
    // req.flash('error', error)
    // res.redirect('/contribute')
  }
}

// async function submitSingle(params, req, res) {
//   try {
//     let item = await makeItem(params)
//     let filter = {
//       contentId: item.contentId,
//       titleId: item.titleId,
//       region: item.region,
//       category: item.category,
//       platform: item.platform,
//       minFw: item.minFw,
//       version: item.version
//     }
//     let options = { upsert: true, new: true, omitUndefined: true }
//     let itemToUpdate = {}
//     itemToUpdate = Object.assign(itemToUpdate, item._doc)
//     itemToUpdate.status = 'pending'
//     delete itemToUpdate._id
//
//     await Item.findOneAndUpdate(filter,
//       itemToUpdate,
//       options,
//       async (err, doc) => {
//         if (err) console.log(err)
//
//         console.log(doc)
//
//         req.flash('success', 'Item successfully submitted!')
//         res.redirect('/contribute')
//       })
//   } catch (error) {
//     console.error(error)
//     req.flash('error', error)
//     res.redirect('/contribute')
//   }
// }

// async function submitBatch(params, req, res) {
//   // TODO: use websockets for to handle large batches
//
//   let tally = 0
//   let options = { upsert: true, new: true, omitUndefined: true }
//   let documents = []
//
//   for (let param of params) {
//     param.status = 'pending'
//   }
//
//   const matchFields = [
//     'contentId',
//     'titleId',
//     'region',
//     'category',
//     'platform',
//     'minFw',
//     'version'
//   ]
//
//   const result = await Item.upsertMany(params, matchFields)
//
//   console.debug(result)
//
//   // for (let param of params) {
//   // let item = await makeItem(param)
//   // documents.push(await makeItem(param))
//   // let filter = {
//   //   contentId: item.contentId,
//   //   titleId: item.titleId,
//   //   region: item.region,
//   //   category: item.category,
//   //   platform: item.platform
//   // }
//   //
//   // item.status = 'pending'
//   // let itemToUpdate = {}
//   // itemToUpdate = Object.assign(itemToUpdate, item)
//   // itemToUpdate.status = 'pending'
//   // delete itemToUpdate._id
//
//   //   await Item.findOneAndUpdate(filter, item, options, (err, doc) => {
//   //     if (err) {
//   //       console.error(err)
//   //     } else {
//   //       tally += 1
//   //       console.log(doc)
//   //     }
//   //   })
//   // }
//
//   // for (let i = 0; i < params.length; i++) {
//   //   let item = await makeItem(params[i])
//   //   let filter = {
//   //     contentId: item.contentId,
//   //     titleId: item.titleId,
//   //     region: item.region,
//   //     category: item.category,
//   //     platform: item.platform
//   //   }
//   //   // let itemToUpdate = {}
//   //   // itemToUpdate = Object.assign(itemToUpdate, item)
//   //   // itemToUpdate = Object.assign(itemToUpdate, params[i])
//   //   // itemToUpdate.status = 'pending'
//   //   item.status = 'pending'
//   //   // delete itemToUpdate._id
//   //
//   //   // await Item.findOneAndUpdate(filter, itemToUpdate, options, (err, doc) => {
//   //   await Item.findOneAndUpdate(filter, item, options, (err, doc) => {
//   //     if (err) {
//   //       console.error(err)
//   //     } else {
//   //       tally += 1
//   //     }
//   //   })
//   // }
//
//   // req.flash(
//   //   'success',
//   //   // `Success! ${documents.length} items successfully submitted!`
//   //   `Success! ${tally} items successfully submitted!`
//   // )
//   // res.redirect('/contribute')
//
//   // let item = await makeItem(params[i]).toObject()
//   // delete item._id
//   // let filter = {
//   //   contentId: item.contentId,
//   //   titleId: item.titleId,
//   //   region: item.region,
//   //   category: item.category,
//   //   platform: item.platform
//   // }
//   // let doc = await Item.findOne(filter)
//   // await Item.findOneAndUpdate(filter, item, {upsert: true}, async (err, res) => {
//   //   if (err) {
//   //     console.error(err)
//   //     // req.flash('error', 'Error! Something went wrong. Try again.')
//   //     // return res.redirect('/contribute')
//   //   }
//   //
//   //   console.debug(res)
//   //
//   //   if (doc) {
//   //     // Delete any pending edits for this item.
//   //     await Edit.deleteMany({
//   //       originalItem: doc._id
//   //     })
//   //   }
//   //
//   //   tally += 1
//
//   // req.flash(
//   //   'success',
//   //   // `Success! ${documents.length} items successfully submitted!`
//   //   `Success! ${tally} items successfully submitted!`
//   // )
//   // res.redirect('/contribute')
//
//   // Bulk insert instead of saving one at a time
//   // await Item.updateMany()
//   // }
//   // await Item.insertMany(documents,{ordered: false}, (err, result) => {
//   //   let numInserted
//   //   if (err) {
//   //     console.error(err)
//   //     numInserted = err.result.result.nInserted
//   //     console.log(`${err.result.result.nInserted} items added`)
//   //     // req.flash('error', 'Error! Something went wrong. Try again.')
//   //     // return res.redirect('/contribute')
//   //   } else {
//   //     console.debug(result)
//   //     numInserted = results.insertedCount
//   //   }
//   //   req.flash(
//   //     'success',
//   //     `Success! ${numInserted} of ${documents.length} items successfully submitted!`
//   //   )
//   //   res.redirect('/contribute')
//   // })
// }

function dprint(input) {
  console.debug(input)
}

exports.validate = async (item, contribType, ws) => {

  let sendBack = (object) => {
    if (ws.readyState === ws.OPEN) {
      ws.send(JSON.stringify(object))
    } else {
      console.error(`Error, client state is ${ws.readyState}`)
    }
  }

  dprint(`Contribution Type: ${contribType.capitalize()}`)

  // PKG only
  if (!item.hasOwnProperty('license') && item.hasOwnProperty('pkg')) {
    dprint(`Contribution Method: PKG-Only`)
    dprint(`Source: ${item.pkg}`)

    dprint('Running GetInfo...')

    item.gi = await getPkgInfo(item.pkg)

    if (!item.gi) {
      dprint(`GetInfo didn't return anything.`)
      return sendBack({
        action: 'reject',
        message: `Invalid URL`,
        item: item,
        isOrphan: false
      })
    }

    dprint('GetInfo was successful!')

    dprint('Checking if item is in the database...')

    if (item.contentId === undefined) {
      item.contentId = item.gi.contentId
    }

    if (item.platform === undefined) {
      item.platform = item.gi.platform
    }

    let filter = {
      contentId: item.contentId,
      titleId: item.titleId,
      platform: item.platform,
      //appVer: item.gi.appVer,
      //version: item.gi.version,
      region: item.gi.region
    }

    if (item.gi.category === 'Game') {
      filter['$or'] = [{ category: 'Game' }, { category: 'Demo' }]
    } else {
      filter['category'] = item.gi.category
    }

    if (item.hasOwnProperty('name') && !item.gi.title) {
      item.gi.title = item.name
    }
    
    let dbCheckResult = await Item.findOne(filter).exec()

    if (!dbCheckResult) {
      dprint('No, item is NOT in the database. New entry.')

      return sendBack({
          action: 'new',
          message: 'Orphaned PKG.',
          item: item,
          isOrphan: true
        })
    }

    if(dbCheckResult.locked) {
      dprint('Item is locked')

      item.category = dbCheckResult.category
      item.gi = {}
      item.gi.title = dbCheckResult.title

      return sendBack({
          action: 'reject',
          message: 'Locked Item.',
          item: item,
          isOrphan: false
        })
    }

    // Update existing DB item with productId and PSN Store URL
    if (!dbCheckResult.productId || !dbCheckResult.url) {
      if (!dbCheckResult.productId && item.hasOwnProperty('productID')) {
        dbCheckResult.productId = item.productID
      }
      if (!dbCheckResult.url && item.hasOwnProperty('url')) {
        dbCheckResult.url = item.url
      }
      dbCheckResult.save()
    }

    if (dbCheckResult.hasOwnProperty('license')) {
      item.license = dbCheckResult.license
      item.gi.license = dbCheckResult.license
    }

    dprint('Is CID already in DB?')

    if (cidIsInDB(dbCheckResult)) {
      dprint('Yes, CID is already in DB. Does entry have PKG?')

      if (dbResultHasPkg(dbCheckResult)) {
        dprint('Yes, DB entry has PKG - item is a duplicate. Rejecting...')

        let out = {
          action: 'reject',
          message: 'PKG has already been contributed.',
          item: item,
          isOrphan: false
        }
        if (dbResultHasLicense(dbCheckResult)) {
          out.isOrphan = true
        }
        return sendBack(out)
      } else {
        dprint('No, DB entry does not have PKG. Does entry have a license?')

        if (dbResultHasLicense(dbCheckResult)) {
          dprint('Yes, DB entry has a license. Update to a DB entry.')

          return sendBack({
              action: 'update',
              message: 'PKG supplied for orphaned license',
              item: item,
              isOrphan: false
            })
        } else {
          console.debug('No, DB entry does not have PKG or license. New entry.')

          return sendBack({
              action: 'new',
              message: 'Orphaned PKG.',
              item: item,
              isOrphan: true
            })
        }
      }
    }
  }

  // License only
  else if (item.hasOwnProperty('license') && !item.hasOwnProperty('pkg')) {
    dprint(`Contribution Method: License-Only`)
    dprint(`License: ${item.license}`)
    dprint('Checking if item is already in the DB...')

    let filter = {
      contentId: item.contentId,
      titleId: item.titleId,
      category: { $ne: 'Update' }
    }

    if (item.hasOwnProperty('platform')) {
      filter['platform'] = item.platform
    }

    let dbCheckResult = await Item.findOne(filter).exec()

    if (!dbCheckResult) {
      dprint('No, item is NOT in the database. New entry.')

      let obj = {
        gi: {
          pkg: 'MISSING',
          url: null,
          fileSize: 0,
          prettySize: '0 MiB',
          minFw: 0,
          appVer: 0,
          baseGame: 'UNKNOWN',
          category: item.category || 'UNKNOWN',
          title: item.name || 'UNKNOWN TITLE',
          regionalTitle: 'UNKNOWN',
          region: item.region || 'UNKNOWN',
          contentId: item.contentId,
          titleId: item.titleId,
          platform: item.platform,
          license: item.license,
          licenseType: item.licenseType,
          subType: item.subType,
          productId: null,
          version: null
        },
        pkg: 'MISSING',
        url: null,
        fileSize: 0,
        prettySize: '0 MiB',
        minFw: 0,
        appVer: 0,
        baseGame: 'UNKNOWN',
        category: item.category || 'UNKNOWN',
        title: item.name || 'UNKNOWN TITLE',
        regionalTitle: 'UNKNOWN',
        region: item.region || 'UNKNOWN',
        contentId: item.contentId,
        titleId: item.titleId,
        platform: item.platform,
        license: item.license,
        licenseType: item.licenseType,
        subType: item.subType,
        productId: null,
        version: null
      }

      return sendBack({
          action: 'new',
          message: 'Orphaned license.',
          item: obj,
          isOrphan: true
        })
    }

    if(dbCheckResult.locked) {
      dprint('Item is locked')

      item.category = dbCheckResult.category
      item.gi = {}
      item.gi.title = dbCheckResult.title

      return sendBack({
          action: 'reject',
          message: 'Locked Item.',
          item: item,
          isOrphan: false
        })
    }

    item.gi = dbCheckResult

    if (
      item.platform === 'PSP' ||
      item.platform === 'PS3' ||
      item.gi.platform === 'PS4'
    ) {
      if (item.gi.subType) {
        item.subType = item.gi.subType
      } else if (item.gi.platform === 'PSP' && item.gi.category === 'Game') {
        item.subType = 'PSP'
        item.gi.subType = 'PSP'
      } else if (item.gi.platform === 'PS3' && item.gi.category === 'Game') {
        item.subType = 'Normal'
        item.gi.subType = 'Normal'
      }

      item.licenseType = 'RAP'
    } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
      if(item.gi.platform === "PSV") {
        item.subType = 'Normal'
        item.gi.subType = 'Normal'
      }
      item.licenseType = 'zRIF'
    }

    if (item.platform === 'PSV' && item.licenseType === 'RAP') {
      return sendBack({
          action: 'reject',
          message:
            'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.',
          item: item,
          isOrphan: false
        })
    }

    if (cidIsInDB(dbCheckResult)) {
      dprint('CID is in DB. is license missing?')

      if (dbResultHasLicense(dbCheckResult)) {
        console.debug('License is already in DB. Rejecting...')
        let out = {
          action: 'reject',
          message: 'License has already been contributed',
          item: item,
          isOrphan: false
        }

        if (!dbResultHasPkg(dbCheckResult)) {
          out.isOrphan = true
        }

        return sendBack(out)
      } else {
        dprint('License is missing from DB entry. Update.')

        item.gi.license = item.license

        return sendBack({
            action: 'update',
            message: 'License supplied for orphaned item.',
            item: item,
            isOrphan: false
          })
      }
    }
  }

  // PKG + License
  else if (item.hasOwnProperty('license') && item.hasOwnProperty('pkg')) {
    dprint(`Contribution Method: PKG + License`)
    dprint(`Source: ${item.pkg}`)
    dprint(`License: ${item.license}`)

    dprint('Running GetInfo...')

    item.gi = await getPkgInfo(item.pkg)

    if (!item.gi) {
      dprint(`GetInfo didn't return anything.`)
      return sendBack({
          action: 'reject',
          message: `Invalid PKG`,
          item: item,
          isOrphan: false
        })
    }

    dprint('GetInfo was successful!')
    if (item.gi.appVer) {
      appVer = 0
    } else {
      appVer = item.gi.appVer
    }

    let filter = {
      contentId: item.gi.contentId,
      titleId: item.gi.titleId,
      platform: item.gi.platform,
      //appVer: appVer,
      //version: item.gi.version,
      region: item.gi.region
    }

    if (item.gi.category === 'Game') {
      filter['$or'] = [{ category: 'Game' }, { category: 'Demo' }]
    } else {
      filter['category'] = item.gi.category
    }

    if (item.hasOwnProperty('name') && !item.gi.title) {
      item.gi.title = item.name
    }

    if (
      item.platform === 'PSP' ||
      item.platform === 'PS3' ||
      item.gi.platform === 'PS4'
    ) {
      if (item.gi.subType) {
        item.subType = item.gi.subType
      } else if (item.gi.platform === 'PSP' && item.gi.category === 'Game') {
        item.subType = 'PSP'
        item.gi.subType = 'PSP'
      } else if (item.gi.platform === 'PS3' && item.gi.category === 'Game') {
        item.subType = 'Normal'
        item.gi.subType = 'Normal'
      }

      item.licenseType = 'RAP'
    } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
      if(item.gi.platform === "PSV") {
        item.subType = 'Normal'
        item.gi.subType = 'Normal'
      }
      item.licenseType = 'zRIF'
    }

    dprint('Checking if item is in the database...')

    let dbCheckResult = await Item.findOne(filter).exec()

    if (!dbCheckResult) {
      dprint('No, item is NOT in the database. New entry.')

      return sendBack({
          action: 'new',
          message: '',
          item: item,
          isOrphan: false
        })
    }

    if(dbCheckResult.locked) {
      dprint('Item is locked')

      item.category = dbCheckResult.category
      item.gi = {}
      item.gi.title = dbCheckResult.title

      return sendBack({
          action: 'reject',
          message: 'Locked Item.',
          item: item,
          isOrphan: false
        })
    }

    // Update existing DB item with productId and PSN Store URL
    //if (!dbCheckResult.productId || !dbCheckResult.url) {
    //  if (!dbCheckResult.productId && item.hasOwnProperty('productID')) {
    //    dbCheckResult.productId = item.productID
    //  }
    //  if (!dbCheckResult.url && item.hasOwnProperty('url')) {
    //    dbCheckResult.url = item.url
    //  }
    //  dbCheckResult.save()
    //}

    item.gi.license = item.license

    dprint('Is CID already in DB?')

    if (cidIsInDB(dbCheckResult)) {
      dprint('Yes, CID is already in DB. Does entry have PKG?')

      if (dbResultHasPkg(dbCheckResult)) {
        dprint('Yes. PKG is in DB. Is the license missing?')

        if (dbResultHasLicense(dbCheckResult)) {
          dprint('No, the license + PKG are in the DB. Rejecting...')

          return sendBack({
              action: 'reject',
              message: `Item has already been contributed.`,
              item: item,
              isOrphan: false
            })
        } else {
          dprint('Yes the license is missing. Update...')

          dbCheckResult.license = item.license

          if (item.platform === 'PSV' && item.licenseType === 'RAP') {
            dprint('zRIF/RAP content ID does not match PKG content ID. Rejecting...')

            return sendBack({
                action: 'reject',
                message:
                  'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.',
                item: item,
                isOrphan: false
              })
          }

          return sendBack({
            action: 'update',
            message: 'License supplied for orphaned item.',
            item: item,
            isOrphan: false
          })

        }
      } else {
        dprint('No, PKG is not in the DB. New entry.')

        if (item.gi.contentId === item.contentId) {
          return sendBack({
              action: 'new',
              item: item,
              isOrphan: false
            })
        } else {
          dprint('zRIF/RAP content ID does not match PKG content ID. Rejecting...')

          return sendBack({
              action: 'rejected',
              message: 'zRIF/RAP content ID does not match PKG content ID',
              item: item,
              isOrphan: true
            })
        }
      }
    } else {
      dprint('No, the CID is not in the DB. This is a new contribution.')

      if (item.gi.contentId === item.contentId) {
        return sendBack({
            action: 'new',
            item: item,
            isOrphan: false
          })
      } else {
        dprint('zRIF/RAP content ID does not match PKG content ID. Rejecting...')
        return sendBack({
            action: 'rejected',
            message: 'zRIF/RAP content ID does not match PKG content ID',
            item: item,
            isOrphan: true
          })
      }
    }
  }
}

// exports.validateContrib = async (item, ws) => {
//   // url only
//   if (!item.license && item.pkg) {
//     console.debug('Contribution Type: Batch/URL-only')
//     console.debug('Is CID already in DB?')
//
//     item.gi = await getPkgInfo(item.pkg).catch(e => {
//       let output = {
//         action: 'reject',
//         message: `Invalid URL - ${e.message}`,
//         item: item,
//         isOrphan: false
//       }
//       ws.send(JSON.stringify(output))
//     })
//
//     if (!item.gi) {
//       return
//     }
//
//     item.gi.platform = item.gi.pkgPlatform
//     item.gi.category = item.gi.pkgType
//
//     let opts = {
//       contentId: item.gi.contentId,
//       titleId: item.gi.titleId,
//       platform: item.gi.platform,
//       appVer: item.gi.appVer,
//       version: item.gi.version,
//       region: item.gi.region
//     }
//
//     if (item.gi.category === 'Game') {
//       opts['$or'] = [{ category: 'Game' }, { category: 'Demo' }]
//     } else {
//       opts['category'] = item.gi.category
//     }
//
//     let dbCheckResult = await Item.findOne(opts).exec()
//
//     // item.gi = {
//     //   url: item.url,
//     //   fileSize: 0,
//     //   prettySize: 0,
//     //   minFw: 0,
//     //   baseGame: item.baseGame || 'UNKNOWN',
//     //   category: item.category || 'UNKNOWN',
//     //   title: item.name || 'UNKNOWN TITLE',
//     //   region: item.region || 'UNKNOWN',
//     //   contentId: item.contentId,
//     //   titleId: item.titleId,
//     //   platform: item.platform,
//     //   licenseType: item.licenseType
//     // }
//     //
//     // if (item.platform !== 'PSP' || item.platform !== 'PSX') {
//     //   item.gi.license = 'MISSING'
//     // }
//     // } else {
//
//     if (item.platform !== 'PSP' || item.platform !== 'PSX') {
//       item.gi.license = 'MISSING'
//
//       if (item.platform === 'PS3' && item.category === 'Demo') {
//         item.gi.license = 'NOT REQUIRED'
//       }
//     }
//
//     if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3') {
//       if (item.gi.pkgSubType) {
//         item.subType = item.gi.pkgSubType
//         item.gi.subType = item.gi.pkgSubType
//       } else {
//         item.subType = 'PSP'
//         item.gi.subType = 'PSP'
//       }
//
//       item.licenseType = 'RAP'
//
//       if (item.gi.platform === 'PS3' && item.gi.category === 'Demo') {
//         item.gi.license = 'NOT REQUIRED'
//       }
//     } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//       item.licenseType = 'zRIF'
//     }
//     // item.gi.license = 'MISSING'
//
//     // item.subType = item.gi.pkgSubType
//     // }
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('Yes, CID is already in DB. Does entry have URL?')
//
//       // Update existing DB item with productId and PSN Store URL
//       if (!dbCheckResult.productId || !dbCheckResult.url) {
//         if (!dbCheckResult.productId && item.hasOwnProperty('productID')) {
//           dbCheckResult.productId = item.productID
//         }
//         if (!dbCheckResult.url && item.hasOwnProperty('url')) {
//           dbCheckResult.url = item.url
//         }
//         dbCheckResult.save()
//       }
//
//       if (dbResultHasPkg(dbCheckResult)) {
//         console.debug('Yes, db entry has URL. Item is a duplicate.')
//
//         ws.send(
//           JSON.stringify({
//             action: 'reject',
//             message: 'Item has already been contributed.',
//             item: item,
//             isOrphan: false
//           })
//         )
//       } else {
//         console.debug(
//           'No, db entry does not have URL. Does entry have license?'
//         )
//
//         if (dbResultHasLicense(dbCheckResult)) {
//           console.debug('Yes, db entry has license.')
//
//           item.license = dbCheckResult.license
//           item.gi.license = dbCheckResult.license
//           if (!item.gi.title) item.gi.title = item.name
//
//           if (
//             dbCheckResult.licenseType === 'zRIF' &&
//             dbCheckResult.license.length < 40
//           ) {
//             ws.send(
//               JSON.stringify({
//                 action: 'reject',
//                 message:
//                   'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.',
//                 item: item,
//                 isOrphan: false
//               })
//             )
//           } else {
//             ws.send(
//               JSON.stringify({
//                 action: 'update',
//                 message: 'URL supplied for orphaned license',
//                 item: item,
//                 isOrphan: false
//               })
//             )
//           }
//         } else {
//           console.debug('No, entry does not have URL or license. New entry.')
//
//           ws.send(
//             JSON.stringify({
//               action: 'new',
//               message: 'Orphaned URL.',
//               item: item,
//               isOrphan: true
//             })
//           )
//         }
//       }
//     } else {
//       // No, CID is NOT in the database.
//       console.debug('No, CID is NOT in the database. New entry.')
//
//       ws.send(
//         JSON.stringify({
//           action: 'new',
//           message: 'Orphaned URL.',
//           item: item,
//           isOrphan: true
//         })
//       )
//     }
//   }
//
//   // license only
//   else if (item.license && !item.pkg) {
//     console.debug('Contribution Type: Batch/License-only')
//     console.debug('Is CID in DB?')
//
//     let dbCheckResult = await Item.findOne({
//       contentId: item.contentId,
//       titleId: item.titleId,
//       // platform: item.platform,
//       category: { $ne: 'Update' }
//     }).exec()
//
//     item.gi = dbCheckResult
//
//     if (item.license && (item.platform === 'PSP' || item.platform === 'PS3' || item.platform === 'PSX')) {
//       item.licenseType = 'RAP'
//     }
//
//     if (item.platform === 'PSP' || item.platform === 'PS3') {
//       if (item.gi) {
//         if (item.gi.subType) item.subType = item.gi.subType
//       }
//     }
//
//     // if (item.platform === 'PSV' && item.license.length < 40) {
//     if (item.platform === 'PSV' && item.licenseType === 'RAP') {
//       return ws.send(
//         JSON.stringify({
//           action: 'reject',
//           message:
//             'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.',
//           item: item,
//           isOrphan: false
//         })
//       )
//     }
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('contentId is in DB. is license missing?')
//
//       if (!dbCheckResult.productId) {
//         if (item.hasOwnProperty('productID')) {
//           item.gi.productId = item.productID
//           dbCheckResult.productId = item.productID
//           dbCheckResult.save()
//         }
//       }
//
//       if (dbResultHasLicense(dbCheckResult)) {
//         console.debug('License is already in DB.')
//
//         ws.send(
//           JSON.stringify({
//             action: 'reject',
//             message: 'License has already been contributed',
//             item: item,
//             isOrphan: false
//           })
//         )
//       } else {
//         console.debug('License is missing from entry.')
//
//         item.gi.license = item.license
//
//         ws.send(
//           JSON.stringify({
//             action: 'update',
//             message: 'License supplied for orphaned item.',
//             item: item,
//             isOrphan: false
//           })
//         )
//       }
//     } else {
//       console.debug('ContentId is not in DB.')
//       let obj = {
//         gi: {
//           pkg: item.pkg || 'MISSING',
//           url: item.url,
//           fileSize: 0,
//           prettySize: 0,
//           minFw: 0,
//           appVer: 0,
//           baseGame: 'UNKNOWN',
//           category: item.category || 'UNKNOWN',
//           title: item.name || 'UNKNOWN TITLE',
//           regionalTitle: 'UNKNOWN',
//           region: item.region || 'UNKNOWN',
//           contentId: item.contentId,
//           titleId: item.titleId,
//           platform: item.platform,
//           license: item.license,
//           licenseType: item.licenseType,
//           subType: item.subType,
//           productId: null,
//           version: null
//         },
//         pkg: item.pkg || 'MISSING',
//         url: item.url,
//         fileSize: 0,
//         prettySize: 0,
//         minFw: 0,
//         appVer: 0,
//         baseGame: 'UNKNOWN',
//         category: item.category || 'UNKNOWN',
//         title: item.name || 'UNKNOWN TITLE',
//         regionalTitle: 'UNKNOWN',
//         region: item.region || 'UNKNOWN',
//         contentId: item.contentId,
//         titleId: item.titleId,
//         platform: item.platform,
//         license: item.license,
//         licenseType: item.licenseType,
//         subType: item.subType,
//         productId: null,
//         version: null
//       }
//
//       if (item.hasOwnProperty('productID')) {
//         obj.gi.productId = item.productID
//         obj.productId = item.productID
//       }
//       if (item.hasOwnProperty('url')) {
//         obj.gi.url = item.url
//         obj.url = item.url
//       }
//
//       ws.send(
//         JSON.stringify({
//           action: 'new',
//           message: 'Orphaned license.',
//           item: obj,
//           isOrphan: true
//         })
//       )
//     }
//   }
//
//   // url + license
//   else if (item.license && item.pkg) {
//     console.debug('Contribution Type: Batch/URL + License')
//     console.debug('Is the CID in the DB?')
//
//     let dbCheckResult = await Item.findOne({
//       contentId: item.contentId,
//       titleId: item.titleId,
//       // platform: item.platform,
//       category: { $ne: 'Update' }
//     }).exec()
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('Yes. The CID is already in the database.')
//       console.debug('Is URL in DB?')
//
//       if (!dbCheckResult.productId || !dbCheckResult.url) {
//         if (!dbCheckResult.productId && item.hasOwnProperty('productID')) {
//           dbCheckResult.productId = item.productID
//         }
//         if (!dbCheckResult.url && item.hasOwnProperty('url')) {
//           dbCheckResult.url = item.url
//         }
//         dbCheckResult.save()
//       }
//
//       if (dbCheckResult.licenseType) {
//         if (dbCheckResult.licenseType === 'zRIF' && item.license.length < 40) {
//           return ws.send(
//             JSON.stringify({
//               action: 'reject',
//               message:
//                 'License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.',
//               item: item,
//               isOrphan: false
//             })
//           )
//         }
//       }
//
//       if (dbResultHasPkg(dbCheckResult)) {
//         console.debug('Yes. URL is in DB. Is the license missing?')
//
//         if (dbResultHasLicense(dbCheckResult)) {
//           console.debug('No, the license + URL are in the DB.')
//
//           ws.send(
//             JSON.stringify({
//               action: 'reject',
//               message: 'Item has already been contributed.',
//               item: item,
//               isOrphan: false
//             })
//           )
//         } else {
//           console.debug('Yes the license is missing.')
//
//           dbCheckResult.license = item.license
//           item.gi = dbCheckResult
//
//           if (
//             item.license &&
//             (item.platform === 'PSP' ||
//               item.platform === 'PS3' ||
//               item.platform === 'PSX')
//           ) {
//             item.licenseType = 'RAP'
//           } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//             item.licenseType = 'zRIF'
//           }
//
//           if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3') {
//             if (item.gi.pkgSubType) {
//               item.subType = item.gi.pkgSubType
//               item.gi.subType = item.gi.pkgSubType
//             } else if (
//               item.gi.platform === 'PSP' &&
//               item.gi.category === 'Game'
//             ) {
//               item.subType = 'PSP'
//               item.gi.subType = 'PSP'
//             }
//           }
//
//           ws.send(
//             JSON.stringify({
//               action: 'update',
//               message: 'Supplied missing license for orphaned URL.',
//               item: item,
//               isOrphan: false
//             })
//           )
//         }
//       } else {
//         console.debug('No, URL is not in DB.')
//
//         let gi = await getPkgInfo(item.pkg).catch(e => {
//           ws.send(
//             JSON.stringify({
//               action: 'reject',
//               message: `Invalid URL - ${e.message}`,
//               item: item,
//               isOrphan: false
//             })
//           )
//         })
//
//         if (!gi) return
//
//         gi.platform = gi.pkgPlatform
//         gi.category = gi.pkgType
//         gi.license = item.license
//
//         // Compare zRIF/RAP decoded content ID to content ID from GetInfo
//         // If they do not match, the zRIF/RAP does not go with the supplied URL
//         if (gi.contentId === item.contentId) {
//           item.gi = gi
//
//           if (
//             item.license &&
//             (item.platform === 'PSP' ||
//               item.platform === 'PS3' ||
//               item.platform === 'PSX')
//           ) {
//             item.licenseType = 'RAP'
//           }
//
//           if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3') {
//             if (item.gi.pkgSubType) {
//               item.subType = item.gi.pkgSubType
//               item.gi.subType = item.gi.pkgSubType
//             }
//           }
//
//           ws.send(
//             JSON.stringify({
//               action: 'update',
//               message: '',
//               item: item,
//               isOrphan: false
//             })
//           )
//         } else {
//           ws.send(
//             JSON.stringify({
//               action: 'reject',
//               message: 'zRIF/RAP content ID does not match PKG content ID',
//               item: item,
//               isOrphan: false
//             })
//           )
//         }
//       }
//     } else {
//       console.debug('No, the CID is not in the DB. This is a new contribution.')
//       let gi = await getPkgInfo(item.url).catch(e => {
//         ws.send(
//           JSON.stringify({
//             action: 'reject',
//             message: `Invalid URL - ${e.message}`,
//             item: item,
//             isOrphan: false
//           })
//         )
//       })
//
//       if (!gi) return
//
//       gi.platform = gi.pkgPlatform
//       gi.category = gi.pkgType
//       gi.license = item.license
//
//       // Compare zRIF/RAP decoded content ID to content ID from GetInfo
//       // If they do not match, the zRIF/RAP does not go with the supplied URL
//       if (gi.contentId === item.contentId) {
//         item.gi = gi
//
//         if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3') {
//           if (item.gi.pkgSubType) {
//             item.subType = item.gi.pkgSubType
//             item.gi.subType = item.gi.pkgSubType
//           }
//         }
//
//         if (item.hasOwnProperty('productID')) {
//           item.gi.productId = item.productID
//           item.productId = item.productID
//         }
//         if (item.hasOwnProperty('url')) {
//           item.gi.url = item.url
//         }
//
//         ws.send(
//           JSON.stringify({
//             action: 'new',
//             message: '',
//             item: item,
//             isOrphan: false
//           })
//         )
//       } else {
//         ws.send(
//           JSON.stringify({
//             action: 'reject',
//             message: 'zRIF/RAP content ID does not match PKG content ID',
//             item: item,
//             isOrphan: false
//           })
//         )
//       }
//     }
//   } else {
//     item.gi = {
//       pkg: item.pkg,
//       url: item.url,
//       fileSize: 0,
//       prettySize: 0,
//       minFw: 0,
//       appVer: 0,
//       baseGame: item.baseGame || 'UNKNOWN',
//       category: item.category || 'UNKNOWN',
//       title: item.name || 'UNKNOWN TITLE',
//       regionalTitle: 'UNKNOWN',
//       region: item.region || 'UNKNOWN',
//       contentId: item.contentId,
//       titleId: item.titleId,
//       platform: item.platform,
//       license: item.license,
//       licenseType: item.licenseType,
//       productId: item.productId
//     }
//
//     if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3') {
//       if (item.gi.pkgSubType) {
//         item.subType = item.gi.pkgSubType
//         item.gi.subType = item.gi.pkgSubType
//       }
//     }
//
//     ws.send(
//       JSON.stringify({
//         action: 'reject',
//         message: 'Missing license and URL',
//         item: item,
//         isOrphan: true
//       })
//     )
//   }
// }

// exports.validateSingle = async (req, res) => {
//   let item = req.body
//
//   let render = item => {
//     return res.render('contribute/finalizeSingle', { item: item })
//   }
//
//   // url only
//   if (!item.license && item.pkg) {
//     console.debug('Contribution Type: Single/URL-only')
//     console.debug('Is CID already in DB?')
//
//     item.gi = await getPkgInfo(item.pkg).catch(e => {
//       req.flash('error', `Error! Requested URL returns ${e.message}.`)
//       return res.redirect('/contribute/single')
//     })
//
//     item.gi.platform = item.gi.pkgPlatform
//     item.gi.category = item.gi.pkgType
//     item.gi.pkg = item.pkg
//
//     if (item.platform !== 'PSP' || item.platform !== 'PSX') {
//       item.gi.license = 'MISSING'
//
//       if (item.platform === 'PS3' && item.category === 'Demo') {
//         item.gi.license = 'NOT REQUIRED'
//       }
//     }
//
//     if (
//       item.gi.platform === 'PSP' ||
//       item.gi.platform === 'PS3' ||
//       item.gi.platform === 'PSX' ||
//       item.gi.platform === 'PS4'
//     ) {
//       if (item.gi.pkgSubType) {
//         item.subType = item.gi.pkgSubType
//         item.gi.subType = item.gi.pkgSubType
//       } else if (item.gi.platform === 'PSP' && item.gi.category === 'Game') {
//         item.subType = 'PSP'
//         item.gi.subType = 'PSP'
//       }
//
//       item.licenseType = 'RAP'
//     } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//       item.licenseType = 'zRIF'
//     }
//
//     let opts = {
//       contentId: item.gi.contentId,
//       titleId: item.gi.titleId,
//       platform: item.gi.platform,
//       version: item.gi.version,
//       minFw: item.gi.minFw
//     }
//
//     if (item.gi.category === 'Game') {
//       opts['$or'] = [{ category: 'Game' }, { category: 'Demo' }]
//     } else {
//       opts['category'] = item.gi.category
//     }
//
//     let dbCheckResult = await Item.findOne(opts).exec()
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('Yes, CID is already in DB. Does entry have URL?')
//
//       if (dbResultHasPkg(dbCheckResult)) {
//         console.debug('Yes, db entry has URL. Item is a duplicate.')
//
//         req.flash('error', 'Error! Item has already been contributed.')
//         return res.redirect('/contribute/single')
//       } else {
//         console.debug(
//           'No, db entry does not have URL. Does entry have license?'
//         )
//
//         if (dbResultHasLicense(dbCheckResult)) {
//           console.debug('Yes, db entry has license.')
//
//           item.license = dbCheckResult.license
//
//           item.gi.pkg = item.pkg
//
//           if (!item.gi.title) item.gi.title = item.name
//
//           render(item)
//         } else {
//           console.debug('No, entry does not have URL or license. New entry.')
//
//           // remove empty entry
//           // await Item.findOneAndDelete({
//           //   $and: [
//           //     {
//           //       contentId: dbCheckResult.contentId,
//           //       $or: [
//           //         { license: 'MISSING' },
//           //         { license: null },
//           //         { pkg: 'MISSING' },
//           //         { pkg: null }
//           //       ]
//           //     }
//           //   ]
//           // })
//
//           render(item)
//         }
//       }
//     } else {
//       // No, CID is NOT in the database.
//       console.debug('No, CID is NOT in the database. New entry.')
//
//       render(item)
//     }
//   }
//
//   // license only
//   else if (item.license && !item.pkg) {
//     console.debug('Contribution Type: Single/License-only')
//     console.debug('Is CID in DB?')
//
//     let dbCheckResult = await Item.findOne({
//       contentId: item.contentId,
//       titleId: item.titleId,
//       platform: item.platform,
//       category: { $ne: 'Update' }
//     }).exec()
//
//     item.gi = dbCheckResult
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('contentId is in DB. is license missing?')
//
//       if (item.gi.platform === 'PSP' || item.gi.platform === 'PS3' || item.gi.platform === 'PS4') {
//         if (item.gi.subType) {
//           item.subType = item.gi.subType
//           // item.gi.subType = item.gi.pkgSubType
//         } else if (item.gi.platform === 'PSP' && item.gi.category === 'Game') {
//           item.subType = 'PSP'
//           item.gi.subType = 'PSP'
//         }
//
//         item.licenseType = 'RAP'
//       } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//         item.licenseType = 'zRIF'
//       }
//
//       if (dbResultHasLicense(dbCheckResult)) {
//         console.debug('License is already in DB.')
//
//         req.flash('error', 'Error! License has already been contributed.')
//
//         return res.redirect('/contribute/single')
//       } else {
//         console.debug('License is missing from entry.')
//
//         item.gi.license = item.license
//
//         if (dbCheckResult.licenseType === 'zRIF' && item.license.length < 40) {
//           req.flash(
//             'error',
//             'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.'
//           )
//           return res.redirect('/contribute/single')
//         }
//
//         render(item)
//       }
//     } else {
//       console.debug('ContentId is not in DB.')
//       let obj = {
//         gi: {
//           pkg: 'MISSING',
//           fileSize: 0,
//           prettySize: 0,
//           minFw: 1,
//           baseGame: 'UNKNOWN',
//           category: 'UNKNOWN',
//           title: 'UNKNOWN TITLE',
//           region: 'UNKNOWN',
//           contentId: item.contentId,
//           titleId: item.titleId,
//           platform: item.platform,
//           license: item.license,
//           licenseType: item.licenseType
//         },
//         pkg: 'MISSING',
//         fileSize: 0,
//         prettySize: 0,
//         minFw: 1,
//         baseGame: 'UNKNOWN',
//         category: 'UNKNOWN',
//         title: 'UNKNOWN TITLE',
//         region: 'UNKNOWN',
//         contentId: item.contentId,
//         titleId: item.titleId,
//         platform: item.platform,
//         license: item.license,
//         licenseType: item.licenseType
//       }
//
//       render(obj)
//     }
//   }
//   // url + license
//   else if (item.license && item.pkg) {
//     console.debug('Contribution Type: Single/URL + License')
//     console.debug('Is the CID in the DB?')
//
//     let gi = await getPkgInfo(item.pkg).catch(e => {
//       req.flash('error', `Error! Requested URL returns ${e.message}.`)
//       return res.redirect('/contribute/single')
//     })
//
//     if (!gi) {
//       req.flash('error', `Error! A problem has occurred. Please try again.`)
//       return res.redirect('/contribute/single')
//     }
//
//     gi.platform = gi.pkgPlatform
//     gi.category = gi.pkgType
//     gi.license = item.license
//     gi.pkg = item.pkg
//
//     let opts = {
//       contentId: gi.contentId,
//       titleId: gi.titleId,
//       platform: gi.platform,
//       version: gi.version,
//       minFw: gi.minFw,
//       category: { $ne: 'Update' }
//     }
//
//     // TODO: Make sure that minFw and version are being passed back to the user
//
//     let dbCheckResult = await Item.findOne(opts).exec()
//
//     // let dbCheckResult = await Item.findOne({
//     //   contentId: item.contentId,
//     //   titleId: item.titleId,
//     //   platform: item.platform,
//     //   category: {
//     //     $ne: 'Update'
//     //   }
//     // }).exec()
//
//     if (cidIsInDB(dbCheckResult)) {
//       console.debug('Yes. The CID is already in the database.')
//       console.debug('Is URL in DB?')
//
//       if (dbResultHasPkg(dbCheckResult)) {
//         console.debug('Yes. URL is in DB. Is the license missing?')
//
//         if (dbResultHasLicense(dbCheckResult)) {
//           console.debug('No, the license + URL are in the DB.')
//           req.flash('error', 'Error! Item has already been contributed.')
//           return res.redirect('/contribute/single')
//         } else {
//           console.debug('Yes the license is missing.')
//
//           dbCheckResult.license = item.license
//           item.gi = dbCheckResult
//
//           if (
//             dbCheckResult.licenseType === 'zRIF' &&
//             dbCheckResult.license.length < 40
//           ) {
//             req.flash(
//               'error',
//               'Error! License is in .rap format. Expected format is zRIF. Dump the license with a PS Vita and try again.'
//             )
//             return res.redirect('/contribute/single')
//           }
//
//           render(item)
//         }
//       } else {
//         console.debug('No, URL is not in DB.')
//
//         // let gi = await getPkgInfo(item.pkg)
//         //   .catch(e => {
//         //     req.flash('error', `Error! Requested URL returns ${e.message}.`)
//         //     return res.redirect('/contribute/single')
//         //   })
//         //
//         // if (!gi) {
//         //   req.flash('error', `Error! A problem has occurred. Please try again.`)
//         //   return res.redirect('/contribute/single')
//         // }
//
//         // gi.platform = gi.pkgPlatform
//         // gi.category = gi.pkgType
//         // gi.license = item.license
//         // gi.pkg = item.pkg
//
//         // Compare zRIF/RAP decoded content ID to content ID from GetInfo
//         // If they do not match, the zRIF/RAP does not go with the supplied URL
//         if (gi.contentId === item.contentId) {
//           item.gi = gi
//
//           if (
//             item.gi.platform === 'PSP' ||
//             item.gi.platform === 'PS3' ||
//             item.gi.platform === 'PS4'
//           ) {
//             if (item.gi.pkgSubType) {
//               item.subType = item.gi.pkgSubType
//               item.gi.subType = item.gi.pkgSubType
//             } else if (
//               item.gi.platform === 'PSP' &&
//               item.gi.category === 'Game'
//             ) {
//               item.subType = 'PSP'
//               item.gi.subType = 'PSP'
//             }
//
//             item.licenseType = 'RAP'
//           } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//             item.licenseType = 'zRIF'
//           }
//
//           render(item)
//         } else {
//           req.flash(
//             'error',
//             'Error! zRIF/RAP content ID does not match PKG content ID'
//           )
//           return res.redirect('/contribute/single')
//         }
//       }
//     } else {
//       console.debug('No, the CID is not in the DB. This is a new contribution.')
//       // let gi = await getPkgInfo(item.pkg)
//       //   .catch(e => {
//       //     // workerFarm.end(workers)
//       //
//       //     req.flash('error', `Error! Requested URL returns ${e.message}.`)
//       //     return res.redirect('/contribute/single')
//       //   })
//       // gi.platform = gi.pkgPlatform
//       // gi.category = gi.pkgType
//       // gi.license = item.license
//
//       // Compare zRIF/RAP decoded content ID to content ID from GetInfo
//       // If they do not match, the zRIF/RAP does not go with the supplied URL
//       if (gi.contentId === item.contentId) {
//         item.gi = gi
//
//         if (
//           item.gi.platform === 'PSP' ||
//           item.gi.platform === 'PS3' ||
//           item.gi.platform === 'PS4'
//         ) {
//           if (item.gi.pkgSubType) {
//             item.subType = item.gi.pkgSubType
//             item.gi.subType = item.gi.pkgSubType
//           } else if (
//             item.gi.platform === 'PSP' &&
//             item.gi.category === 'Game'
//           ) {
//             item.subType = 'PSP'
//             item.gi.subType = 'PSP'
//           }
//
//           item.licenseType = 'RAP'
//         } else if (item.gi.platform === 'PSV' || item.gi.platform === 'PSM') {
//           item.licenseType = 'zRIF'
//         }
//
//         render(item)
//       } else {
//         req.flash(
//           'error',
//           'Error! zRIF/RAP content ID does not match PKG content ID'
//         )
//         return res.redirect('/contribute/single')
//       }
//     }
//   }
// }

function cidIsInDB(dbCheckResult) {
  return Boolean(dbCheckResult)
}

function dbResultHasPkg(dbCheckResult) {
  return Boolean(dbCheckResult.pkg && dbCheckResult.pkg !== 'MISSING')
}

function dbResultHasLicense(dbCheckResult) {
  if (!dbCheckResult.license) return false
  if (dbCheckResult.platform === 'PSP' || dbCheckResult.platform === 'PSX')
    return true

  return Boolean(
    dbCheckResult.license !== 'MISSING' &&
      dbCheckResult.license !== 'NOT REQUIRED' &&
      dbCheckResult.license
  )
}

exports.getPkgInfo = getPkgInfo
