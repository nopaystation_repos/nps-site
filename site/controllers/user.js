let User = require(__basedir + '/models/user')
exports.deleteUser = (req, res) => {
  let id = req.params.id

  User.deleteOne({ _id: id }, err => {
    if (err) {
      req.flash('error', 'Could not find a user with that ID. Not deleted.')
      return res.redirect('/manage/users')
    }
    req.flash('success', 'User deleted!')
    res.redirect('/manage/users')
  })
}

exports.updateUser = (req, res) => {
  let id = req.params.id

  // console.log(req.body)

  // User.findOne(id, (err) => {
  //   if (err) {
  //     req.flash('error', 'Could not find a user with that ID. Not updated.')
  //     return res.redirect('/manage/users')
  //   }
  //   req.flash('success', 'User updated!')
  //   res.redirect('/manage/users')
  // })

  User.findOneAndUpdate({ _id: id }, req.body, {}, err => {
    if (err) {
      req.flash('error', 'Could not find a user with that ID. Not updated.')
      return res.redirect('/manage/users')
    }
    req.flash('success', 'User updated!')
    res.redirect('/manage/users')
  })
}


exports.updatePassword = (req, res) => {
  let newPass = req.body.newPassword
  let cNewPass = req.body.cNewPassword

  if (newPass !== cNewPass) {
    req.flash('error', 'Confirm new password does not match new password.')
    return res.redirect('/manage/changePassword')
  }

  // console.log(req.user)

  User.findOne(req.user._id, (err, user) => {
    if (err) {
      req.flash('error', 'Could not find a user with that ID. Password not updated.')
      return res.redirect('/manage/changePassword')
    }

    user.password = newPass
    user.save((err) => {
      if (err) {
        req.flash('error', 'An error occured. Please try again.')
        return res.redirect('/manage/changePassword')
      }
      req.flash('success', 'Password updated!')
      res.redirect('/manage/changePassword')
    })
  })
}

// exports.manage = (req, res) => {
//     if (req.user.role !== 'admin') {
//         return res.status(401)
//     }
//
//     User.find({}, (err, users) => {
//         if (err) return res.status(500).json({
//             message: 'Something went wrong',
//             error: err
//         });
//
//         return res.render('manage/users', {
//             user: req.user,
//             users: users
//         })
//     })
// };
