const path = require('path')
const request = require('request')

require(path.join(__basedir, '/public/build/js/utils.js'))

let paginate = require('express-paginate')


let Item = require(path.join(__basedir, '/models/item'))
let Edit = require(path.join(__basedir, '/models/edit'))
let Detail = require(path.join(__basedir, '/models/detail'))

let cache = require('./redisClient')

exports.browsePending = async (req, res) => {
  let page = req.query.page
  let limit = req.query.limit
  let skip = req.skip

  try {
    let opts = {
      status: 'pending',
      category: {
        $ne: 'Update'
      },
      platform: {
        $ne: 'PS4'
      }
    }
    const [results, itemCount] = await Promise.all([
      Item.find(opts)
          .sort({ createdDate: -1 })
          .limit(limit)
          .skip(skip)
          .lean(false)
          .exec(),
      Item.countDocuments(opts)
    ])

    const pageCount = Math.ceil(itemCount / limit)

    let baseUrl = '/browse/pending'

    let renderVars = {
      prevUrl:
        page - 1 > 0
          ? baseUrl + '?page=' + parseInt(page - 1) + '&limit=' + limit
          : null,
      nextUrl:
        page + 1 < pageCount
          ? baseUrl + '?page=' + parseInt(page + 1) + '&limit=' + limit
          : null,
      items: results,
      pagination: {
        limit: limit,
        currentPage: page,
        pageCount: pageCount,
        itemCount: itemCount,
        hasPreviousPages: page > 1,
        pages: paginate.getArrayPages(req)(5, pageCount, page)
      }
    }

    res.render('browse/indexPending', renderVars)
  } catch (err) {
    console.error(err)
  }
}

exports.browse = async (req, res) => {
  let page = req.query.page
  let limit = req.query.limit
  let skip = req.skip

  try {
    let opts = {
      status: 'approved',
      category: {
        $ne: 'Update'
      },
      platform: {
        $ne: 'PS4'
      }
    }
    const [results, itemCount] = await Promise.all([
      Item.find(opts)
	  .sort({ completionDate: -1 })
          .limit(limit)
          .skip(skip)
          .lean(false)
          .exec(),
      Item.countDocuments(opts)
    ])
	
    const pageCount = Math.ceil(itemCount / limit)

    let baseUrl = '/browse'

    let renderVars = {
      prevUrl:
        page - 1 > 0
          ? baseUrl + '?page=' + parseInt(page - 1) + '&limit=' + limit
          : null,
      nextUrl:
        page + 1 < pageCount
          ? baseUrl + '?page=' + parseInt(page + 1) + '&limit=' + limit
          : null,
      items: results,
      pagination: {
        limit: limit,
        currentPage: page,
        pageCount: pageCount,
        itemCount: itemCount,
        hasPreviousPages: page > 1,
        pages: paginate.getArrayPages(req)(5, pageCount, page)
      }
    }

    console.log(renderVars)

    res.render('browse/index', renderVars)
  } catch (err) {
    console.error(err)
  }
}

exports.showPending = async (req, res) => {
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let appVer = req.params.appVer
  let version = req.query.version
	
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    status: 'pending',
    titleId: titleId,
    platform: platform,
    category: {
      $ne: 'Update'
    },
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let userAgent  = req.headers["user-agent"]
  cache.get(req.originalUrl).then(async data => {
    if (!data) {
      await Item.findOne(filter)
        .lean({virtuals: ['licenseId']})
        .exec()
        .then(async item => {
          // If Item doesn't exist (some DLC from game_details haven't been contributed yet.)
          if (!item) {
            return res.sendStatus(404)
          }

          item.details = await Detail.findOne({
            contentId: item.contentId
          })
            .lean()
            .populate('parent.doc')
            .exec()

          // item.details.addOns = addOns

          // item.details = await Detail.aggregate()
          //    .lookup({
          //      from: 'items',
          //      pipeline: [
          //          {$match: {
          //            contentId: item.contentId
          //            }},
          //          {$project: {
          //
          //            }}
          //        ]
          //      as: 'addOns'
          //    })

          // item.details = await Detail.findOne({contentId: item.contentId})
          //                            .lean()
          //                             .exec()

          // item.details = await Detail.aggregate()
          //   .match({contentId: item.contentId})
          //   .lookup({
          //     from: 'items',
          //     localField: 'addOns.contentId',
          //     foreignField: 'contentId',
          //     as: 'addOns'
          //   })
          //
          //   .exec()
          //
          // item.details.themes = await Detail.aggregate()
          //   .unwind()
          //   .exec()
           // .unwind('$addOns')
           //  .exec()
            // .lookup({
            //   from: 'items',
            //   localField: 'themes.contentId',
            //   foreignField: 'contentId',
            //   as: 'themes'
            // })
            // .exec()

          if (item.details) {


            // if (Array.isArray(item.details) item.details

            // item.details.addOns.forEach(async (addOn, index) => {
            //   let t = await Item.findOne({contentId: addOn.contentId})
            //     .lean()
            //     .exec()
            //
            //   console.log(t)
            // })

            // item.details.addOns = await Detail.aggregate()
            //                                   .match({contentId: item.contentId})
            //                                   .lookup({
            //                                     from: 'items',
            //                                     localField: 'contentId',
            //                                     foreignField: 'contentId',
            //                                     as: 'addOns'
            //                                   })
            //                                   .unwind('$addOns')
            //                                   .exec()

            if (!item.prettySize) {
              item.prettySize = humanFileSize(item.details.size)
            }
          }

          console.log(item)

          cache.setex(req.originalUrl, 600, JSON.stringify(item))

          res.render('browse/showPendingItem', {
            item: item,
            isVita: (userAgent.includes("PlayStation Vita") || userAgent.includes("PLAYSTATION"))
          })
        })
    } else {
      res.render('browse/showPendingItem', {
        item: JSON.parse(data)
      })
    }
  })
}

exports.show = async (req, res) => {
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: {
      $ne: 'Update'
    },
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let userAgent  = req.headers["user-agent"]

  cache.get(req.originalUrl).then(async data => {
    if (!data) {
      await Item.findOne(filter)
        .lean({virtuals: ['licenseId']})
        .exec()
        .then(async item => {
          // If Item doesn't exist (some DLC from game_details haven't been contributed yet.)
          if (!item) {
            return res.sendStatus(404)
          }

          item.details = await Detail.findOne({
            contentId: item.contentId
          })
            .lean()
            .populate('parent.doc')
            .exec()

          // item.details.addOns = addOns

          // item.details = await Detail.aggregate()
          //    .lookup({
          //      from: 'items',
          //      pipeline: [
          //          {$match: {
          //            contentId: item.contentId
          //            }},
          //          {$project: {
          //
          //            }}
          //        ]
          //      as: 'addOns'
          //    })

          // item.details = await Detail.findOne({contentId: item.contentId})
          //                            .lean()
          //                             .exec()

          // item.details = await Detail.aggregate()
          //   .match({contentId: item.contentId})
          //   .lookup({
          //     from: 'items',
          //     localField: 'addOns.contentId',
          //     foreignField: 'contentId',
          //     as: 'addOns'
          //   })
          //
          //   .exec()
          //
          // item.details.themes = await Detail.aggregate()
          //   .unwind()
          //   .exec()
           // .unwind('$addOns')
           //  .exec()
            // .lookup({
            //   from: 'items',
            //   localField: 'themes.contentId',
            //   foreignField: 'contentId',
            //   as: 'themes'
            // })
            // .exec()

          if (item.details) {


            // if (Array.isArray(item.details) item.details

            // item.details.addOns.forEach(async (addOn, index) => {
            //   let t = await Item.findOne({contentId: addOn.contentId})
            //     .lean()
            //     .exec()
            //
            //   console.log(t)
            // })

            // item.details.addOns = await Detail.aggregate()
            //                                   .match({contentId: item.contentId})
            //                                   .lookup({
            //                                     from: 'items',
            //                                     localField: 'contentId',
            //                                     foreignField: 'contentId',
            //                                     as: 'addOns'
            //                                   })
            //                                   .unwind('$addOns')
            //                                   .exec()

            if (!item.prettySize) {
              item.prettySize = humanFileSize(item.details.size)
            }
          }

          cache.setex(req.originalUrl, 600, JSON.stringify(item))

          res.render('browse/showItem', {
            item: item
          })
        })
    } else {
      res.render('browse/showItem', {
        item: JSON.parse(data),
        isVita: (userAgent.includes("PlayStation Vita") || userAgent.includes("PLAYSTATION"))
      })
    }
  })
}

exports.edit = async (req, res) => {
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let category = req.params.category
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }


  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: category,
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let item = await Item.findOne(filter).lean({virtuals: ['licenseId']}).exec()

  return res.render('browse/editItem', {
    item: item
  })
}

exports.unlock = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let category = req.params.category
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: category,
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let item = await Item.findOne(filter).lean({virtuals: ['licenseId']}).exec()
  item.locked = false

  await Item.findOneAndUpdate(filter, item, {upsert: true});

  return res.render('browse/showItem', {
    item: item
  })
}

exports.lock = async (req, res) => {
  if (!req.user.role) {
    return res.status(401).send('Unauthorized')
  }

  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let category = req.params.category
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: category,
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let item = await Item.findOne(filter).lean({virtuals: ['licenseId']}).exec()
  item.locked = true

  await Item.findOneAndUpdate(filter, item, {upsert: true});

  return res.render('browse/showItem', {
    item: item
  })
}

exports.submitEdit = async (req, res) => {
  let keys = Object.keys(req.body)
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let category = req.params.category
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    status: 'approved',
    titleId: titleId,
    platform: platform,
    category: category,
    // version: version,
    appVer: appVer,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  let prevUrl
  if (version) {
    keys.push(version)
    prevUrl = `/view/${platform}/${titleId}/${licenseId}/${appVer}?version=${version}`
  } else {
    prevUrl = `/view/${platform}/${titleId}/${licenseId}/${appVer}`
  }

  await Item.findOne(filter, (err, data) => {
      if (err || !data) {
        console.log(err)
        req.flash('error', 'An error has occurred.')
        res.redirect(`/edit/${platform}/${titleId}/${licenseId}`)
      }

      let newEditArr = []

      keys.forEach((v, idx) => {
        if (v === 'minFw' || v === 'fileSize' || v === 'version' || v === 'appVer') {
          if (Number(req.body[v]) !== null) {
            if (req.body[v] % 1 === 0) {
              // is Int
              req.body[v] = parseInt(req.body[v])
            } else if (req.body[v] % 1 !== 0) {
              // is Float
              req.body[v] = parseFloat(req.body[v])
            }
          }

        }

        if (data[v] !== req.body[v]) {
          if (!data.hasOwnProperty(v) && req.body[v] === '') {
            return
          }

          let edit = new Edit()
          edit.originalItem = data._id
          edit.property = v
          edit.newValue = req.body[v]
          newEditArr.push(edit)
        }
      })

      Edit.insertMany(newEditArr, (err, docs) => {
        if (err) {
          console.error(err)
          req.flash('error', 'Error! Something went wrong. Try again.')

          return res.redirect(prevUrl)
        }

        req.flash(
          'success',
          `Success! ${docs.length} proposed ${
            docs.length !== 1 ? 'edits are' : 'edit is'
          } now pending approval from a moderator.`
        )
        res.redirect(prevUrl)
      })
    }).lean({virtuals: ['licenseId']})
}

exports.searchItems = async (req, res) => {
  let qparams = req.query
  let missing = req.query.missing || ''
  let platform = req.query.platform || ''
  let category = req.query.category || ''
  let region = req.query.region || ''
  let query = req.query.query

  let orderBy = req.query.orderBy || ''
  let sort = req.query.sort || ''

  let limit = req.query.limit
  let page = req.query.page
  let skip = req.skip

  let opts = {
    $or: [
      {
        title: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        regionalTitle: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        contentId: {
          $regex: query || '',
          $options: 'i'
        }
      }
    ],
    category: {
      $ne: 'Update'
    },
    status: 'approved'
  }

  if (category) {
    if (category === 'dlc') {
      opts.category = category.toUpperCase()
    } else {
      opts.category = category.capitalize()
    }
  } else {
    opts.category = {
      $ne: 'Update'
    }
  }

  if (region) {
    opts.region = region.toUpperCase()
  }

  if (platform && platform !== 'PS4') {
    opts.platform = platform.toUpperCase()
  } else {
    opts.platform = {
      $ne: 'PS4'
    }
  }

  if (missing) {
    if (missing.capitalize() === 'Hide') {
      opts['$and'] = [
        {
          license: {
            $ne: 'MISSING'
          }
        },
        {
          pkg: {
            $ne: 'MISSING'
          }
        }
      ]
    } else if (missing.capitalize() === 'Only') {
      opts['$and'] = [
        {
          $or: [
            {
              license: {
                $eq: 'MISSING'
              }
            },
            {
              pkg: {
                $eq: 'MISSING'
              }
            }
          ]
        }
      ]
    }
  }

  let sortOpt = {}
  sortOpt[orderBy] = sort.toLowerCase() === 'asc' ? 1 : -1

  try {
    const [results, itemCount] = await Promise.all([
      await Item.find(opts)
          .sort(sortOpt)
          .limit(limit)
          .skip(skip)
          .lean({virtuals: ['licenseId']})
          .exec(),
      Item.countDocuments(opts)
    ])

    const pageCount = Math.ceil(itemCount / limit)

    let baseUrl =
          '/search?' +
          'query=' +
          query +
          '&platform=' +
          platform +
          '&category=' +
          category +
          '&region=' +
          region

    let userAgent  = req.headers["user-agent"]

    res.render('browse/index', {
      prevUrl:
        page - 1 > 0
          ? baseUrl +
          '&page=' + parseInt(page - 1) + '&limit=' + limit +
          '&sort=' + sort + '&orderBy=' + orderBy + '&missing=' + missing : null,
      nextUrl:
        page + 1 < pageCount
          ? baseUrl +
          '&page=' + parseInt(page + 1) + '&limit=' + limit +
          '&sort=' + sort + '&orderBy=' + orderBy + '&missing=' + missing : null,
      items: results,
      pagination: {
        limit: limit,
        currentPage: page,
        pageCount: pageCount,
        itemCount: itemCount,
        hasPreviousPages: page > 1,
        pages: paginate.getArrayPages(req)(5, pageCount, page)
      },
      user: req.user,
      searchParams: qparams,
      isVita: (userAgent.includes("PlayStation Vita") || userAgent.includes("PLAYSTATION"))
    })
  } catch (err) {
    console.error(err)
  }
}

exports.searchPendingItems = async (req, res) => {
  let qparams = req.query
  let missing = req.query.missing || ''
  let platform = req.query.platform || ''
  let category = req.query.category || ''
  let region = req.query.region || ''
  let query = req.query.query

  let orderBy = req.query.orderBy || ''
  let sort = req.query.sort || ''

  let limit = req.query.limit
  let page = req.query.page
  let skip = req.skip

  let opts = {
    $or: [
      {
        title: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        regionalTitle: {
          $regex: query || '',
          $options: 'i'
        }
      },
      {
        contentId: {
          $regex: query || '',
          $options: 'i'
        }
      }
    ],
    category: {
      $ne: 'Update'
    },
    status: 'pending'
  }

  if (category) {
    if (category === 'dlc') {
      opts.category = category.toUpperCase()
    } else {
      opts.category = category.capitalize()
    }
  } else {
    opts.category = {
      $ne: 'Update'
    }
  }

  if (region) {
    opts.region = region.toUpperCase()
  }

  if (platform && platform !== 'PS4') {
    opts.platform = platform.toUpperCase()
  } else {
    opts.platform = {
      $ne: 'PS4'
    }
  }

  if (missing) {
    if (missing.capitalize() === 'Hide') {
      opts['$and'] = [
        {
          license: {
            $ne: 'MISSING'
          }
        },
        {
          pkg: {
            $ne: 'MISSING'
          }
        }
      ]
    } else if (missing.capitalize() === 'Only') {
      opts['$and'] = [
        {
          $or: [
            {
              license: {
                $eq: 'MISSING'
              }
            },
            {
              pkg: {
                $eq: 'MISSING'
              }
            }
          ]
        }
      ]
    }
  }

  let sortOpt = {}
  sortOpt[orderBy] = sort.toLowerCase() === 'asc' ? 1 : -1

  try {
    const [results, itemCount] = await Promise.all([
      await Item.find(opts)
          .sort(sortOpt)
          .limit(limit)
          .skip(skip)
          .lean({virtuals: ['licenseId']})
          .exec(),
      Item.countDocuments(opts)
    ])

    const pageCount = Math.ceil(itemCount / limit)

    let baseUrl =
        '/search/pending?' +
          'query=' +
          query +
          '&platform=' +
          platform +
          '&category=' +
          category +
          '&region=' +
          region

    let userAgent  = req.headers["user-agent"]

    res.render('browse/indexPending', {
      prevUrl:
        page - 1 > 0
          ? baseUrl +
          '&page=' + parseInt(page - 1) + '&limit=' + limit +
          '&sort=' + sort + '&orderBy=' + orderBy + '&missing=' + missing : null,
      nextUrl:
        page + 1 < pageCount
          ? baseUrl +
          '&page=' + parseInt(page + 1) + '&limit=' + limit +
          '&sort=' + sort + '&orderBy=' + orderBy + '&missing=' + missing : null,
      items: results,
      pagination: {
        limit: limit,
        currentPage: page,
        pageCount: pageCount,
        itemCount: itemCount,
        hasPreviousPages: page > 1,
        pages: paginate.getArrayPages(req)(5, pageCount, page)
      },
      user: req.user,
      searchParams: qparams,
      isVita: (userAgent.includes("PlayStation Vita") || userAgent.includes("PLAYSTATION"))
    })
  } catch (err) {
    console.error(err)
  }
}

exports.downloadPkg = async (req, res) => {
  let titleId = req.params.titleId
  let licenseId = req.params.licenseId
  let platform = req.params.platform
  let appVer = req.params.appVer
  let version = req.query.version
  if (appVer != undefined) {
    if (isNaN((Number(appVer)))) {
      return res.sendStatus(404)
    }
  }

  if (version != undefined) {
    if (isNaN((Number(version)))) {
      return res.sendStatus(404)
    }
  }

  let filter = {
    titleId: titleId,
    appVer: appVer,
    platform: platform,
    contentId: {
      $regex: licenseId,
      $options: 'i'
    }
  }

  if (version) {
    filter['version'] = version
  }

  Item.findOne(filter, (err, item) => {
      if (err || (item.pkg == null)) {
        // return res.status(404).send('Not found.')
        res.redirect('/404.html')
      }
      if (item.pkg.endsWith('.xml')) {
        res.setHeader('content-disposition', `attachment; filename=${item.pkg.slice(item.pkg.lastIndexOf('/') + 1)}`)
        request(item.pkg).pipe(res)
      }
      else {
        res.redirect(item.pkg)
      }
    })
}

exports.counts = async () => {
  let counts = {}

  counts.contributions = await Item.count({
    status: 'pending'
  })

  counts.edits = await Edit.count()

  return counts
}

exports.getLastUpdated = async () => {
  return cache.get('lastUpdated').then(async data => {
    if (!data) {
      let res = await Item.aggregate()
        .sort({ completionDate: -1 })
        .limit(1)
        .project({ completionDate: 1 })
        .exec()

      if (res.length) {
        let time = res[0].completionDate
        cache.set('lastUpdated', time)

        return time
      } else {
        return null
      }
    } else {
      return data
    }
  })
}

exports.getPercentPSNComplete = async () => {
  return cache.get('psnPercentComplete').then(async data => {
    if (!data) {
      let regions = ['US', 'EU', 'JP', 'ASIA']
      let categories = {
        PSV: ['Game', 'DLC', 'Theme'],
        PS3: ['Game', 'DLC', 'Theme', 'Avatar'],
        PSP: ['Game', 'DLC', 'Theme'],
        PSX: ['Game'],
        PSM: ['Game']
      }
      let platforms = Object.keys(categories)

      let obj = {}

      for (let i = 0; i < platforms.length; i++) {
        let platform = platforms[i]

        obj[platform] = {
          total: 0,
          complete: 0
        }

        for (let j = 0; j < categories[platform].length; j++) {
          let category = categories[platform][j]

          obj[platform][category] = {
            total: 0,
            complete: 0
          }

          for (let k = 0; k < regions.length; k++) {
            let region = regions[k]

            obj[platform][category][region] = {
              total: await Item.countDocuments({
                region: region,
                platform: platform,
                category: category,
                pkg: {
                  $ne: 'CART ONLY'
                }
              }),
              complete: await Item.countDocuments({
                region: region,
                platform: platform,
                category: category,
                pkg: {
                  $nin: ['MISSING', 'CART ONLY']
                },
                license: {
                  $ne: 'MISSING'
                }
              })
            }

            obj[platform][category][region].percentage = (
              (obj[platform][category][region].complete /
                obj[platform][category][region].total) *
              100
            ).toFixed(2)

            obj[platform][category].total +=
              obj[platform][category][region].total
            obj[platform][category].complete +=
              obj[platform][category][region].complete
          }

          obj[platform][category].percentage = (
            (obj[platform][category].complete / obj[platform][category].total) *
            100
          ).toFixed(2)

          obj[platform].total += obj[platform][category].total
          obj[platform].complete += obj[platform][category].complete
        }

        obj[platform].percentage = (
          (obj[platform].complete / obj[platform].total) *
          100
        ).toFixed(2)
      }

      let output = JSON.stringify(obj)

      cache.set('psnPercentComplete', output)
      return output
    } else {
      return data
    }
  })
}

function humanFileSize(bytes, si = false) {
  var thresh = si ? 1000 : 1024;
  if(Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }
  var units = si
    ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
    : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while(Math.abs(bytes) >= thresh && u < units.length - 1);
  return bytes.toFixed(1)+' '+units[u];
}
