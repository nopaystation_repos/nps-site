global.__basedir = __dirname

const config = require('./config/config')
const cron = require('node-cron');
const http = require('http')
const express = require('express')
const helmet = require('helmet')
const rotatingFileStream = require('rotating-file-stream')
const logger = require('morgan')
const websocket = require('ws')
const path = require('path')
const session = require('express-session')
const flash = require('express-flash')
const passport = require('passport')

const mongoose = require('mongoose')
const MongoDBStore = require('connect-mongodb-session')(session)

// let corsProxy = require('cors-anywhere').createServer()

const app = express()

const router = require('./routes')
/*************
 * Database Connections
 *************/
const sessionStore = new MongoDBStore(
  {
    uri: config.dbConnection,
    databaseName: 'nps',
    collection: 'sessions'
  },
  error => {
    if (error) console.log(error)
  }
)
sessionStore.on('error', error => {
  console.log(error)
})

mongoose.connect(config.dbConnection, {
  useNewUrlParser: true,
  useCreateIndex: true
})
let db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => {
  console.log('> Connected to MongoDB')
})

/*************
 * Template Engine
 *************/
app.set('views', './src/views')
app.set('view engine', 'pug')

/*************
 * Logging
 *************/
let accessLogStream = rotatingFileStream('access.log', {
  mode: 0o666,
  interval: '1d', // rotate daily
  path: path.join(__basedir, 'logs')
})

app.use(
  logger(':method :url :status :response-time ms - :res[content-length]', {
    skip: function(req, res) {
      return res.statusCode < 400
    }, // skip anything less than status code 400
    stream: accessLogStream
  })
)

/*************
 * Request Middlewares
 *************/
app.use(helmet())
app.use(
  express.urlencoded({ extended: true, limit: '5mb', parameterLimit: 50000 })
)
app.use(express.json())

app.use(
  session({
    name: 'session_id',
    secret: config.sessionSecret,
    cookie: {
      maxAge: 7 * 24 * 60 * 60 * 1000 // 7 days
    },
    store: sessionStore,
    resave: true,
    saveUninitialized: false
  })
)

app.use(flash())
app.use(passport.initialize())
app.use(passport.session())

// Specify the path to our static files
app.use(express.static(path.join(__dirname, '/public')))

app.use((req, res, next) => {
  // if there's a flash message in the session request, make it available in the response, then delete it
  res.locals.sessionFlash = req.session.sessionFlash;
  delete req.session.sessionFlash;
  next();
})

/*************
 * Routes
 *************/

app.use('/', router)

// router.get('/proxy/:proxyUrl*', (req, res) => {
//   req.url = req.url.replace('/proxy/', '/')
//   corsProxy.emit('request', req, res)
// })

// app.use('/favicon.ico', function(req, res) {
//   res.sendStatus(204)
// })

// If no route has been matched by now, it must be a 404
app.use(function(req, res, next) {
  res.redirect("/404.html")
})

// app.use(function (err, req, res, next) {
//   res.render('error/500', {
//     status: err.status || 500,
//     error: err
//   })
// })

/*************
 * Server Start
 *************/
// if (cluster.isMaster) {
//   const cpuCount = os.cpus().length
//   for (let i = 0; i < cpuCount; i++) {
//     cluster.fork()
//   }
//
//   cluster.on('exit', worker => {
//     console.debug(`Mayday! Mayday! Worker ${worker.id} is no more!`)
//     cluster.fork()
//   })
// } else {

console.log(`Worker ${process.pid} started...`)


let port = config.port
let server = http.createServer(app)
const ws = new websocket.Server({
  path: '/contribute',
  server: server
})

let Contribute = require(__basedir + '/controllers/contribute')
ws.on('connection', (ws, req) => {

  ws.on('message', (msg) => {
    let jsonMsg = JSON.parse(msg)
    if (jsonMsg.action === 'validate') {
      let contribType = jsonMsg.contribType
      Contribute.validate(jsonMsg.item, contribType, ws)
    } else if (jsonMsg.action === 'finalize') {
      Contribute.finalize(jsonMsg.item, ws)
    }
  })

  ws.on('close', (e) => {
    console.debug(e)
  })

})

server.listen(port, () => {
  console.log(`> Server is listening on port ${port}`)
  console.log(`http://localhost:${port}`)
})


const tsvUtils = require('./controllers/tsvUtils')
cron.schedule('*/5 * * * *', function() {
  items = {
      PSV: ['Game', 'Demo', 'DLC', 'Theme', 'Update'],
      PS3: ['Unknown', 'Game', 'Demo', 'DLC', 'Theme', 'Avatar', 'Update'],
      PSP: ['Game', 'DLC', 'Theme', 'Update'],
      PSX: ['Game'],
      PSM: ['Game']
  }

  for(var platform in items) {
    for ( var categoryIndex in items[platform]) {
      tsvUtils.makePendingTSV(platform, items[platform][categoryIndex]).then(() => {
        console.log(`${platform} ${items[platform][categoryIndex]} pending TSV saved`)
      })
    }
  }
});

server.timeout = 600000 // 10 min
