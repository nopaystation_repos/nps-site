function pug_attr(t,e,n,f){return!1!==e&&null!=e&&(e||"class"!==t&&"style"!==t)?!0===e?" "+(f?t:t+'="'+t+'"'):("function"==typeof e.toJSON&&(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||-1===e.indexOf('"'))?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"):""}
function pug_attrs(t,r){var a="";for(var s in t)if(pug_has_own_property.call(t,s)){var u=t[s];if("class"===s){u=pug_classes(u),a=pug_attr(s,u,!1,r)+a;continue}"style"===s&&(u=pug_style(u)),a+=pug_attr(s,u,!1,r)}return a}
function pug_classes(s,r){return Array.isArray(s)?pug_classes_array(s,r):s&&"object"==typeof s?pug_classes_object(s):s||""}
function pug_classes_array(r,a){for(var s,e="",u="",c=Array.isArray(a),g=0;g<r.length;g++)(s=pug_classes(r[g]))&&(c&&a[g]&&(s=pug_escape(s)),e=e+u+s,u=" ");return e}
function pug_classes_object(r){var a="",n="";for(var o in r)o&&r[o]&&pug_has_own_property.call(r,o)&&(a=a+n+o,n=" ");return a}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_has_own_property=Object.prototype.hasOwnProperty;
var pug_match_html=/["&<>]/;
function pug_merge(e,r){if(1===arguments.length){for(var t=e[0],g=1;g<e.length;g++)t=pug_merge(t,e[g]);return t}for(var l in r)if("class"===l){var n=e[l]||[];e[l]=(Array.isArray(n)?n:[n]).concat(r[l]||[])}else if("style"===l){var n=pug_style(e[l]);n=n&&";"!==n[n.length-1]?n+";":n;var a=pug_style(r[l]);a=a&&";"!==a[a.length-1]?a+";":a,e[l]=n+a}else e[l]=r[l];return e}
function pug_rethrow(n,e,r,t){if(!(n instanceof Error))throw n;if(!("undefined"==typeof window&&e||t))throw n.message+=" on line "+r,n;try{t=t||require("fs").readFileSync(e,"utf8")}catch(e){pug_rethrow(n,null,r)}var i=3,a=t.split("\n"),o=Math.max(r-i,0),h=Math.min(a.length,r+i),i=a.slice(o,h).map(function(n,e){var t=e+o+1;return(t==r?"  > ":"    ")+t+"| "+n}).join("\n");throw n.path=e,n.message=(e||"Pug")+":"+r+"\n"+i+"\n\n"+n.message,n}
function pug_style(r){if(!r)return"";if("object"==typeof r){var t="";for(var e in r)pug_has_own_property.call(r,e)&&(t=t+e+":"+r[e]+";");return t}return r+""}module.exports = function(locals) {var pug_html = "", pug_mixins = {}, pug_interp;var pug_debug_filename, pug_debug_line;try {;var locals_for_with = (locals || {});(function (items) {;pug_debug_line = 1;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["regionDDL"] = pug_interp = function(region, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 2;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 3;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"region\""+pug_attr("name", `${contentId}[region]`, true, false)) + "\u003E";
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "US"},{ 'selected': region === "US" }]), false)) + "\u003E";
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "US\u003C\u002Foption\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "EU"},{ 'selected': region === "EU" }]), false)) + "\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "EU\u003C\u002Foption\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "JP"},{ 'selected': region === "JP" }]), false)) + "\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "JP\u003C\u002Foption\u003E";
;pug_debug_line = 7;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "ASIA"},{ 'selected': region === "ASIA" }]), false)) + "\u003E";
;pug_debug_line = 7;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "ASIA\u003C\u002Foption\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "INT"},{ 'selected': region === "INT" }]), false)) + "\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "INT\u003C\u002Foption\u003E";
;pug_debug_line = 9;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': region === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 9;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"region\" name=\"region\"\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "US"},{ 'selected': region === "US" }]), false)) + "\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "US\u003C\u002Foption\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "EU"},{ 'selected': region === "EU" }]), false)) + "\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "EU\u003C\u002Foption\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "JP"},{ 'selected': region === "JP" }]), false)) + "\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "JP\u003C\u002Foption\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "ASIA"},{ 'selected': region === "ASIA" }]), false)) + "\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "ASIA\u003C\u002Foption\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "INT"},{ 'selected': region === "INT" }]), false)) + "\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "INT\u003C\u002Foption\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': region === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["platformDDL"] = pug_interp = function(platform, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 20;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 21;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"platform\""+pug_attr("name", `${contentId}[platform]`, true, false)) + "\u003E";
;pug_debug_line = 22;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSX"},{ 'selected': platform === "PSX" }]), false)) + "\u003E";
;pug_debug_line = 22;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSX\u003C\u002Foption\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': platform === "PSP" }]), false)) + "\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS3"},{ 'selected': platform === "PS3" }]), false)) + "\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS3\u003C\u002Foption\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003C!-- option(value=\"PS4\")&attributes({ 'selected': platform === \"PS4\" }) PS4--\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSM"},{ 'selected': platform === "PSM" }]), false)) + "\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSM\u003C\u002Foption\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSV"},{ 'selected': platform === "PSV" }]), false)) + "\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSV\u003C\u002Foption\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': platform === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 30;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"platform\" name=\"platform\"\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSX"},{ 'selected': platform === "PSX" }]), false)) + "\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSX\u003C\u002Foption\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': platform === "PSP" }]), false)) + "\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS3"},{ 'selected': platform === "PS3" }]), false)) + "\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS3\u003C\u002Foption\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003C!-- option(value=\"PS4\")&attributes({ 'selected': platform === \"PS4\" }) PS4--\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSM"},{ 'selected': platform === "PSM" }]), false)) + "\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSM\u003C\u002Foption\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSV"},{ 'selected': platform === "PSV" }]), false)) + "\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSV\u003C\u002Foption\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': platform === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 39;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["categoryDDL"] = pug_interp = function(category, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 40;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 41;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"category\""+pug_attr("name", `${contentId}[category]`, true, false)) + "\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Game"},{ 'selected': category === "Game" }]), false)) + "\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Game\u003C\u002Foption\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Theme"},{ 'selected': category === "Theme" }]), false)) + "\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Theme\u003C\u002Foption\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "DLC"},{ 'selected': category === "DLC" }]), false)) + "\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "DLC\u003C\u002Foption\u003E";
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Avatar"},{ 'selected': category === "Avatar" }]), false)) + "\u003E";
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Avatar\u003C\u002Foption\u003E";
;pug_debug_line = 46;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Update"},{ 'selected': category === "Update" }]), false)) + "\u003E";
;pug_debug_line = 46;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Update\u003C\u002Foption\u003E";
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Demo"},{ 'selected': category === "Demo" }]), false)) + "\u003E";
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Demo\u003C\u002Foption\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': (category === "Unknown" || category === 'UNKNOWN') }]), false)) + "\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"category\" name=\"category\"\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Game"},{ 'selected': category === "Game" }]), false)) + "\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Game\u003C\u002Foption\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Theme"},{ 'selected': category === "Theme" }]), false)) + "\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Theme\u003C\u002Foption\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "DLC"},{ 'selected': category === "DLC" }]), false)) + "\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "DLC\u003C\u002Foption\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Avatar"},{ 'selected': category === "Avatar" }]), false)) + "\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Avatar\u003C\u002Foption\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Update"},{ 'selected': category === "Update" }]), false)) + "\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Update\u003C\u002Foption\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Demo"},{ 'selected': category === "Demo" }]), false)) + "\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Demo\u003C\u002Foption\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': (category === "Unknown" || category === 'UNKNOWN') }]), false)) + "\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 59;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";












































































;pug_debug_line = 2;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 3;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {
      for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
        var item = $$obj[pug_index0];
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.baseGame !== 'UNKNOWN') {
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.pkgType) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.baseGame) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
}
else {
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.pkgType) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.titleId) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
}
;pug_debug_line = 10;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[title]`, true, false)+pug_attr("value", item.item.name, true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.regionalTitle) {
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 18;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--if item.item.gi.regionalTitle === item.item.gi.title--\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--\tinput.form-control#regional-title(type=\"text\" name=`${item.item.gi.contentId}[regionalTitle]` value=item.item.gi.title required)--\u003E";
;pug_debug_line = 20;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--else--\u003E";
;pug_debug_line = 21;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", item.item.gi.regionalTitle, true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[url]`, true, false)+pug_attr("value", item.item.url, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 30;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", item.licenseType, true, false)) + "\u002F\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[license]`, true, false)+pug_attr("value", item.item.gi.license ? item.item.gi.license : 'MISSING', true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", item.item.gi.contentId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 38;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 38;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 39;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", item.item.gi.titleId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 41;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.region !== 'UNKNOWN') {
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"region\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[region]`, true, false)+pug_attr("value", item.item.gi.region, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["regionDDL"](item.item.gi.region, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 49;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", item.item.gi.minFw, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.pkgPlatform !== 'UNKNOWN') {
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"platform\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[platform]`, true, false)+pug_attr("value", item.item.gi.pkgPlatform, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 59;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["platformDDL"](item.item.gi.pkgPlatform, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 61;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.pkgType !== 'UNKNOWN') {
;pug_debug_line = 64;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"file-type\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[category]`, true, false)+pug_attr("value", item.item.gi.pkgType, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["categoryDDL"](item.item.gi.pkgType, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 70;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", item.item.gi.prettySize, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index0 in $$obj) {
      $$l++;
      var item = $$obj[pug_index0];
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.baseGame !== 'UNKNOWN') {
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.pkgType) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.baseGame) ? "" : pug_interp));
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
}
else {
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.pkgType) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.gi.titleId) ? "" : pug_interp));
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name) ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
}
;pug_debug_line = 10;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[title]`, true, false)+pug_attr("value", item.item.name, true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.regionalTitle) {
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 18;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--if item.item.gi.regionalTitle === item.item.gi.title--\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--\tinput.form-control#regional-title(type=\"text\" name=`${item.item.gi.contentId}[regionalTitle]` value=item.item.gi.title required)--\u003E";
;pug_debug_line = 20;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003C!--else--\u003E";
;pug_debug_line = 21;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", item.item.gi.regionalTitle, true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[url]`, true, false)+pug_attr("value", item.item.url, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 30;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", item.licenseType, true, false)) + "\u002F\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[license]`, true, false)+pug_attr("value", item.item.gi.license ? item.item.gi.license : 'MISSING', true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", item.item.gi.contentId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 38;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 38;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 39;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", item.item.gi.titleId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 41;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.region !== 'UNKNOWN') {
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"region\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[region]`, true, false)+pug_attr("value", item.item.gi.region, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["regionDDL"](item.item.gi.region, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 49;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-6\"\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", item.item.gi.minFw, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.pkgPlatform !== 'UNKNOWN') {
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"platform\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[platform]`, true, false)+pug_attr("value", item.item.gi.pkgPlatform, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 59;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["platformDDL"](item.item.gi.pkgPlatform, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 61;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
if (item.item.gi.pkgType !== 'UNKNOWN') {
;pug_debug_line = 64;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"file-type\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[category]`, true, false)+pug_attr("value", item.item.gi.pkgType, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E";
}
else {
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_mixins["categoryDDL"](item.item.gi.pkgType, item.item.gi.contentId);
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm-4\"\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 70;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", item.item.gi.prettySize, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FnewContribFinalizeForm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Ful\u003E";}.call(this,"items" in locals_for_with?locals_for_with.items:typeof items!=="undefined"?items:undefined));} catch (err) {pug_rethrow(err, pug_debug_filename, pug_debug_line);};return pug_html;}