function pug_attr(t,e,n,f){return!1!==e&&null!=e&&(e||"class"!==t&&"style"!==t)?!0===e?" "+(f?t:t+'="'+t+'"'):("function"==typeof e.toJSON&&(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||-1===e.indexOf('"'))?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"):""}
function pug_attrs(t,r){var a="";for(var s in t)if(pug_has_own_property.call(t,s)){var u=t[s];if("class"===s){u=pug_classes(u),a=pug_attr(s,u,!1,r)+a;continue}"style"===s&&(u=pug_style(u)),a+=pug_attr(s,u,!1,r)}return a}
function pug_classes(s,r){return Array.isArray(s)?pug_classes_array(s,r):s&&"object"==typeof s?pug_classes_object(s):s||""}
function pug_classes_array(r,a){for(var s,e="",u="",c=Array.isArray(a),g=0;g<r.length;g++)(s=pug_classes(r[g]))&&(c&&a[g]&&(s=pug_escape(s)),e=e+u+s,u=" ");return e}
function pug_classes_object(r){var a="",n="";for(var o in r)o&&r[o]&&pug_has_own_property.call(r,o)&&(a=a+n+o,n=" ");return a}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_has_own_property=Object.prototype.hasOwnProperty;
var pug_match_html=/["&<>]/;
function pug_merge(e,r){if(1===arguments.length){for(var t=e[0],g=1;g<e.length;g++)t=pug_merge(t,e[g]);return t}for(var l in r)if("class"===l){var n=e[l]||[];e[l]=(Array.isArray(n)?n:[n]).concat(r[l]||[])}else if("style"===l){var n=pug_style(e[l]);n=n&&";"!==n[n.length-1]?n+";":n;var a=pug_style(r[l]);a=a&&";"!==a[a.length-1]?a+";":a,e[l]=n+a}else e[l]=r[l];return e}
function pug_rethrow(n,e,r,t){if(!(n instanceof Error))throw n;if(!("undefined"==typeof window&&e||t))throw n.message+=" on line "+r,n;try{t=t||require("fs").readFileSync(e,"utf8")}catch(e){pug_rethrow(n,null,r)}var i=3,a=t.split("\n"),o=Math.max(r-i,0),h=Math.min(a.length,r+i),i=a.slice(o,h).map(function(n,e){var t=e+o+1;return(t==r?"  > ":"    ")+t+"| "+n}).join("\n");throw n.path=e,n.message=(e||"Pug")+":"+r+"\n"+i+"\n\n"+n.message,n}
function pug_style(r){if(!r)return"";if("object"==typeof r){var t="";for(var e in r)pug_has_own_property.call(r,e)&&(t=t+e+":"+r[e]+";");return t}return r+""}module.exports = function(locals) {var pug_html = "", pug_mixins = {}, pug_interp;var pug_debug_filename, pug_debug_line;try {;var locals_for_with = (locals || {});(function (contributionMethod, items) {;pug_debug_line = 1;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["regionDDL"] = pug_interp = function(region, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 2;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 3;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"region\""+pug_attr("name", `${contentId}[region]`, true, false)) + "\u003E";
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "US"},{ 'selected': region === "US" }]), false)) + "\u003E";
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "US\u003C\u002Foption\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "EU"},{ 'selected': region === "EU" }]), false)) + "\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "EU\u003C\u002Foption\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "JP"},{ 'selected': region === "JP" }]), false)) + "\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "JP\u003C\u002Foption\u003E";
;pug_debug_line = 7;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "ASIA"},{ 'selected': region === "ASIA" }]), false)) + "\u003E";
;pug_debug_line = 7;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "ASIA\u003C\u002Foption\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "INT"},{ 'selected': region === "INT" }]), false)) + "\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "INT\u003C\u002Foption\u003E";
;pug_debug_line = 9;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': region === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 9;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"region\" name=\"region\"\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "US"},{ 'selected': region === "US" }]), false)) + "\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "US\u003C\u002Foption\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "EU"},{ 'selected': region === "EU" }]), false)) + "\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "EU\u003C\u002Foption\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "JP"},{ 'selected': region === "JP" }]), false)) + "\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "JP\u003C\u002Foption\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "ASIA"},{ 'selected': region === "ASIA" }]), false)) + "\u003E";
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "ASIA\u003C\u002Foption\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "INT"},{ 'selected': region === "INT" }]), false)) + "\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "INT\u003C\u002Foption\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': region === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 17;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["platformDDL"] = pug_interp = function(platform, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 20;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 21;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"platform\""+pug_attr("name", `${contentId}[platform]`, true, false)) + "\u003E";
;pug_debug_line = 22;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSX"},{ 'selected': platform === "PSX" }]), false)) + "\u003E";
;pug_debug_line = 22;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSX\u003C\u002Foption\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': platform === "PSP" }]), false)) + "\u003E";
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS3"},{ 'selected': platform === "PS3" }]), false)) + "\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS3\u003C\u002Foption\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003C!-- option(value=\"PS4\")&attributes({ 'selected': platform === \"PS4\" }) PS4--\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSM"},{ 'selected': platform === "PSM" }]), false)) + "\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSM\u003C\u002Foption\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSV"},{ 'selected': platform === "PSV" }]), false)) + "\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSV\u003C\u002Foption\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': platform === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 30;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"platform\" name=\"platform\"\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSX"},{ 'selected': platform === "PSX" }]), false)) + "\u003E";
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSX\u003C\u002Foption\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': platform === "PSP" }]), false)) + "\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS3"},{ 'selected': platform === "PS3" }]), false)) + "\u003E";
;pug_debug_line = 33;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS3\u003C\u002Foption\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003C!-- option(value=\"PS4\")&attributes({ 'selected': platform === \"PS4\" }) PS4--\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSM"},{ 'selected': platform === "PSM" }]), false)) + "\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSM\u003C\u002Foption\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSV"},{ 'selected': platform === "PSV" }]), false)) + "\u003E";
;pug_debug_line = 36;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSV\u003C\u002Foption\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "UNKNOWN"},{ 'selected': platform === "UNKNOWN" }]), false)) + "\u003E";
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 39;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["categoryDDL"] = pug_interp = function(category, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 40;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 41;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"category\""+pug_attr("name", `${contentId}[category]`, true, false)) + "\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Game"},{ 'selected': category === "Game" }]), false)) + "\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Game\u003C\u002Foption\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Theme"},{ 'selected': category === "Theme" }]), false)) + "\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Theme\u003C\u002Foption\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "DLC"},{ 'selected': category === "DLC" }]), false)) + "\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "DLC\u003C\u002Foption\u003E";
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Avatar"},{ 'selected': category === "Avatar" }]), false)) + "\u003E";
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Avatar\u003C\u002Foption\u003E";
;pug_debug_line = 46;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Update"},{ 'selected': category === "Update" }]), false)) + "\u003E";
;pug_debug_line = 46;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Update\u003C\u002Foption\u003E";
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Demo"},{ 'selected': category === "Demo" }]), false)) + "\u003E";
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Demo\u003C\u002Foption\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': (category === "Unknown" || category === 'UNKNOWN') }]), false)) + "\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"category\" name=\"category\"\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Game"},{ 'selected': category === "Game" }]), false)) + "\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Game\u003C\u002Foption\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Theme"},{ 'selected': category === "Theme" }]), false)) + "\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Theme\u003C\u002Foption\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "DLC"},{ 'selected': category === "DLC" }]), false)) + "\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "DLC\u003C\u002Foption\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Avatar"},{ 'selected': category === "Avatar" }]), false)) + "\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Avatar\u003C\u002Foption\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Update"},{ 'selected': category === "Update" }]), false)) + "\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Update\u003C\u002Foption\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Demo"},{ 'selected': category === "Demo" }]), false)) + "\u003E";
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Demo\u003C\u002Foption\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': (category === "Unknown" || category === 'UNKNOWN') }]), false)) + "\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "UNKNOWN\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 59;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_mixins["subTypeDDL"] = pug_interp = function(subType, contentId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
;pug_debug_line = 60;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
if (contentId) {
;pug_debug_line = 61;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect" + (" class=\"form-control\""+" id=\"subType\""+pug_attr("name", `${contentId}[subType]`, true, false)) + "\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': subType === 'PSP' }]), false)) + "\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PC Engine"},{ 'selected': subType === "PC Engine" }]), false)) + "\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PC Engine\u003C\u002Foption\u003E";
;pug_debug_line = 64;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Go"},{ 'selected': subType === "Go" }]), false)) + "\u003E";
;pug_debug_line = 64;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Go\u003C\u002Foption\u003E";
;pug_debug_line = 65;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP NeoGeo"},{ 'selected': subType === "PSP NeoGeo"}]), false)) + "\u003E";
;pug_debug_line = 65;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP NeoGeo\u003C\u002Foption\u003E";
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP Mini"},{ 'selected': subType === "PSP Mini"}]), false)) + "\u003E";
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP Mini\u003C\u002Foption\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP Remaster"},{ 'selected': subType === 'PSP Remaster' }]), false)) + "\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP Remaster\u003C\u002Foption\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS2 Classic"},{ 'selected': subType === 'PS2 Classic' }]), false)) + "\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS2 Classic\u003C\u002Foption\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': subType === 'Unknown' || typeof subType === 'undefined' }]), false)) + "\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Unknown\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
else {
;pug_debug_line = 71;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Cselect class=\"form-control\" id=\"subType\" name=\"subType\"\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP"},{ 'selected': subType === 'PSP' }]), false)) + "\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP\u003C\u002Foption\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PC Engine"},{ 'selected': subType === "PC Engine" }]), false)) + "\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PC Engine\u003C\u002Foption\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Go"},{ 'selected': subType === "Go" }]), false)) + "\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Go\u003C\u002Foption\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP NeoGeo"},{ 'selected': subType === "PSP NeoGeo" }]), false)) + "\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP NeoGeo\u003C\u002Foption\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP Mini"},{ 'selected': subType === "PSP Mini" }]), false)) + "\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP Mini\u003C\u002Foption\u003E";
;pug_debug_line = 77;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PSP Remaster"},{ 'selected': subType === 'PSP Remaster' }]), false)) + "\u003E";
;pug_debug_line = 77;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PSP Remaster\u003C\u002Foption\u003E";
;pug_debug_line = 78;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "PS2 Classic"},{ 'selected': subType === 'PS2 Classic' }]), false)) + "\u003E";
;pug_debug_line = 78;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "PS2 Classic\u003C\u002Foption\u003E";
;pug_debug_line = 79;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "\u003Coption" + (pug_attrs(pug_merge([{"value": "Unknown"},{ 'selected': subType === 'Unknown' || typeof subType === 'undefined' }]), false)) + "\u003E";
;pug_debug_line = 79;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fincludes\u002F_itemDropDownLists.pug";
pug_html = pug_html + "Unknown\u003C\u002Foption\u003E\u003C\u002Fselect\u003E";
}
};
;pug_debug_line = 3;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv id=\"form\"\u003E";
;pug_debug_line = 4;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003C!--h2.mb-3.text-center Batch Contributions--\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ch2 class=\"mb-3 text-center\"\u003E";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Confirm ";
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = contributionMethod.capitalize()) ? "" : pug_interp));
;pug_debug_line = 5;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " Contribution\u003C\u002Fh2\u003E";
;pug_debug_line = 6;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (items.orphans.length) {
;pug_debug_line = 7;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle\"\u003E";
;pug_debug_line = 8;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-title\"\u003E";
;pug_debug_line = 9;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ch5\u003E";
;pug_debug_line = 10;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ci class=\"fas fa-plus\"\u003E\u003C\u002Fi\u003E";
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cspan class=\"title-name\"\u003E";
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Orphan Submissions (";
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = items.orphans.length) ? "" : pug_interp));
;pug_debug_line = 11;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + ")\u003C\u002Fspan\u003E\u003C\u002Fh5\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 12;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-inner\"\u003E";
;pug_debug_line = 13;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 14;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
// iterate items.orphans
;(function(){
  var $$obj = items.orphans;
  if ('number' == typeof $$obj.length) {
      for (var pug_index0 = 0, $$l = $$obj.length; pug_index0 < $$l; pug_index0++) {
        var item = $$obj[pug_index0];
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 18;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Reason: ";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.message) ? "" : pug_interp)) + "\u003C\u002Fp\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index0 in $$obj) {
      $$l++;
      var item = $$obj[pug_index0];
;pug_debug_line = 15;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 16;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 18;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Reason: ";
;pug_debug_line = 19;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.message) ? "" : pug_interp)) + "\u003C\u002Fp\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 22;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (items.rejected.length) {
;pug_debug_line = 23;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle\"\u003E";
;pug_debug_line = 24;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-title\"\u003E";
;pug_debug_line = 25;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ch5\u003E";
;pug_debug_line = 26;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ci class=\"fas fa-plus\"\u003E\u003C\u002Fi\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cspan class=\"title-name\"\u003E";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Rejected Submissions (";
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = items.rejected.length) ? "" : pug_interp));
;pug_debug_line = 27;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + ")\u003C\u002Fspan\u003E\u003C\u002Fh5\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 28;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-inner\"\u003E";
;pug_debug_line = 29;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 30;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
// iterate items.rejected
;(function(){
  var $$obj = items.rejected;
  if ('number' == typeof $$obj.length) {
      for (var pug_index1 = 0, $$l = $$obj.length; pug_index1 < $$l; pug_index1++) {
        var item = $$obj[pug_index1];
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Reason: ";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.message) ? "" : pug_interp)) + "\u003C\u002Fp\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index1 in $$obj) {
      $$l++;
      var item = $$obj[pug_index1];
;pug_debug_line = 31;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 32;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 34;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cp\u003E";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Reason: ";
;pug_debug_line = 35;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.message) ? "" : pug_interp)) + "\u003C\u002Fp\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 37;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cform\u003E";
;pug_debug_line = 38;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (items.updated.length) {
;pug_debug_line = 39;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle\"\u003E";
;pug_debug_line = 40;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-title\"\u003E";
;pug_debug_line = 41;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ch5\u003E";
;pug_debug_line = 42;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ci class=\"fas fa-plus\"\u003E\u003C\u002Fi\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cspan class=\"title-name\"\u003E";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Updated Items (";
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = items.updated.length) ? "" : pug_interp));
;pug_debug_line = 43;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + ")\u003C\u002Fspan\u003E\u003C\u002Fh5\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 44;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-inner\"\u003E";
;pug_debug_line = 45;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 46;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
// iterate items.updated
;(function(){
  var $$obj = items.updated;
  if ('number' == typeof $$obj.length) {
      for (var pug_index2 = 0, $$l = $$obj.length; pug_index2 < $$l; pug_index2++) {
        var item = $$obj[pug_index2];
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.contentId || item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[title]`, true, false)+pug_attr("value", (item.item.name || item.item.gi.title || 'UNKNOWN TITLE'), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.regionalTitle || item.item.gi.regionalTitle) {
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 58;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", (item.item.regionalTitle || item.item.gi.regionalTitle), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 60;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 61;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"pkg\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"pkg\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[pkg]`, true, false)+pug_attr("value", (item.item.gi.pkg || item.item.pkg || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 65;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.license || item.item.gi.license) {
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.licenseType || item.item.gi.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", (item.item.licenseType || item.item.gi.licenseType), true, false)) + "\u002F\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[license]`, true, false)+pug_attr("value", (item.item.license || item.item.gi.license || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 71;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", (item.item.contentId || item.item.gi.contentId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 77;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", (item.item.titleId || item.item.gi.titleId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 79;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 80;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 81;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 81;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 82;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["regionDDL"]((item.item.region || item.item.gi.region), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 84;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 85;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 85;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 86;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["categoryDDL"]((item.item.category || item.item.gi.category), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 88;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 89;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 89;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 90;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["platformDDL"]((item.item.platform || item.item.gi.platform), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 92;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.subType || item.item.gi.subType) {
;pug_debug_line = 93;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 94;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"subType\"\u003E";
;pug_debug_line = 94;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Sub Type\u003C\u002Flabel\u003E";
;pug_debug_line = 95;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["subTypeDDL"]((item.item.subType || item.item.gi.subType), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 97;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 98;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.minFw !== null) {
;pug_debug_line = 99;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 100;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 100;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 101;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", (item.item.minFw || item.item.gi.minFw || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 103;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 104;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"fileSize\"\u003E";
;pug_debug_line = 104;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Size\u003C\u002Flabel\u003E";
;pug_debug_line = 105;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"fileSize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[fileSize]`, true, false)+pug_attr("value", (item.item.fileSize || item.item.gi.fileSize || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 107;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 108;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 108;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 109;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", (item.item.prettySize || item.item.gi.prettySize), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 111;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.appVer !== null) {
;pug_debug_line = 112;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 113;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"appVer\"\u003E";
;pug_debug_line = 113;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "App Version\u003C\u002Flabel\u003E";
;pug_debug_line = 114;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"appVer\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[appVer]`, true, false)+pug_attr("value", (item.item.appVer || item.item.gi.appVer || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 117;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.version !== null || item.item.gi.version !== null) {
;pug_debug_line = 118;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 119;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"version\"\u003E";
;pug_debug_line = 119;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Version\u003C\u002Flabel\u003E";
;pug_debug_line = 120;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"version\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[version]`, true, false)+pug_attr("value", item.item.gi.version, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 122;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url) {
;pug_debug_line = 123;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 124;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 125;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 125;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Store URL:\u003C\u002Flabel\u003E";
;pug_debug_line = 126;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[url]`, true, false)+pug_attr("value", (item.item.url || item.item.gi.url), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 128;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 129;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 130;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 130;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 131;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index2 in $$obj) {
      $$l++;
      var item = $$obj[pug_index2];
;pug_debug_line = 47;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 48;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 50;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.contentId || item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 51;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 52;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 53;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 54;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[title]`, true, false)+pug_attr("value", (item.item.name || item.item.gi.title || 'UNKNOWN TITLE'), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 55;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.regionalTitle || item.item.gi.regionalTitle) {
;pug_debug_line = 56;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 57;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 58;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", (item.item.regionalTitle || item.item.gi.regionalTitle), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 60;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 61;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"pkg\"\u003E";
;pug_debug_line = 62;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 63;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"pkg\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[pkg]`, true, false)+pug_attr("value", (item.item.gi.pkg || item.item.pkg || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 65;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.license || item.item.gi.license) {
;pug_debug_line = 66;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 67;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.licenseType || item.item.gi.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 68;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", (item.item.licenseType || item.item.gi.licenseType), true, false)) + "\u002F\u003E";
;pug_debug_line = 69;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[license]`, true, false)+pug_attr("value", (item.item.license || item.item.gi.license || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 71;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 72;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 73;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 74;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", (item.item.contentId || item.item.gi.contentId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 75;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 76;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 77;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", (item.item.titleId || item.item.gi.titleId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 79;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 80;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 81;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 81;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 82;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["regionDDL"]((item.item.region || item.item.gi.region), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 84;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 85;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 85;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 86;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["categoryDDL"]((item.item.category || item.item.gi.category), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 88;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 89;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 89;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 90;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["platformDDL"]((item.item.platform || item.item.gi.platform), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 92;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.subType || item.item.gi.subType) {
;pug_debug_line = 93;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 94;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"subType\"\u003E";
;pug_debug_line = 94;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Sub Type\u003C\u002Flabel\u003E";
;pug_debug_line = 95;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["subTypeDDL"]((item.item.subType || item.item.gi.subType), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 97;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 98;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.minFw !== null) {
;pug_debug_line = 99;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 100;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 100;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 101;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", (item.item.minFw || item.item.gi.minFw || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 103;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 104;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"fileSize\"\u003E";
;pug_debug_line = 104;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Size\u003C\u002Flabel\u003E";
;pug_debug_line = 105;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"fileSize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[fileSize]`, true, false)+pug_attr("value", (item.item.fileSize || item.item.gi.fileSize || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 107;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 108;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 108;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 109;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", (item.item.prettySize || item.item.gi.prettySize), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 111;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.appVer !== null) {
;pug_debug_line = 112;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 113;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"appVer\"\u003E";
;pug_debug_line = 113;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "App Version\u003C\u002Flabel\u003E";
;pug_debug_line = 114;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"appVer\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[appVer]`, true, false)+pug_attr("value", (item.item.appVer || item.item.gi.appVer || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 117;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.version !== null || item.item.gi.version !== null) {
;pug_debug_line = 118;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 119;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"version\"\u003E";
;pug_debug_line = 119;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Version\u003C\u002Flabel\u003E";
;pug_debug_line = 120;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"version\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[version]`, true, false)+pug_attr("value", item.item.gi.version, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 122;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url) {
;pug_debug_line = 123;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 124;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 125;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 125;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Store URL:\u003C\u002Flabel\u003E";
;pug_debug_line = 126;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[url]`, true, false)+pug_attr("value", (item.item.url || item.item.gi.url), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 128;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 129;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 130;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 130;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 131;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 133;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (items.new.length) {
;pug_debug_line = 134;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle newItems\"\u003E";
;pug_debug_line = 135;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-title\"\u003E";
;pug_debug_line = 136;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ch5\u003E";
;pug_debug_line = 137;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ci class=\"fas fa-plus\"\u003E\u003C\u002Fi\u003E";
;pug_debug_line = 138;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cspan class=\"title-name\"\u003E";
;pug_debug_line = 138;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "New Additions (";
;pug_debug_line = 138;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = items.new.length) ? "" : pug_interp));
;pug_debug_line = 138;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + ")\u003C\u002Fspan\u003E\u003C\u002Fh5\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 139;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"toggle-inner\"\u003E";
;pug_debug_line = 140;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cul\u003E";
;pug_debug_line = 141;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
// iterate items.new
;(function(){
  var $$obj = items.new;
  if ('number' == typeof $$obj.length) {
      for (var pug_index3 = 0, $$l = $$obj.length; pug_index3 < $$l; pug_index3++) {
        var item = $$obj[pug_index3];
;pug_debug_line = 142;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 145;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.contentId || item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 146;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 147;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 148;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 148;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 149;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[title]`, true, false)+pug_attr("value", (item.item.name || item.item.gi.title || 'UNKNOWN TITLE'), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 150;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.regionalTitle || item.item.gi.regionalTitle) {
;pug_debug_line = 151;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 152;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 152;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 153;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", (item.item.regionalTitle || item.item.gi.regionalTitle), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 155;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 156;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 157;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"pkg\"\u003E";
;pug_debug_line = 157;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 158;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"pkg\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[pkg]`, true, false)+pug_attr("value", (item.item.gi.pkg || item.item.pkg || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 160;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.license || item.item.gi.license) {
;pug_debug_line = 161;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 162;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 162;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.licenseType || item.item.gi.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 163;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", (item.item.licenseType || item.item.gi.licenseType), true, false)) + "\u002F\u003E";
;pug_debug_line = 164;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[license]`, true, false)+pug_attr("value", (item.item.license || item.item.gi.license || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 166;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 167;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 168;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 168;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 169;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", (item.item.contentId || item.item.gi.contentId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 170;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 171;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 171;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 172;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", (item.item.titleId || item.item.gi.titleId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 174;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 175;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 175;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 176;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["regionDDL"]((item.item.region || item.item.gi.region), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 178;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 179;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 180;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 180;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 181;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["categoryDDL"]((item.item.category || item.item.gi.category), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 183;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 184;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 184;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 185;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["platformDDL"]((item.item.platform || item.item.gi.platform), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 187;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.subType || item.item.gi.subType) {
;pug_debug_line = 188;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 189;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"subType\"\u003E";
;pug_debug_line = 189;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Sub Type\u003C\u002Flabel\u003E";
;pug_debug_line = 190;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["subTypeDDL"]((item.item.subType || item.item.gi.subType), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 192;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 193;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.minFw !== null) {
;pug_debug_line = 194;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 195;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 195;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 196;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", (item.item.minFw || item.item.gi.minFw || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 198;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 199;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"fileSize\"\u003E";
;pug_debug_line = 199;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Size\u003C\u002Flabel\u003E";
;pug_debug_line = 200;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"fileSize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[fileSize]`, true, false)+pug_attr("value", (item.item.fileSize || item.item.gi.fileSize || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 202;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 203;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 203;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 204;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", (item.item.prettySize || item.item.gi.prettySize), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 206;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.appVer !== null) {
;pug_debug_line = 207;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 208;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"appVer\"\u003E";
;pug_debug_line = 208;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "App Version\u003C\u002Flabel\u003E";
;pug_debug_line = 209;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"appVer\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[appVer]`, true, false)+pug_attr("value", (item.item.appVer || item.item.gi.appVer || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 212;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.version !== null) {
;pug_debug_line = 213;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 214;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"version\"\u003E";
;pug_debug_line = 214;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Version\u003C\u002Flabel\u003E";
;pug_debug_line = 215;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"version\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[version]`, true, false)+pug_attr("value", item.item.gi.version, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 217;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url || item.item.productId || item.item.gi.productId) {
;pug_debug_line = 218;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 219;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url) {
;pug_debug_line = 220;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 221;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 221;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Store URL:\u003C\u002Flabel\u003E";
;pug_debug_line = 222;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[url]`, true, false)+pug_attr("value", (item.item.url || item.item.gi.url), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 224;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.productId || item.item.gi.productId) {
;pug_debug_line = 225;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 226;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"productId\"\u003E";
;pug_debug_line = 226;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Product ID:\u003C\u002Flabel\u003E";
;pug_debug_line = 227;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"productId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[productId]`, true, false)+pug_attr("value", item.item.productId || item.item.gi.productId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 229;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 230;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 231;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 231;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 232;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
      }
  } else {
    var $$l = 0;
    for (var pug_index3 in $$obj) {
      $$l++;
      var item = $$obj[pug_index3];
;pug_debug_line = 142;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cli\u003E";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"showForm\" href=\"#\"\u003E";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.platform || item.item.gi.platform || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.category || item.item.gi.category || 'UNKNOWN') ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.titleId) ? "" : pug_interp));
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + " - ";
;pug_debug_line = 143;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.name || item.item.gi.title || 'UNKNOWN TITLE') ? "" : pug_interp)) + "\u003C\u002Fa\u003E";
;pug_debug_line = 145;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv" + (pug_attr("id", item.item.contentId || item.item.gi.contentId, true, false)) + "\u003E";
;pug_debug_line = 146;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 147;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 148;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"title\"\u003E";
;pug_debug_line = 148;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title\u003C\u002Flabel\u003E";
;pug_debug_line = 149;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[title]`, true, false)+pug_attr("value", (item.item.name || item.item.gi.title || 'UNKNOWN TITLE'), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 150;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.regionalTitle || item.item.gi.regionalTitle) {
;pug_debug_line = 151;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 152;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"regional-title\"\u003E";
;pug_debug_line = 152;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Regional Title\u003C\u002Flabel\u003E";
;pug_debug_line = 153;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"regional-title\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[regionalTitle]`, true, false)+pug_attr("value", (item.item.regionalTitle || item.item.gi.regionalTitle), true, false)+pug_attr("required", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 155;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 156;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 157;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"pkg\"\u003E";
;pug_debug_line = 157;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "PKG Url\u003C\u002Flabel\u003E";
;pug_debug_line = 158;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"pkg\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[pkg]`, true, false)+pug_attr("value", (item.item.gi.pkg || item.item.pkg || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 160;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.license || item.item.gi.license) {
;pug_debug_line = 161;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 162;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"license-text\"\u003E";
;pug_debug_line = 162;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = item.item.licenseType || item.item.gi.licenseType) ? "" : pug_interp)) + "\u003C\u002Flabel\u003E";
;pug_debug_line = 163;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"d-none\""+" id=\"licenseType\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[licenseType]`, true, false)+pug_attr("value", (item.item.licenseType || item.item.gi.licenseType), true, false)) + "\u002F\u003E";
;pug_debug_line = 164;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"license-text\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[license]`, true, false)+pug_attr("value", (item.item.license || item.item.gi.license || 'MISSING'), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 166;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 167;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 168;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"contentId\"\u003E";
;pug_debug_line = 168;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Content ID\u003C\u002Flabel\u003E";
;pug_debug_line = 169;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"contentId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[contentId]`, true, false)+pug_attr("value", (item.item.contentId || item.item.gi.contentId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 170;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 171;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"titleId\"\u003E";
;pug_debug_line = 171;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Title ID\u003C\u002Flabel\u003E";
;pug_debug_line = 172;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"titleId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[titleId]`, true, false)+pug_attr("value", (item.item.titleId || item.item.gi.titleId), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 174;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 175;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"region\"\u003E";
;pug_debug_line = 175;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Region\u003C\u002Flabel\u003E";
;pug_debug_line = 176;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["regionDDL"]((item.item.region || item.item.gi.region), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 178;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 179;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 180;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"file-type\"\u003E";
;pug_debug_line = 180;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Category\u003C\u002Flabel\u003E";
;pug_debug_line = 181;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["categoryDDL"]((item.item.category || item.item.gi.category), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 183;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 184;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"console-type\"\u003E";
;pug_debug_line = 184;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Platform\u003C\u002Flabel\u003E";
;pug_debug_line = 185;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["platformDDL"]((item.item.platform || item.item.gi.platform), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 187;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.subType || item.item.gi.subType) {
;pug_debug_line = 188;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 189;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"subType\"\u003E";
;pug_debug_line = 189;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Sub Type\u003C\u002Flabel\u003E";
;pug_debug_line = 190;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_mixins["subTypeDDL"]((item.item.subType || item.item.gi.subType), (item.item.contentId || item.item.gi.contentId));
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 192;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 193;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.minFw !== null) {
;pug_debug_line = 194;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 195;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"min-fw\"\u003E";
;pug_debug_line = 195;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Min FW\u003C\u002Flabel\u003E";
;pug_debug_line = 196;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"min-fw\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[minFw]`, true, false)+pug_attr("value", (item.item.minFw || item.item.gi.minFw || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 198;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 199;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"fileSize\"\u003E";
;pug_debug_line = 199;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Size\u003C\u002Flabel\u003E";
;pug_debug_line = 200;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"fileSize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[fileSize]`, true, false)+pug_attr("value", (item.item.fileSize || item.item.gi.fileSize || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 202;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 203;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"prettySize\"\u003E";
;pug_debug_line = 203;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Pretty Size\u003C\u002Flabel\u003E";
;pug_debug_line = 204;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"prettySize\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[prettySize]`, true, false)+pug_attr("value", (item.item.prettySize || item.item.gi.prettySize), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
;pug_debug_line = 206;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.appVer !== null) {
;pug_debug_line = 207;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 208;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"appVer\"\u003E";
;pug_debug_line = 208;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "App Version\u003C\u002Flabel\u003E";
;pug_debug_line = 209;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"appVer\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[appVer]`, true, false)+pug_attr("value", (item.item.appVer || item.item.gi.appVer || 0), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 212;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.gi.version !== null) {
;pug_debug_line = 213;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 214;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"version\"\u003E";
;pug_debug_line = 214;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Version\u003C\u002Flabel\u003E";
;pug_debug_line = 215;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"version\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[version]`, true, false)+pug_attr("value", item.item.gi.version, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 217;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url || item.item.productId || item.item.gi.productId) {
;pug_debug_line = 218;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 219;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.url || item.item.gi.url) {
;pug_debug_line = 220;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 221;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"url\"\u003E";
;pug_debug_line = 221;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Store URL:\u003C\u002Flabel\u003E";
;pug_debug_line = 222;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"url\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[url]`, true, false)+pug_attr("value", (item.item.url || item.item.gi.url), true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 224;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (item.item.productId || item.item.gi.productId) {
;pug_debug_line = 225;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 226;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"productId\"\u003E";
;pug_debug_line = 226;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Product ID:\u003C\u002Flabel\u003E";
;pug_debug_line = 227;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"productId\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[productId]`, true, false)+pug_attr("value", item.item.productId || item.item.gi.productId, true, false)+pug_attr("readonly", true, true, false)) + "\u002F\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";
}
;pug_debug_line = 229;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-row\"\u003E";
;pug_debug_line = 230;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group col-sm\"\u003E";
;pug_debug_line = 231;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Clabel for=\"item-notes\"\u003E";
;pug_debug_line = 231;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Notes:\u003C\u002Flabel\u003E";
;pug_debug_line = 232;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput" + (" class=\"form-control\""+" id=\"item-notes\" type=\"text\""+pug_attr("name", `${item.item.contentId || item.item.gi.contentId}[notes]`, true, false)+" value=\"\"") + "\u002F\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E\u003C\u002Fli\u003E";
    }
  }
}).call(this);

pug_html = pug_html + "\u003C\u002Ful\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fform\u003E";
;pug_debug_line = 234;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cbr\u002F\u003E";
;pug_debug_line = 236;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
if (items.updated.length || items.new.length) {
;pug_debug_line = 237;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cform id=\"finalizeForm\" action=\"\u002Fcontribute\u002Fbatch\u002Ffinalize\" method=\"POST\"\u003E";
;pug_debug_line = 238;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group-col-sm\"\u003E";
;pug_debug_line = 239;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cinput class=\"d-none\" id=\"serializedInput\" type=\"text\" name=\"serializedInput\"\u002F\u003E";
;pug_debug_line = 240;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cbutton class=\"btn btn-block btn-primary\" id=\"btnFinalize\" type=\"submit\"\u003E";
;pug_debug_line = 240;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Contribute\u003C\u002Fbutton\u003E\u003C\u002Fdiv\u003E\u003C\u002Fform\u003E";
}
else {
;pug_debug_line = 242;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Cdiv class=\"form-group-col-sm\"\u003E";
;pug_debug_line = 243;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "\u003Ca class=\"button btn btn-block btn-primary\" href=\"\u002Fcontribute\"\u003E";
;pug_debug_line = 243;pug_debug_filename = "\u002Ftmp\u002Fnix-build-nps-site.drv-0\u002Fsite\u002Fsrc\u002Fviews\u002Fcontribute\u002FbatchConfirm.pug";
pug_html = pug_html + "Go Back\u003C\u002Fa\u003E\u003C\u002Fdiv\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E";}.call(this,"contributionMethod" in locals_for_with?locals_for_with.contributionMethod:typeof contributionMethod!=="undefined"?contributionMethod:undefined,"items" in locals_for_with?locals_for_with.items:typeof items!=="undefined"?items:undefined));} catch (err) {pug_rethrow(err, pug_debug_filename, pug_debug_line);};return pug_html;}