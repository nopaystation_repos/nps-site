<link href="/bootstrap.min.css" rel="stylesheet">
<link href="/narrow-jumbotron.css" rel="stylesheet">

<div style="margin-left: 20px;">
<?php
$jsonF = file_get_contents("version.json");
$json = json_decode($jsonF);
echo "<h2>Download newest version: ".GetLink($json[0]->url, "<b>NPS_Browser_".$json[0]->version)."</b></h2>";
echo "</br>";
echo "<h3>Latest changelog:</br></h3>";
foreach($json[0]->changelog as $change)
echo "<h6>- ".$change."</h6>";
echo "</br>";
echo "<h3>History:</h3>";
for($i=1; $i<sizeof($json); $i++)
{
	echo GetLink($json[$i]->url, "<h4>NPS_Browser_".$json[$i]->version)."</h4>";
	if($json[$i]->changelog!=null)
	{
		foreach($json[$i]->changelog as $change)
		echo "<h6>- ".$change."</h6>";
		echo "</br>";
	}
}
echo "</br></br>";
echo "<h6>Source code: ";
echo GetLink("source.zip","Source.zip");
function GetLink($url, $display)
{
	return "<a href='".$url."'>".$display."</a></h6>";
}
?>
</div>