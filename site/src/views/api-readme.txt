To use this API:
URLs can be constructed by using the following schema: /api?platform=PLATFORM&category=CATEGORY&region=REGION
The acceptable parameters for CONSOLE are: "PSV", "PS3", "PSX", "PSP", "PSM"
The acceptable parameters for CATEGORY are: "GAME", "DLC", "THEME", "AVATAR"
The acceptable parameters for REGION are: "US", "EU", "ASIA", "JP", "INT"

A search query can be used by adding the parameter 'query'

Examples: /api?platform=psv&category=game&region=eu&query=tetris

Note: URL parameters are case-insensitive. Capitalization and order of parameters is irrelevant.


To retrieve the timestamp of the last database update, developers can use the url /api/lastUpdated
