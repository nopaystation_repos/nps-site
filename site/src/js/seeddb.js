// require('./utils.spinner')
const JSZip = require('jszip')

$(document).ready(() => {
  // let jsonFileField = $('#detailsFile')
  // let jsonData = $('#jsonData')
  // let btnSubmitDetailsForm = $('#btnSubmitDetailsForm')
  // let tsvForm = $('#tsvForm')
  // let tsvFileField = $('#tsvFile')[0]
  let btnSubmitTsvForm = $('#btnSubmitTsvForm')

  // let reader = {}
  // let xhr
  // let meter
  // let file = {}
  // let uploaders = []
  // let chooseFile
  // let slice_size = 1000 * 1024

  btnSubmitTsvForm.click(e => {
    // e.preventDefault()
    // $.spin('true')

    // let url = tsvForm.attr('action')
    //
    // let files = tsvFileField.files
    // let formData = new FormData()
    //
    // for (var i = 0; i < files.length; i++) {
    //   let file = files[i]
    //
    //   // check the file type
    //   if (!file.type.match('text/tab-separated-values')) continue
    //
    //   // add the file to the request
    //   formData.append('files[]', file, file.name)
    // }

    // reader = new FileReader()
    // file = tsvFileField.files[0]
    // let entries = getZipEntries(file)
    // entries.map((entry) => {
    //
    // })
    // upload(0)

    // upload(formData)
  })
  //
  // function upload(formData) {
  //   xhr = new XMLHttpRequest()
  //   xhr.open('POST', tsvForm.attr('action'), true)
  //   xhr.upload.onprogress = e => {
  //     if (e.lengthComputable) {
  //       let meter = {
  //         value: Math.round((e.loaded / e.total) * 100),
  //         textContent: parseFloat(this.value) + '%'
  //       }
  //       console.log(meter)
  //     }
  //
  //     // if (meter.textContent === '100%')
  //   }
  //   xhr.onload = e => {
  //     if (xhr.status === 200) {
  //       console.log('files uploaded')
  //
  //       if (xhr.responseText === 'done') location.reload()
  //     } else {
  //       console.error(xhr.statusText)
  //     }
  //   }
  //   // xhr.onloadend = e => {
  //   //   uploaders.pop()
  //   //   if (!uploaders.length) console.log('All Done!')
  //   // }
  //   // uploaders.push(xhr)
  //   xhr.send(formData)
  // }

  //
  // chooseFile = tsvFileField.on('change', e => {
  //   let self = e.currentTarget
  //   let blob = self.files[0]
  // })

  // function upload(startAt) {
  //   let next_slice = startAt + slice_size + 1
  //   let blob = file.slice(startAt, next_slice)
  //
  //   reader.onloadend = (event) => {
  //     if (event.target.readyState !== FileReader.DONE) return
  //   }
  // }
})

function getZipEntries(zipFile) {
  return JSZip.loadAsync(zipFile).then(zip => {
    let entries = Object.keys(zip.files)
      .map(name => {
        if (!zip.files[name].dir) {
          let file = zip.files[name]
          let fileName = file.name.split('/').pop()
          let ext = fileName.substr(fileName.lastIndexOf('.')).toLowerCase()

          file.filename = fileName
          file.ext = ext
          if (ext === '.tsv') return zip.files[name]
        }
      })
      .filter(el => {
        if (el !== undefined) {
          if (el.name.slice(0, 8) !== '__MACOSX') {
            return el
          }
        }
      })

    return entries
  })
}

// btnSubmitTsvForm.click(e => {
//   e.preventDefault()
//
//
//   let zipFile = tsvFileField.files[0]
//
//   let z = JSZip.loadAsync(zipFile)
//     .then(zip => {
//       let entries = Object.keys(zip.files).map((name) => {
//         if (!zip.files[name].dir) {
//           let file = zip.files[name]
//           let fileName = file.name.split('/').pop()
//           let ext = fileName.substr(fileName.lastIndexOf('.')).toLowerCase()
//
//           file.filename = fileName
//           file.ext = ext
//           if (ext === '.tsv') return zip.files[name]
//         }
//       })
//         .filter((el) => {
//           if (el !== undefined) {
//             if (el.name.slice(0, 8) !== '__MACOSX') {
//               return el
//             }
//           }
//         })
//
//       return entries
//     })
//     .then((entries) => {
//       console.log(entries)
//     })
//
// })

// jsonFileField.change(e => {
//   let file = jsonFileField[0].files[0]
//
//   let reader = new FileReader()
//
//   return new Promise((resolve, reject) => {
//     reader.onerror = () => {
//       reader.abort()
//       reject(new DOMException('Problem parsing input file.'))
//     }
//
//     reader.onload = () => {
//       let items = reader.result
//
//       console.log(items)
//
//       jsonData.val(items)
//     }
//
//     reader.readAsText(file)
//   })
// })
