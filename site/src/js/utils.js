String.prototype.toCamelCase = function() {
  return this.toLowerCase()
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) =>
      idx === 0 ? ltr.toLowerCase() : ltr.toUpperCase()
    )
    .replace(/\s+/g, '')
}

String.prototype.capitalize = function() {
  return this.toLowerCase().charAt(0).toUpperCase() + this.slice(1)
}

// exports.stringUtils = {
//   toCamelCase: (String.prototype.toCamelCase = function() {
//     return this.toLowerCase()
//       .replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) =>
//         idx === 0 ? ltr.toLowerCase() : ltr.toUpperCase()
//       )
//       .replace(/\s+/g, '')
//   }),
//
//   capitalize: (String.prototype.capitalize = function() {
//     return this.charAt(0).toUpperCase() + this.slice(1)
//   })
// }
