$(document).ready(function() {
  $('.checkAll').on('click', function() {
    $(this)
      .closest('table')
      .find('tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr')
      .toggleClass('selected', this.checked)
  })

  $('tbody :checkbox').on('click', function() {
    $(this)
      .closest('tr')
      .toggleClass('selected', this.checked) // Classe de seleção na row

    $(this)
      .closest('table')
      .find('.checkAll')
      .prop(
        'checked',
        $(this)
          .closest('table')
          .find('tbody :checkbox:checked').length ===
          $(this)
            .closest('table')
            .find('tbody :checkbox').length
      )
  })

  $('input:checkbox').change(() => {
    let len = $('table').find('tbody :checkbox:checked').length

    if (len > 0) {
      $('#numSelectedItems').text(len)
      $('.divActions > button').prop('disabled', false)
    } else {
      $('#numSelectedItems').text('0')
      $('.divActions > button').prop('disabled', true)
    }

    let a = $('input:checkbox')
      .map(function() {
        if ($(this).is(':checked')) {
          if (this.value !== 'on') {
            return this.value
          }
        }
      })
      .get()
    $('#selectedItems').val(JSON.stringify(a))
  })

  $('#contribForm button').click(e => {
    e.preventDefault()

    if (e.target.value.toLowerCase() === 'approve') {
      $('#contribForm').attr('action', '/manage/contributions/approve')
      $('#contribForm').submit()
    } else if (e.target.value.toLowerCase() === 'reject') {
      $('#contribForm').attr('action', '/manage/contributions/reject')
      $('#contribForm').submit()
    }
  })
})
