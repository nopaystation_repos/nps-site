$(document).ready(() => {
  let searchForm = $('#dbSearchForm')

  $('.btnFilterProperty > input, #limit, #orderBy, #sort, #missing').on(
    'change',
    e => {
      e.preventDefault()
      let url = '/search/pending?' + searchForm.serialize()

      searchForm.attr('action', url)

      $('#btnSearch').click()
    }
  )

  $('.btn-group-toggle').on('click', 'label.btn', function(e) {
    if ($(this).hasClass('active')) {
      setTimeout(
        function() {
          $(this)
            .removeClass('active')
            .find('input')
            .prop('checked', false)

          $('#btnSearch').click()

        }.bind(this),
        10
      )
    }
  })

  searchForm.submit(e => {
    let url = '/search/pending?' + searchForm.serialize()

    return searchForm.attr('action', url)
  })

  $('a[data-toggle="modal"]').on('click', e => {
    e.preventDefault()
  })


//   var hash = window.location.hash;
//   if (hash !== ''){
//     $(hash).modal('show');
//   }
//
//   $('div.modal').on('show.bs.modal', function() {
//     var modal = this;
//     var hash = modal.id;
//     window.location.hash = hash;
//     window.onhashchange = function() {
//       if (!location.hash){
//         $(modal).modal('hide');
//       } else {
//         $(modal).modal('show');
//       }
//     }
//   });
//
//   $('div.modal').on('hidden.bs.modal', function() {
//     var hash = this.id;
//     history.replaceState('', document.title, window.location.pathname);
//   });
//
// // when close button clicked simulate back
//   $('div.modal button.close').on('click', function(){
//     window.history.back();
//   })
//
// // when esc pressed when modal open simulate back
//   $('div.modal').keyup(function(e) {
//     if (e.keyCode == 27){
//       window.history.back();
//     }
//   });
})
