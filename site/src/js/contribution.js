import { rifRapTools } from './rifraptools'
const _contributionConfirmBatch = require('../../public/js/batchConfirm.js')
const buffer = require('./buffer-bundle.js')
const JSZip = require('jszip')

let inputForm
let contributionMethod

$(document).ready(() => {
  let btnValidate = $('#btnValidate')
  inputForm = $('#inputForm')
  contributionMethod = getContributionMethod(inputForm)
  // let inputForm = $('#inputForm')

  $('#licenseFileField').change(async () => {
    let licenseField = $('#license')
    let licenseTypeField = $('#licenseType')
    let platformField = $('#platform')
    let titleIdField = $('#titleId')
    let contentIdField = $('#contentId')
    let licenseFile = $('#licenseFileField')[0].files[0]
    let license = await getLicenseFromFile(licenseFile)

    licenseField.val(license.license)
    platformField.val(license.platform)
    contentIdField.val(license.contentId)
    titleIdField.val(license.titleId)

    // if (license.platform === 'PSM' || license.platform === 'PSV') {
    //   licenseTypeField.val('zRIF')
    // } else if (license.platform === 'PS3' || license.platform === 'PS4') {
    //   licenseTypeField.val('RAP')
    // }
    if (
      license.platform === 'PS3' ||
      license.platform === 'PSX' ||
      license.platform === 'PS4'
    ) {
      licenseTypeField.val('RAP')
    } else {
      licenseTypeField.val('zRIF')
    }
  })

  $('#psdle').change(async () => {
    let reader = new FileReader()
    let file = $('#psdle')[0].files[0]

    return new Promise((resolve, reject) => {
      reader.onerror = () => {
        reader.abort()
        reject(new DOMException('Problem parsing input file.'))
      }

      reader.onload = () => {
        let parsedJson = JSON.parse(reader.result)
        let columns = parsedJson.columns
        let columnList = []
        columns.map(column => {
          if (typeof column === 'object' && column.hasOwnProperty('property')) {
            columnList.push(column.property)
          }
        })

        if (
          !columnList.includes('platform') ||
          !columnList.includes('name') ||
          !columnList.includes('pkg') ||
          !columnList.includes('id') ||
          !columnList.includes('size') ||
          !columnList.includes('baseGame')
        ) {
          alert('Invalid JSON format.')
          reader.abort()
          $('#psdle').val('')
        }
      }
      reader.readAsText(file)
    })
  })

  $.LoadingOverlaySetup({
    textClass: 'spinnerText',
    textOrder: 2,
    progress: true,
    progressFixedPosition: 'bottom 4rem'
  })

  btnValidate.click(e => {
    e.preventDefault()

    if (contributionMethod === 'single') validateSingle()
    else if (contributionMethod === 'batch') vb()
  })
})

function validateSingle() {
  let urlField = $('#pkg')
  let licenseField = $('#license')
  let licenseTypeField = $('#licenseType')
  let platformField = $('#platform')
  let titleIdField = $('#titleId')
  let inputForm = $('#inputForm')

  // remove any spaces
  let url = urlField.val().replace(/\s/g, '')
  let license = licenseField.val()
  let platform = platformField.val()
  let titleId = titleIdField.val()

  $('.jumbotron').LoadingOverlay('show', { text: 'Analyzing...' })

  // url only
  if (!license && url) {
    platform = getPlatformFromTitleID(titleId)
    platformField.val(platform)
    titleId = getTitleIdFromUrl(url)
    titleIdField.val(titleId)

    // if (platform === 'PSM' || platform === 'PSV' || platform === 'PSM') {
    //   licenseTypeField.val('zRIF')
    // } else if (platform === 'PS3' || platform === 'PS4') {
    //   licenseTypeField.val('RAP')
    // }

    // inputForm.submit()

    makeRequest(
      [
        inputForm
          .find('input')
          .filter(function(index, el) {
            return $(el).val() !== ''
          })
          .serializeJSON()
      ],
      'validate'
    )
  }
  else if (!url && license) {
    // no url, yes license
    if (license) {
      makeRequest(
        [
          inputForm
            .find('input')
            .filter(function(index, el) {
              return $(el).val() !== ''
            })
            .serializeJSON()
        ],
        'validate'
      )
      // inputForm.submit()
      // makeRequest(inputForm, inputForm.serialize(), data => {
      //   $('.jumbotron').LoadingOverlay('hide')
      //     .empty()
      //     .append(_contributionFinalizeSingle(data))
      // $.spin('stop')
      // $('.jumbotron')
      //   .empty()
      //   .append(_contributionFinalizeSingle(data))
      // })
    } else {
      alert('Invalid license supplied.')
      // $('.jumbotron').LoadingOverlay('hide')
    }
  }
  else if (license && url) {
    titleId = getTitleIdFromUrl(url)
    titleIdField.val(titleId)
    platform = getPlatformFromTitleID(titleId)
    platformField.val(platform)

    if (platform === 'PSM' || platform === 'PSV' || platform === 'PSM') {
      licenseTypeField.val('zRIF')
    } else if (platform === 'PS3' || platform === 'PS4') {
      licenseTypeField.val('RAP')
    }

    // inputForm.submit()
    makeRequest(
      [
        inputForm
          .find('input')
          .filter(function(index, el) {
            return $(el).val() !== ''
          })
          .serializeJSON()
      ],
      'validate'
    )

    // makeRequest(inputForm, inputForm.serialize(), data => {
    //   $('.jumbotron').LoadingOverlay('hide')
    //       .empty()
    //       .append(_contributionFinalizeSingle(data))
    // $.spin('stop')
    // $('.jumbotron')
    //   .empty()
    //   .append(_contributionFinalizeSingle(data))
    // })
  } else {
    $.LoadingOverlay('hide')
    alert('Data must be provided to at least one of the fields.')
  }
}

function vb() {
  let licenseField = $('#batchzip')[0]
  let psdleField = $('#psdle')[0]
  let files = {
    json: psdleField.files[0],
    zip: licenseField.files[0]
  }

  // $('.jumbotron').LoadingOverlay('show', {
  //   text: 'Judging your files...'
  // })

  // let items = {
  //   new: [],
  //   orphans: [],
  //   rejected: [],
  //   updated: []
  // }
  //
  // let fileItem = item => {
  //   if (!item.item.baseGame) item.item.baseGame = 'UNKNOWN'
  //   if (item.action === 'new') items.new.push(item)
  //   else if (item.action === 'reject') items.rejected.push(item)
  //   else if (item.action === 'update') items.updated.push(item)
  //   if (item.isOrphan) items.orphans.push(item)
  // }

  $('.jumbotron').LoadingOverlay('show', { text: 'Preparing...' })

  if (files.json && !files.zip) {
    // $('.jumbotron').LoadingOverlay('show', {
    //   text: 'Preparing...'
    // })
    jsonOnly()
  } else if (!files.json && files.zip) {
    // $('.jumbotron').LoadingOverlay('show', {
    //   text: 'Preparing...'
    // })
    licensesOnly()
  } else if (files.json && files.zip) {
    // $('.jumbotron').LoadingOverlay('show', {
    //   text: 'Preparing...'
    // })
    jsonAndLicenses()
  } else {
    $('.jumbotron').LoadingOverlay('hide')

    alert('Data must be provided to at least one of the fields.')
  }

  async function jsonOnly() {
    console.debug('no url, json only')
    let arr = []
    let json = getJSONContent(files.json)
    json
      .then(jobjs => {
        let jkeys = Object.keys(jobjs)
        jkeys.forEach((v, i) => {
          arr.push(jobjs[v])
        })
        return arr
      })
      .then(arr => {
        makeRequest(arr, 'validate')
      })
  }

  function licensesOnly() {
    console.debug('no json, yes zip')
    let zip = getZipContent(files.zip)
    let arr = []

    zip
      .then(zobjs => {
        let zkeys = Object.keys(zobjs)
        zkeys.forEach((v, i) => {
          arr.push(zobjs[v])
        })
        return arr
      })
      .then(arr => {
        makeRequest(arr, 'validate')
      })
  }

  function jsonAndLicenses() {
    console.debug('json and licenses')
    Promise.all([getJSONContent(files.json), getZipContent(files.zip)]).then(
      values => {
        let json = values[0]
        let licenses = values[1]
        let items = matchJSONToLicenses(json, licenses)
        makeRequest(items, 'validate')
        // makeRequest($('#inputForm'), { items }, data => {
        //   $('.jumbotron')
        //     .LoadingOverlay('hide')
        //     .empty()
        //     .append(_contributionConfirmBatch(data))
        //   hookFinalizePage()
        // })
      },
      err => {
        console.error(err)
      }
    )
  }

  // function makeRequest(data) {
  //   const socket = new WebSocket(`wss://${document.location.host}/contribute`)
  //
  //   let index = 0
  //
  //   let updateLoadingOverlayProgress = index => {
  //     let el = $('.jumbotron')
  //     el.LoadingOverlay('text', `Checking ${index}/${data.length}`)
  //     el.LoadingOverlay('progress', (index / data.length) * 100)
  //   }
  //
  //   let next = () => {
  //     updateLoadingOverlayProgress(index)
  //     let _data = JSON.stringify({
  //       action: 'validate',
  //       contribType: contributionMethod,
  //       item: data[index]
  //     })
  //     socket.send(_data)
  //     index += 1
  //   }
  //
  //   socket.addEventListener('open', e => {
  //     next()
  //   })
  //
  //   socket.addEventListener('message', e => {
  //     let parsedItem = JSON.parse(e.data)
  //
  //     console.debug(e)
  //
  //     if (parsedItem.item.platform === 'PS3' || parsedItem.item.platform === 'PS4') {
  //       parsedItem.item.licenseType = 'RAP'
  //     } else if (parsedItem.item.platform === 'PSV' || parsedItem.item.platform === 'PSM') {
  //       parsedItem.item.licenseType = 'zRIF'
  //     }
  //
  //     fileItem(parsedItem)
  //
  //     if (index < data.length) {
  //       next()
  //     } else {
  //       socket.close()
  //       $('.jumbotron')
  //         .LoadingOverlay('progress', 100)
  //         .LoadingOverlay('hide')
  //         .empty()
  //         .append(_contributionConfirmBatch({ items }))
  //       hookFinalizePage()
  //
  //       // startGettingInfo(items)
  //
  //       console.log(items)
  //     }
  //   })
  // }
}

function makeRequest(data, action) {
  const socket = new WebSocket(`ws://${document.location.host}/contribute`)

  let items = {
    new: [],
    orphans: [],
    rejected: [],
    updated: []
  }

  let fileItem = item => {
    if (!item.item.baseGame) item.item.baseGame = 'UNKNOWN'
    if (item.action === 'new') items.new.push(item)
    else if (item.action === 'reject') items.rejected.push(item)
    else if (item.action === 'update') items.updated.push(item)
    if (item.isOrphan) items.orphans.push(item)
  }

  let updateLoadingOverlayProgress = index => {
    let el = $('.jumbotron')
    if (action === 'validate') {
      el.LoadingOverlay('text', `Validating ${index}/${data.length}`)
    } else if (action === 'finalize') {
      el.LoadingOverlay('text', `Finalizing ${index}/${data.length}`)
    }

    el.LoadingOverlay('progress', (index / data.length) * 100)
  }

  let timerId = 0
  function keepAlive() {
    let timeout = 20000
    if (socket.readyState === socket.OPEN) {
      socket.send('{}')
    }
    timerId = setTimeout(keepAlive, timeout)
  }
  function cancelKeepAlive() {
    if (timerId) {
      clearTimeout(timerId)
    }
  }

  let index = 0

  let next = () => {
    updateLoadingOverlayProgress(index)
    let _data = JSON.stringify({
      action: action,
      contribType: contributionMethod,
      item: data[index]
    })
    socket.send(_data)
    index += 1
  }

  socket.onopen = e => {
    keepAlive()
    next()
  }

  socket.onclose = e => {
    cancelKeepAlive()
  }

  socket.onerror = e => {
    console.error(e)
  }

  socket.onmessage = e => {
    let parsedItem = JSON.parse(e.data)

    console.log(parsedItem)

    if (action === 'validate') {
      fileItem(parsedItem)
    }

    if (index < data.length) {
      next()
    } else {
      $('.jumbotron')
        .LoadingOverlay('progress', 100)
        .LoadingOverlay('hide')
        .empty()

      if (action === 'validate') {
        $('.jumbotron').append(
          _contributionConfirmBatch({ items, contributionMethod })
        )
        console.log(items)
        hookFinalizePage()
      } else if (action === 'finalize') {
        socket.close()
        $('.jumbotron').append(`Success! Submitted ${index} items!`)
      }

      // startGettingInfo(items)
    }
  }
}

function hookFinalizePage() {
  let finalizeForm = $('#finalizeForm')
  let btnFinalize = $('#btnFinalize')
  require('./utils.slideToggle')

  $('.showForm + div').hide()
  $('.newItems > .toggle-title')
    .addClass('active')
    .closest('.toggle')
    .find('.toggle-inner')
    .slideDown(600)

  $('.itemsHeader').click(e => {
    e.preventDefault()
    $(e.target)
      .parent()
      .next()
      .toggle('slow')
  })

  $('a.showForm').click(e => {
    e.preventDefault()
    $(e.target)
      .next()
      .toggle('slow')
  })

  btnFinalize.click(e => {
    e.preventDefault()
    let arr = $("form[id!='finalizeForm']").serializeJSON()

    let result = []
    for (let i in arr) {
      result.push(arr[i])
    }

    makeRequest(result, 'finalize')
  })
}

// async function startGettingInfo(items) {
//   let getInfo = new GetInfo({
//     baseUrl: 'http://proxy.nopaystation.com'
//   })
//   let index = 0
//
//   let next = async () => {
//     let item = items.new[index].item
//     let el = $('#' + item.contentId)
//     el.LoadingOverlay('show', {
//       text: 'Checking URL',
//       direction: 'row'
//     })
//     let gi = await getInfo.pkg(item.url)
//     item.gi = gi.results
//     el.LoadingOverlay('hide')
//     console.log(item)
//     index += 1
//     if (index < items.new.length) {
//       next()
//     } else {
//       $('.newItems > .toggle-inner')
//         .empty()
//         .html(_newContribFinalizeForm({ items: items.new }))
//
//       $('.newItems .showForm + div').hide()
//       $('.newItems .showForm').click(e => {
//         e.preventDefault()
//         $(e.target)
//           .next()
//           .toggle('slow')
//       })
//     }
//   }
//
//   next()
// }

// function validateBatch() {
//   let licenseField = $('#batchzip')[0]
//   let psdleField = $('#psdle')[0]
//
//   // let itemsField = $('#items')
//
//   let files = {
//     json: psdleField.files[0],
//     zip: licenseField.files[0]
//   }
//
//   if (files.json && !files.zip) {
//     let json = getJSONContent(files.json)
//     let arr = []
//
//     json
//       .then(jobjs => {
//         let jkeys = Object.keys(jobjs)
//         jkeys.forEach((v, i) => {
//           arr.push(jobjs[v])
//         })
//         return arr
//       })
//       .then(arr => {
//         makeRequest($('#inputForm'), { items: arr }, data => {
//           $.spin('stop')
//           $('.jumbotron')
//             .empty()
//             .append(_contributionFinalizeBatch(data))
//           hookFinalizePage()
//         })
//
//         // itemsField.val(JSON.stringify(arr))
//         // inputForm.submit()
//       })
//   } else if (!files.json && files.zip) {
//     console.log('no json, yes zip')
//     let zip = getZipContent(files.zip)
//     let arr = []
//
//     zip
//       .then(zobjs => {
//         let zkeys = Object.keys(zobjs)
//         zkeys.forEach((v, i) => {
//           arr.push(zobjs[v])
//         })
//         return arr
//       })
//       .then(arr => {
//         makeRequest($('#inputForm'), { items: arr }, data => {
//           $.spin('stop')
//           $('.jumbotron')
//             .empty()
//             .append(_contributionFinalizeBatch(data))
//           hookFinalizePage()
//         })
//       })
//   } else if (files.json && files.zip) {
//     Promise.all([getJSONContent(files.json), getZipContent(files.zip)]).then(
//       values => {
//         let json = values[0]
//         let licenses = values[1]
//         let items = matchJSONToLicenses(json, licenses)
//
//         makeRequest($('#inputForm'), { items: items }, data => {
//           $.spin('stop')
//           $('.jumbotron')
//             .empty()
//             .append(_contributionFinalizeBatch(data))
//           hookFinalizePage()
//         })
//       },
//       err => {
//         console.error(err)
//       }
//     )
//   }
// }

function getTitleIdFromUrl(url) {
  let urlPieces = $(url.split('/'))

  if (url.endsWith('.pkg') || url.endsWith('.xml')) {
    return urlPieces.get(urlPieces.length - 2).split('_')[0]
  } else if (url.endsWith('.json')) {
    return urlPieces.get(urlPieces.length - 5).split('_')[0]
  }
}

function getContributionMethod(form) {
  let methods = ['single', 'batch']
  return $(form)
    .attr('action')
    .split('/')
    .filter(word => (methods.includes(word) ? word : false))[0]
}

// https://github.com/Stuk/jszip/issues/375
function getZipContent(zipFile) {
  return JSZip.loadAsync(zipFile)
    .then(zip => {
      let entries = Object.keys(zip.files)
        .map(function(name) {
          if (!zip.files[name].dir) {
            let tmp = zip.files[name].name.split('.')
            let fileExt = tmp[tmp.length - 1].toLowerCase()

            if (fileExt !== 'db') {
              return zip.files[name]
            }
          }
        })
        .filter(el => {
          if (el !== undefined) {
            // Only return files that are not inside a __MACOSX folder (auto-generated on macOS)
            if (el.name.slice(0, 8) !== '__MACOSX') {
              return el
            }
          }
        })

      let listOfPromises = entries.map(function(entry) {
        return entry.async('uint8array').then(function(u8) {
          let tmp = entry.name.split('.')
          let fileExt = tmp[tmp.length - 1].toLowerCase()
          if (fileExt === 'rif') {
            // psv/psm
            return [entry.name, rifRapTools().encode(u8)]
          } else if (fileExt === 'rap') {
            // ps3
            return [entry.name, buffer.Buffer.from(u8).toString('hex')]
          } else if (fileExt === 'db') {
          }
        })
      })

      return Promise.all(listOfPromises)
    })
    .then(function(list) {
      let arr = {}
      list.reduce(function(accumulator, current) {
        let currentName = current[0].split('/')
        let currentValue = current[1]

        let tmp = current[0].split('.')
        let fileExt = tmp[tmp.length - 1].toLowerCase()

        let obj = {
          license: currentValue,
          contentId: '',
          titleId: '',
          licenseType: '',
          region: ''
        }

        if (fileExt === 'rif') {
          let contentId = rifRapTools().zrif2rif(currentValue).contentId
          let titleId = contentIdToTitleId(obj.contentId)
          let platform = getPlatformFromTitleID(obj.titleId)

          if (contentId.startsWith('EP9000') && platform !== 'PS4') {
            return accumulator
          }
          obj.contentId = contentId
          obj.titleId = titleId
          obj.platform = getPlatformFromTitleID(obj.titleId)
          obj.region = rifRapTools().getRegionFromTitleID(obj.titleId)
          obj.licenseType = 'zRIF'
        } else if (fileExt === 'rap') {
          obj.contentId = currentName[currentName.length - 1].split('.')[0]
          obj.titleId = contentIdToTitleId(obj.contentId)
          obj.platform = getPlatformFromTitleID(obj.titleId)
          obj.region = rifRapTools().getRegionFromTitleID(obj.titleId)
          obj.licenseType = 'RAP'
        }

        arr[obj.contentId] = obj
        return arr
      }, {})

      return arr
    })
}

function getLicenseFromFile(file) {
  let reader = new FileReader()
  let ext = file.name.slice(-3)

  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort()
      reject(new DOMException('Problem parsing input file.'))
    }

    reader.onload = () => {
      let result = reader.result
      if (ext === 'rif' || ext === 'bin') {
        resolve(rifRapTools().zrif2rif(rifRapTools().encode(result)))
      } else if (ext === 'rap') {
        try {
          let lic = buffer.Buffer.from(result).toString('hex')
          let cid = file.name.slice(0, file.name.length - 4)
          let tid = contentIdToTitleId(cid)
          resolve({
            license: lic,
            contentId: cid,
            titleId: tid,
            platform: getPlatformFromTitleID(tid),
            region: rifRapTools().getRegionFromTitleID(tid)
          })
        } catch (err) {
          alert(`Invalid license: ${file.name}`)
          $('#licenseFileField').val('')
        }
      }
    }
    reader.readAsArrayBuffer(file)
  })
}

async function getJSONContent(file) {
  let reader = new FileReader()

  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort()
      reject(new DOMException('Problem parsing input file.'))
    }

    reader.onload = () => {
      let parsedJson = JSON.parse(reader.result)

      let items = parsedJson.items.filter(item => {
        if (
          item.hasOwnProperty('platform') &&
          item.hasOwnProperty('pkg') &&
          item.hasOwnProperty('id')
        ) {
          return !item.pkg.endsWith('.MNV')
        }
      })

      items.map(item => {
        item.contentId = item.id
        item.productId = item.productID
        item.titleId = contentIdToTitleId(item.id)
        if (item.platform === 'PS Vita') {
          item.platform = 'PSV'
        }
      })

      const arr2Obj = array =>
        array.reduce((obj, item) => {
          obj[item.id] = item
          return obj
        }, {})

      resolve(arr2Obj(items))
    }

    reader.readAsText(file)
  })
}

function matchJSONToLicenses(json, licenses) {
  for (var key in licenses) {
    if (json.hasOwnProperty(key)) {
      json[key].license = licenses[key].license
      json[key].contentId = licenses[key].contentId
    } else {
      json[key] = licenses[key]
    }
  }

  return Object.values(json)
}

function contentIdToTitleId(contentId) {
  return contentId.split('-')[1].split('_')[0]
}

// https://www.psdevwiki.com/ps3/Productcode
function getPlatformFromTitleID(titleId) {
  if (titleId.startsWith('PCS')) {
    return 'PSV'
  } else if (
    titleId.startsWith('BCU') ||
    titleId.startsWith('BLU') ||
    titleId.startsWith('BCJ') ||
    titleId.startsWith('BLJ') ||
    titleId.startsWith('BCA') ||
    titleId.startsWith('BLA') ||
    titleId.startsWith('BLE') ||
    titleId.startsWith('NPA') ||
    titleId.startsWith('NPE') ||
    titleId.startsWith('NPH') ||
    titleId.startsWith('NPJ') ||
    titleId.startsWith('NPK') ||
    titleId.startsWith('NPU') ||
    titleId.startsWith('NPI') ||
    titleId.startsWith('UCU') ||
    titleId.startsWith('ULU')
  ) {
    let productCodeType = titleId.slice(3, 4)

    if (
      productCodeType === 'A' ||
      productCodeType === 'B' ||
      productCodeType === 'C' ||
      productCodeType === 'D' ||
      productCodeType === 'J' ||
      productCodeType === 'K' ||
      productCodeType === 'L' ||
      productCodeType === 'O' ||
      productCodeType === 'P' ||
      productCodeType === 'Q' ||
      productCodeType === 'S'
    ) {
      return 'PS3'
    } else if (
      productCodeType === 'E' ||
      productCodeType === 'G' ||
      productCodeType === 'H' ||
      productCodeType === 'I' ||
      productCodeType === 'T' ||
      productCodeType === 'W' ||
      productCodeType === 'X' ||
      productCodeType === 'Y' ||
      productCodeType === 'Z'
    ) {
      return 'PSP'
    }
  } else if (
    titleId.startsWith('NPN') ||
    titleId.startsWith('NPP') ||
    titleId.startsWith('NPO') ||
    titleId.startsWith('NPQ')
  ) {
    return 'PSM'
  } else if (titleId.startsWith('CUSA')) {
    return 'PS4'
  }
}
