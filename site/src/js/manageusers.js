$(document).ready(() => {
  $('#submitNewUserForm').click(e => {
    e.preventDefault()

    console.log('submitted')

    $.post('/register', $('#newuser-form').serialize(), () => {
      // location.reload()
      window.location.replace('/manage/users')
    })
  })

  $('.submitDeleteUserForm').click(e => {
    e.preventDefault()

    let id = e.target.id
    let url = '/manage/users/' + id + '/delete'

    $.post(url, { _id: id }, () => {
      window.location.replace('/manage/users')
    })
  })

  $('.submitEditUserForm').click(e => {
    e.preventDefault()

    console.log('submitted')
    console.log(e.target.id)

    let id = e.target.id
    let url = '/manage/users/' + id + '/edit'

    let form = $('#editUser-form-' + id)

    let postData = form.serialize()

    $.post(url, postData, () => {
      window.location.replace('/manage/users')
    })
  })
})
