$(document).ready(() => {
  let filterForm = $('#dbFilterForm')

  $('.btnFilterProperty > input, #limit, #completeness').on(
    'change',
    e => {
      e.preventDefault()
      let url = '/manage/contributions?' + filterForm.serialize()

      filterForm.attr('action', url)

      $('#btnFilter').click()
    }
  )

  $('.btn-group-toggle').on('click', 'label.btn', function(e) {
    if ($(this).hasClass('active')) {
      setTimeout(
        function() {
          $(this)
            .removeClass('active')
            .find('input')
            .prop('checked', false)

          $('#btnFilter').click()

        }.bind(this),
        10
      )
    }
  })

  filterForm.submit(e => {
    let url = '/manage/contributions?' + filterForm.serialize()

    return filterForm.attr('action', url)
  })

  $('a[data-toggle="modal"]').on('click', e => {
    e.preventDefault()
  })
})
