const util = require('util')
const execFile = util.promisify(require('child_process').execFile)

const path = require('path')

module.exports = async function(url, callback) {
  // const { stdout, stderr, error } = await execFile(
  //   path.join(__dirname, '/PSN_get_pkg_info.py'),
  //   ['-f 3', url]
  // )
  try {
    let result = await execFile(
      path.join(__dirname, '/PSN_get_pkg_info.py'),
      ['-f 3', url]
    )
    callback(null, result)
  } catch (e) {
    callback(e, null)
  }


  // if (stderr) {
  //   console.error(stderr.slice(stderr.lastIndexOf('\n')))
  // }
  // else if (error) {
  //   console.error('error', error)
  //   // callback(error, null)
  // }
  // else if (stdout) {
  //   if (stdout.startsWith('[ERROR]')) {
  //     callback(stdout, null)
  //   } else {
  //     try {
  //       callback(null, JSON.parse(stdout).results)
  //     } catch(e) {
  //       console.error(e)
  //     }
  //   }
  // }



}
