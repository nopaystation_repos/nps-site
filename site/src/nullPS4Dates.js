db.getCollection('items').update(
  { consoleType: 'PS4' },
  { $set: { lastModificationDate: null } },
  {
    multi: true
  }
)
