{
  services.npsSiteBackend = { pkgs, lib, ... }: {
    nixos.useSystemd = true;
    nixos.configuration = {
      boot.tmpOnTmpfs = true;
      services.mongodb = {
        enable = true;
        bind_ip = "0.0.0.0";
      };
    };

    service.volumes = [ "${toString ./.}/mongodb-data:/var/db/mongodb" ];
    service.useHostStore = true;
    service.ports = [
      "27017:27017"
    ];
  };
}
